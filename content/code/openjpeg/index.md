---
title: "Openjpeg"
slug: "openjpeg"
date: 2022-11-15T08:29:49+01:00
draft: false
type: "code"


# Table of content
toc: false       # do you want to show a ToC ? [true, false]
tocOpen: false   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of codes)
thumbnail: opj_logo_full.png

# Add a cover picture [optional]
cover: opj_logo_full.png
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: "The OpenJPEG library is an open-source JPEG 2000 codec written in C language. Thanks to its active development and its BSD license, it has been chosen by the JPEG Committee to shortly become one of its reference software." 

# Add categories of the research post (e.g. image processing)
categories: ["Codec", "C language", "JPEG"]

# Add researchers involved
researchers: ["Antonin Descampe"]
---

OpenJPEG is an open-source JPEG 2000 codec written in C language. It has been developed in order to promote the use of [JPEG 2000](http://www.jpeg.org/jpeg2000), a still-image compression standard from the Joint Photographic Experts Group [JPEG](http://www.jpeg.org).

**This codec is hosted [here](http://www.openjpeg.org/)**
