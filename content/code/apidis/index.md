---
title: "APIDIS dataset"
slug: "apidis"
date: 2022-10-13T18:20:17+02:00
draft: false
type: "code"

# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: right  # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

thumbnail: all.jpg

# Add a cover picture [optional]
cover: all.jpg
coverCaption: "7 viewpoints of the APIDIS acquisition"

# Add a short description that will be shown on the list page [optional]
description: APIDIS basketball dataset

# Add categories of the research post (e.g. image processing)
categories: ["Dataset", "Sport Images", "Sport Videos", "Video Processing"]
---

APIDIS European project led to two acquisitions of basketball games. The APIDIS dataset (presented here) and the <a href="../spiroudome">SPIROUDOME dataset</a>.


# Download
- <a href="https://www.kaggle.com/datasets/gabrielvanzandycke/apidis-videos-part-1">Part 1</a> : raw video files for cameras 1 to 4.
- <a href="https://www.kaggle.com/datasets/gabrielvanzandycke/apidis-videos-part-2">Part 2</a> : raw video files for cameras 5 to 8.
- <a href="https://www.kaggle.com/datasets/gabrielvanzandycke/apidis-metadata">Metadata</a> : annotations, pseudo-synchronised dataset, code, ...



# Data

Approximatively 2 hours of videos were captured from 7 viewpoints during a professional basketball game.

**Raw data**

The cameras were recording at almost 22 fps in average, with a resolution of 1600x1200 pixels. The video files are available in their native format, i.e. one motion jpeg file (~300 MB) per minute per camera.

**Pseudo-synchronised video**

A pseudo synchronised dataset is provided by resampling the raw videos at 25 fps (using the closest available frame) with a resolution of 800x600 pixels. The video files are available as one MPEG-4 file (between 28 MB and 56 MB) per minute per camera. For optimal timestamps accuracy, the original dataset should be prefered.

# Annotations

**Timestamps**

All timestamps are expressed in seconds since Epoch when provided as integers. When provided in a human readable format, e.g. in filenames, they follow the <a href="http://en.wikipedia.org/wiki/ISO_8601">ISO 8601</a> date/time syntax.

**Calibration**

All cameras are <a href="http://www.arecontvision.com/">Arecont Vision</a> AV2100M IP cameras (<a href="http://www.arecontvision.com/resources.php?pid=106">datasheet</a>). The fish-eye lenses used for the top view cameras are <a href="http://www.fujinon.com">Fujinon</a> FE185C086HA-1 lenses (<a href="http://www.fujifilmusa.com/products/optical_devices/security/fish-eye/5-mp/fe185c086ha-1/">datasheet</a>). Each camera was indivually calibrated into a common world coordinate system.

**Additional annotations**

Several manually annotated objects are provided (see NEM'08 summit paper):
- Basket ball events like ball possession periods, throws, violations ; for the whole basketball game
- Ball, Players and referees position ; for one minute of the game.

In addition to ball annotation in the 2D images, an approximate 3D localization is inferred for the pseudo-synchronized dataset.


# Terms of use
This dataset is available for non-commercial research in video signal processing only. We kindly ask you to mention the APIDIS project when using this dataset (in publications, video demonstrations...).

# Acknowledgements
We would like to thank Jean-François Prior (<a href="http://www.belfiusnamurcapitale.be/">Dexia Namur</a> basket ball team), Philippe Delmulle <a href="http://www.dbcwaregem.be/">Declercq Stortbeton Waregem</a> basket ball team) and the city of <a href="http://www.namur.be/">Namur</a> for their authorisations and technical help collecting this dataset.



# Related publications
- Parisot, Pascaline ; De Vleeschouwer, Christophe. Graph-based filtering of ballistic trajectory. IEEE International Conference on Multimedia and Expo (ICME), Barcelona, July 2011.
- K.C., Amit Kumar ; Parisot, Pascaline ; De Vleeschouwer, Christophe. Spatio-temporal template matching for ball detection. ACM/IEEE International Conference on Distributed Smart Cameras (ICDSC), Ghent, Belgium, August 2011.
- Parisot, Pascaline; De Vleeschouwer, Christophe. "Consensus-based trajectory estimation for ball detection in a calibrated cameras system." (submitted to the Journal of Real-Time Image Processing) (code available in metadata part)
- Delannay, Damien ; Danhier, Nicolas ; De Vleeschouwer, Christophe. Detection and recognition of sports(wo)men from multiple views. ACM/IEEE International Conference on Distributed Smart Cameras (ICDSC), Como, Italy, August 2009.
- K.C., Amit Kumar ; De Vleeschouwer, Christophe. Discriminative Label Propagation for Multi-Object Tracking with Sporadic Appearance Features. International Conference on Computer Vision (ICCV), Sydney, Australia, December 2013
- Parisot, Pascaline ; Sevilmis, Berk ; De Vleeschouwer, Christophe. Training with corrupted labels to reinforce a probably correct teamsport player detector. Advanced Concepts for Intelligent Vision Systems. Lecture Notes in Computer Science Volume 8192, 2013.
- Parisot, Pascaline ; De Vleeschouwer, Christophe. Scene-specific classifier for effective and efficient team sport players detection from a single calibrated camera (in submission at CVIU - Special Issue on Computer Vision in Sports) (databases available in the metadata part of the dataset)
- Chen, Fan ; De Vleeschouwer, Christophe. Personalized production of basketball videos from multi-sensored data under limited display resolution. Computer Vision and Image Understanding, Vol. 114, no. 6, p. 667-680, 2010.
- De Vleeschouwer, Christophe ; Chen, Fan ; Delannay, Damien ; Parisot, Christophe ; Chaudy, Christophe ; Martrou, Eric ; Cavallaro, Andrea. Distributed video acquisition and annotation for sport-event summarization, NEM Summit, Saint Malo, France, 2008.
- Chen, Fan ; Delannay, Damien ; De Vleeschouwer, Christophe. An Autonomous Framework to Produce and Distribute Personalized Team-Sport Video Summaries: A Basketball Case Study, IEEE Transactions on Multimedia, Volume:13 ,  Issue: 6, pp. 1381 - 1394, December 2011.

# Contacts

If necessary, please contact the coordinator: Christophe De Vleeschouwer, christophe.devleeschouwer@uclouvain.be.
You can also contact Damien Delannay, Damien.Delannay@uclouvain.be.

<image src="ico_logo_ville.gif">  <image src="Logo_Dexia_2008_bis.jpg">  <image src="7.jpg">  <image src="apidislogo.jpg">  <image src="flag.jpg">


