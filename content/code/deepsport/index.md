---
title: "DeepSport basketball-instants dataset"
slug: "deepsport"
date: 2022-11-15T08:29:24+01:00
draft: false
type: "code"


# Table of content
toc: false       # do you want to show a ToC ? [true, false]
tocOpen: false   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of codes)
thumbnail: dataset-cover.png

# Add a cover picture [optional]
cover: dataset-cover.png
coverCaption:

# Add a short description that will be shown on the list page [optional]
description: "This basketball dataset was acquired under the Walloon region project DeepSport, using the Keemotion system installed in multiple arenas."

# Add categories of the research post (e.g. image processing)
categories: ["Dataset", "Sport Images", "Sport Videos"]

# Add researchers involved
researchers: ["gabriel-vanzandycke", "maxime-istasse", "christophe-de-vleeschouwer"]
---


This basketball dataset was acquired under the Walloon region project DeepSport, using the Keemotion system installed in multiple arenas.

## Terms of use

This dataset is made available for non-commercial research only under the licence [cc-by-nc-sa](https://creativecommons.org/licenses/by-nc-sa/2.0/).

We kindly ask you to mention related publications of the ISPGroup (see bottom of the page) when using this dataset (in publications, video demonstrations...).


## Dataset format

The dataset is composed of images file (in `.png` format), keypoint ground-truth files (in `.json` format) and masks ground-truth files (in `.png` format).

*Images:* The dataset is composed of pairs of successive images captured in different basketball arenas during professional games. The cameras capture a fixed part of the basketball court and have a resolution between 2Mpx and 5Mpx. The resulting images have a definition varying between 65px/m (furthest point on court in the arena with the lowest resolution cameras) and 265px/m (closest point on court in the arena with the highest resolution cameras). The delay between the two successive images is 40ms.

## Ground-truth

*Keypoints Ground-Truth*: Keypoints Ground-Truth are stored in json files as a list of annotations with multiple attributes.

**Ball keypoint**:
- type: "Ball"
- camera: The camera index on which the ball has been annotated (starting at 0)
- position: The (x,y) position of the center of the ball in the image
- visible: A boolean stating if at least half of the ball surface is visible

**Human poses**:
- type: "Player"
- camera: The camera index on which the human has been annotated (starting at 0)
- head: The (x,y) position of the center of the head in the image space
- hips: The (x,y) position of the center of the hips in the image space
- foot1: The (x,y) position of one foot in the image space (no left/right consideration)
- foot2: The (x,y) position of the other foot in the image space (no left/right consideration)

**Note on Object Keypoint Similarity**: To compute OKS, the standard deviation of the unnormalized Gaussian trough which the euclidian distance between predicted and annotated keypoints is passed should be s&#954;i, where s is the object scale and &#954;i is a per-keypont constant that controls falloff. We measured the following values:
- ball: &#954; = 0.06
- head: &#954; = 0.12
- hips: &#954; = 0.16
- feet: &#954; = 0.08

*Segmentation Ground-Truth*: Mask segmentation is stored in a png file where each pixel correspond to one single instance. The class is given by the 4th digit and the instance ID (starting at 1) is given by the 1st digit:
- Unlabelled class: 0
- Human class: 1
- Ball class: 3


## Dataset

A public subset is made available on [Kaggle](https://www.kaggle.com/gabrielvanzandycke/deepsport-dataset). Additional images and annotations can be provided on demand.


## Acknowledgments

This basketball dataset was acquired under the Walloon region project DeepSport, using the Keemotion system installed in multiple arenas.
We would like to thanks both Keemotion for letting us use their system for raw image acquisition during live productions, and the LNB for the rights on their images.

{{<image src="wallonia.jpg" width="20%" position="center">}}
{{<image src="keemotion.png" width="20%" position="center">}}
{{<image src="LNB.png" width="20%" position="center">}}
