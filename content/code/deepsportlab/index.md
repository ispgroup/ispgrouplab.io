---
title: "Deepsportlab"
slug: "deepsportlab"
date: 2022-11-18T10:25:53+01:00
draft: false
type: "code"


# Table of content
toc: false       # do you want to show a ToC ? [true, false]
tocOpen: false   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of codes)
thumbnail: DSL_19_all_small.jpg

# Add a cover picture [optional]
cover: DSL_19_all_small.jpg
coverCaption:

# Add a short description that will be shown on the list page [optional]
description: DeepSportLab; a Unified Framework for BallDetection, Player Instance Segmentationand Pose Estimation in Team Sports Scenes

# Add categories of the research post (e.g. image processing)
categories: ["Sport Videos", "Pose Estimation", "Segmentation", "Ball Detection", "Basketball"]

# Add researchers involved
researchers: ["abolfazl-ghasemzadeh","gabriel-vanzandycke","Maxime Istasse", "Niels Sayez", "Amirafshar Moshtaghpour", "christophe-de-vleeschouwer"]
---

# Description
DeepSportLab: a Unified Framework for Ball Detection, Player Instance Segmentationand Pose Estimation in Team Sports Scenes

Link to the github repo: [Github](https://github.com/ispgroupucl/DeepSportLab)

Link to the paper: [arXiv](https://arxiv.org/abs/2112.00627)

This paper presents a unified framework to (i)locate the ball, (ii)predict the pose, and (iii)segment the instance mask of players in team sports scenes. Those problems are ofhigh interest in automated sports analytics, production, and broadcast. A common prac-tice is to individually solve each problem by exploiting universal state-of-the-art models,e.g., Panoptic-DeepLab for player segmentation. In addition to the increased complexityresulting from the multiplication of single-task models, the use of the off-the-shelf mod-els also impedes the performance due to the complexity and specificity of the team sportsscenes, such as strong occlusion and motion blur. To circumvent those limitations, ourpaper proposes to train a single model that simultaneously predicts the ball and the playermask and pose by combining the part intensity fields and the spatial embeddings princi-ples. Part intensity fields provide the ball and player location, as well as player joints lo-cation. Spatial embeddings are then exploited to associate player instance pixels to theirrespective player center, but also to group player joints into skeletons. We demonstratethe effectiveness of the proposed model on the DeepSport basketball dataset, achievingcomparable performance to the SoA models addressing each individual task separately.
