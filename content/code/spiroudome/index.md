---
title: "Spiroudome dataset"
slug: "spiroudome"
date: 2022-10-13T21:51:41+02:00
draft: false
type: "code"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: right  # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

thumbnail: spiroudome_all.jpg

# Add a cover picture [optional]
cover: spiroudome_all.jpg
coverCaption:

# Add a short description that will be shown on the list page [optional]
description: SPIROUDOME basketball dataset.

# Add categories of the research post (e.g. image processing)
categories: ['Dataset', "Sport Video"]
---

SPORTIC European project led to two acquisitions of basketball games. The <a href="../apidis">APIDIS dataset</a> and the SPIROUDOME dataset (presented here).

# Download

<a href="https://www.kaggle.com/datasets/gabrielvanzandycke/spiroudome-dataset">The dataset is available on Kaggle.</a>


# Data

A basketball game was captured with 8 cameras (4 on each side of the court) at ~25 fps with a resolutin of 1600x1200 pixels. The video files are available in their native format, i.e. one motion jpeg file (~300 MB) per minute per camera.

# Annotations

The video files come with calibration data for each camera into a common world coordinate system, and timestamps for each frames. In addition, ground truth player positions were manually annotated on camera 2.


# Terms of use
This dataset is available for non-commercial research in video signal processing only. We kindly ask you to mention related publications of the ISPGroup (see bottom of the <a href="../apidis">APIDIS dataset webpage</a>) when using this dataset (in publications, video demonstrations...).



# Contacts

If necessary, please contact the coordinator: Christophe De Vleeschouwer, christophe.devleeschouwer@uclouvain.be.
You can also contact Damien Delannay, Damien.Delannay@uclouvain.be.

