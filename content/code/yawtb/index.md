---
title: "Yet Another Wavelet toolbox (YAWTB)"
slug: "yawtb"
date: 2022-11-15T08:30:50+01:00
draft: false
type: "code"


# Table of content
toc: false       # do you want to show a ToC ? [true, false]
tocOpen: false   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of codes)
thumbnail: yawtb_logo.jpg

# Add a cover picture [optional]
cover: 
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: 

# Add categories of the research post (e.g. image processing)
categories: ["Toolbox", "Matlab", "Wavelet"]

# Add researchers involved
researchers: ["laurent-jacques"]
---

# Welcome to the Yet Another Wavelet toolbox.

The purpose of this toolbox is to implement in Matlab 1D, 2D, 3D Continuous Wavelet Transforms (CWTs), 1D+Time (spatio-temporal) CWT, (Stereographical) Spherical CWT, 2D dyadic Frames (isotropic and directional), (in development) Spherical Frames of Stereographical Wavelets

**This Matlab toolbox is hosted on [GitHub](https://github.com/jacquesdurden/yawtb)** <span class="icon-github"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 19c-5 1.5-5-2.5-7-3m14 6v-3.87a3.37 3.37 0 0 0-.94-2.61c3.14-.35 6.44-1.54 6.44-7A5.44 5.44 0 0 0 20 4.77 5.07 5.07 0 0 0 19.91 1S18.73.65 16 2.48a13.38 13.38 0 0 0-7 0C6.27.65 5.09 1 5.09 1A5.07 5.07 0 0 0 5 4.77a5.44 5.44 0 0 0-1.5 3.78c0 5.42 3.3 6.61 6.44 7A3.37 3.37 0 0 0 9 18.13V22"></path></svg></span>
