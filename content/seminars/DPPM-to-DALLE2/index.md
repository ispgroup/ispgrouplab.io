---
title: "Diffusion models : from DDPM to DALLE-2"
slug: "DPPM-to-DALLE2"
draft: false

# name of the speaker
speaker: "Mathieu Simon (UCLouvain)"

# date of the seminar
seminarDate:  2022-07-22 

# external link (eg web page of the speark)
externalLink: 

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files :
    slides: ISPS220722.pdf
    notebook: ISPS220722.ipynb
# Location of the seminar
location: Nyquist seminar room, ICTEAM

# duration of the seminar in minutes
duration: 50

# Abstract of the seminar
abstract: Many of us have probably been mind blowned by the recent progress in text-to-image generation tasks demonstrated by models such as GLIDE, DALLE-2 or Imagen. A lot of these incredible results can be attributed to the meteoric rise of diffusion models, a type of generative model that has been gaining significant popularity recently and has shown to outperform GANs on image synthesis. This presentation aims at giving a small (and humble) introduction to these models and to share my understanding on how we went from the original denoising diffusion probabilistic model to DALLE-2.

---
## External References
* DDPM : https://arxiv.org/abs/2006.11239
* DDIM : https://arxiv.org/abs/2010.02502
* Diffusion models beat GANs : https://arxiv.org/abs/2105.05233
* Classifier free guidance : https://openreview.net/pdf?id=qw8AKxfYbI
* GLIDE : https://arxiv.org/abs/2112.10741
