---
title: High Level Markov Modeling and Markov Random Tree recognition
slug: ABS110612-High-Level-Markov-Modeling-and-Markov-Random-Tree-recognition
draft: false

# name of the speaker
speaker: "Jerome Plumat"

# date of the seminar
seminarDate: 2012-06-11T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Markov Random Field (MRF) modeling is a powerful framework allowing to formulate and to solve very complex imaging problems. This talk presents a particular case of MRF: the High Level MRF with application to root segmentation. This framework enables to formulate features based matching. The structures to recognize are assimilated to Markov Random Trees. A curves formulation aims to reduce the solution space and implement complex metrics. Results will be presented on data base and isolate images."

---
