---
title: "Compressive Classification: A Guided Tour"
slug: ABS240915-Compressive-Classification:-A-Guided-Tour
draft: false

# name of the speaker
speaker: "Valerio Cambareri"

# date of the seminar
seminarDate: 2015-09-24T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_240915.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "The mature concept of compressed sensing (CS) is being transferred to the application level as a means to save the physical resources spent in the analog-to-digital interface of challenging signal and image acquisition tasks, i.e., when the underlying sensing process requires a critical amount of time, power, sensor area and cost. For most structured signals, this method amounts to applying a dimensionality-reducing random matrix followed by an accurate, yet computationally expensive recovery algorithm that is capable of producing full-resolution signal recoveries from such low-dimensional measurements."

---
