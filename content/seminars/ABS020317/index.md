---
title: FBMC/OQAM transceivers for 5G mobile communication systems
slug: ABS020317-FBMC/OQAM-transceivers-for-5G-mobile-communication-systems
draft: false

# name of the speaker
speaker: "François Rottenberg (DIGICOM, ICTEAM, UCL)"

# date of the seminar
seminarDate: 2016-03-02T16:15:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_020317.pdf

# Location of the seminar
location: "Nysquist Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "One of the main challenges investigated for 5G wireless communication systems is the ability to take care of the largely increasing number of devices, establishing connections with a wide variety of requirements: bit rate, flexibility, energy efficiency... Under those aspects, Offset-QAM-based filterbank multicarrier (FBMC-OQAM) has been shown to be a promising alternative to cyclic prefix-orthogonal division multiplexing (CP-OFDM), the modulation used in most wireless communication systems nowadays, such as, Wi-Fi, DSL, LTE, DVB..."

---
