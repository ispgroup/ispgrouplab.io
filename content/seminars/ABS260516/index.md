---
title: Low Rank and Group-Average Sparsity Driven Convex Optimization for Direct Exoplanets Imaging
slug: ABS260516-Low-Rank-and-Group-Average-Sparsity-Driven-Convex-Optimization-for-Direct-Exoplanets-Imaging
draft: false

# name of the speaker
speaker: "Benoît Pairet"

# date of the seminar
seminarDate: 2016-05-26T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Direct exoplanets imaging is a challenging task for two main reasons. First, the host star is several order of magnitude brighter than exoplanets. Second, the great distance between us and the star system makes the exoplanets-star angular distance very small. Furthermore, imperfections on the optics along with atmospheric disturbances create \"speckles\" that look like planets. Ten years ago, astronomers introduced Angular Differential Imaging (ADI) that uses the rotation of the Earth to add diversity to the data. Signal processing then allows to separate the starlight from the planet signal. After that, a detection signal to noise ratio (SNR) is computed. We will present a sparsity driven convex optimization program along with a new SNR map that beat the state of the art methods for small angular separation."

---
