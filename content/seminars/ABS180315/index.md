---
title: Non-parametric PSF estimation from celestial transit solar images using blind deconvolution
slug: ABS180315-Non-parametric-PSF-estimation-from-celestial-transit-solar-images-using-blind-deconvolution
draft: false

# name of the speaker
speaker: "Adriana Gonzalez"

# date of the seminar
seminarDate: 2015-03-18T10:45:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Characterization of instrumental effects in astronomical imaging is important in order to extract accurate physical information from the observations. Optics are never perfect and the non-ideal path through the telescope is usually represented by the convolution of an ideal image with the Point Spread Function (PSF) of the instrument. Additionally, the image acquisition process is contaminated by other sources of noise. The problem of estimating both the PSF and the denoised and deblurred image, called blind deconvolution, is ill-posed. We propose a blind deconvolution scheme that relies on image regularization. Contrarily to most methods presented in the literature, the proposed method does not assume a parametric model of the PSF and can thus be applied to any telescope. Our scheme is based on a wavelet analysis image prior model and weak assumptions on the filter’s response. We use the observations from a celestial body transit where such object can be assumed to be a black disk. Such constraints limit the interchangeability between the filter and the image in the blind deconvolution problem. The proposed method is applied on synthetic and experimental data. The PSF is estimated for SECCHI/EUVI instrument using the 2007 Lunar transit, and for SDO/AIA with the 2012 Venus transit. Results show that the proposed non-parametric blind deconvolution method is able to estimate the core of the PSF with a similar quality than parametric methods proposed in the literature. We also show that, if these parametric estimations are incorporated in the acquisition model, the resulting PSF outperforms both the parametric and non-parametric methods."

---
