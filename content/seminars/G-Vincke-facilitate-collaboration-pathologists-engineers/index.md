---
title: "How to facilitate collaboration between pathologists and engineers in AI research applied to Digital Pathology using Cytomine"
slug: "G-Vincke-facilitate-collaboration-pathologists-engineers_1"
draft: false
type: "seminars"

# name of the speaker
speaker: Grégoire Vincke (Cytomine Corporation SA, Liège, Belgium)

# date of the seminar
seminarDate: 2021-09-15T14:00:00

# external link (eg web page of the speark)
externalLink: https://cytomine.com

# Attached files 
files : 

# Location of the seminar
location: Shannon seminar room

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: Cytomine is a full-web open-source platform dedicated to the collaborative analysis of Whole-Slide Images (WSI) and the development of AI-driven analysis solutions adapted to Digital Pathology. Cytomine can be used in teaching, research, or diagnosis, but in this seminar, we will focus on the features dedicated to the collaboration between pathologists and AI engineers. Cytomine fosters collaboration between pathologists and AI engineers by providing working interfaces adapted to each community. Using our user-friendly web interface, pathologists are able to manage and explore the WSI and enrich them with semantic added value like annotations or terms. Using our java or python external clients, AI engineers are able to use all these images, annotations, and information to develop AI algorithms adapted to Digital Pathology, and train or run them in any computing center. All the AI-generated information is then available inside the web interface to allow pathologists to validate or edit it, using dedicated web tools. If needed, AI apps can be run by end-users directly from inside the web interface. In this seminar, we will present all this workflow alternatively from the pathologists' and the AI engineers' point of view, and by emphasizing the development possibilities offered by Cytomine open source license. 

---
