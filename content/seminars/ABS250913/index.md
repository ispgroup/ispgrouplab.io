---
title: Optimization of MRI reconstruction by constraining the minimization of TV norm
slug: ABS250913-Optimization-of-MRI-reconstruction-by-constraining-the-minimization-of-TV-norm
draft: false

# name of the speaker
speaker: "Damien Jacobs (ICTEAM/ELEN, UCL)"

# date of the seminar
seminarDate: 2013-09-25T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "In high field MRI, the SNR is significantly increased and makes possible
to improve the voxel resolution. A high number of data's can be acquired
on the patient and should help his diagnosis by the physician. The timing
restriction to scan the patient is an obligation in the medical
institutions. Getting a high resolution with a limited time scan is a real
challenge in MRI reconstruction. Also, high rf stimulation and gradient
amplitudes are restricted in high field MRI to avoid nerves stimulation.
These constraints make harder the sampling and the acquisition of data's.
The sampling of data's in MRI coupled to the MRI reconstruction is a key
point and its interest is growing with the presence of high field MRI.
In this ISPS, a well-known approach of convex optimization by minimizing
Total-Variation norm will be presented in the case of MRI reconstruction.
Simulation of data's and real phantoms acquired on the 11.7 T MRI scan
will be presented."

---
