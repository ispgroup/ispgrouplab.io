---
title: "Compressive acquisition of linear dynamical systems (with application to video CS)"
slug: "ABS120515-Compressive-acquisition-of-linear-dynamical-systems-(with-application-to-video-CS)"
draft: false

# name of the speaker
speaker: "Amirafshar Moshtaghpour"

# date of the seminar
seminarDate: 2015-05-12T10:45:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_120515.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "This talk will analyze the paper A. C. Sankaranarayanan, P. K. Turaga, R. Chellappa, and R. G. Baraniuk, \"Compressive acquisition of linear dynamical systems,\" SIAM Journal on Imaging Sciences 6, No. 4, pp. 2109-2133, 2013. (pdf). Its abstract reads:"

---
