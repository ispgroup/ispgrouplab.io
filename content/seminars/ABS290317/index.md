---
title: Deep Learning in Medical Imaging
slug: ABS290317-Deep-Learning-in-Medical-Imaging
draft: false

# name of the speaker
speaker: "Eliott Brion"

# date of the seminar
seminarDate: 2017-03-29T16:15:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "To illustrate its use in medical imaging, the goal of this seminar is to show how deep learning can automatically contour healthy organs and tumors in CT scans."

---
