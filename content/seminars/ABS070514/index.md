---
title: Single shot depth and image using engineered point spread function
slug: ABS070514-Single-shot-depth-and-image-using-engineered-point-spread-function
draft: false

# name of the speaker
speaker: "Muhammad Arsalan"

# date of the seminar
seminarDate: 2014-05-07T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "TVNUM Seminar Room (a124) Place du Levant 2, Stévin Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "(please, login to read it)"

---
