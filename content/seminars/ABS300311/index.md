---
title: Ball detection and tracking in multi-view setting 
slug: ABS300311-Ball-detection-and-tracking-in-multi-view-setting-
draft: false

# name of the speaker
speaker: "Pascaline Parisot"

# date of the seminar
seminarDate: 2011-03-30T10:30:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "In the context of team sport events monitoring, the various phases of the game must be delimited and interpreted. In the case of a basketball game, the detection and the tracking of the ball are
mandatory. First some common visual characteristics of a ball are presented. Then a ball detection framework based on the extracted knowledge on all the views are detailed. Finally, a graph-based tracking is explained. It consists in a temporal analysis of ball candidates and more specially in detecting ballistic trajectories among them."

---
