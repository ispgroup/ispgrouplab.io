---
title: "1930s Analysis for 2010s Signal Processing: Recent Progress on the Superresolution Question"
slug: "ABS300715-1930s-Analysis-for-2010s-Signal-Processing:-Recent-Progress-on-the-Superresolution-Question"
draft: false

# name of the speaker
speaker: "Prof. Laurent Demanet, Imaging and Computing Group, MIT, USA"

# date of the seminar
seminarDate: 2015-07-30T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "The ability to access signal features below the diffraction limit of an imaging system is a delicate nonlinear phenomenon called superresolution.  The main theoretical question in this area is still mostly open: it concerns the precise balance of noise, bandwidth, and signal structure that enables super-resolved recovery.  When structure is understood as sparsity on a grid, we show that there is a precise scaling law that extends Shannon-Nyquist theory, and which governs the asymptotic performance of a class of simple \"subspace-based\" algorithms. This law is universal in the minimax sense that no statistical estimator can outperform it significantly.  By contrast, compressed sensing is in many cases suboptimal for the same task.  Joint work with Nam Nguyen."

---
