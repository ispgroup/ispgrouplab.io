---
title: Elliptic curve cryptograpy in JavaScript with application for e-voting
slug: ABS281111-Elliptic-curve-cryptograpy-in-JavaScript-with-application-for-e-voting
draft: false

# name of the speaker
speaker: "Quentin De Neyer"

# date of the seminar
seminarDate: 2011-11-28T10:45:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Current browsers offer fairly limited support for performing cryptographic
operations on the client-side of web applications. The support of the
TLS/SSL protocols enables secure client-server communications, but these
protocols can only be useful in settings where the server is trusted by
the client. There are numerous applications, however, in which it is not
desirable to ask web application users to trust a server. E-voting is one
of them and motivated our developement of a new cryptographic library,
entirely implemented in JavaScript. The use of the JavaScript engine was
the most convenient choice for computing on the client side of web
applications: a JavaScript engine is provided with all major browsers. On
the one hand, despite tremendous improvements during the last two years,
the performance of JavaScript code remains extremely low compared to
optimized compiled code executed on the same computer. On the other hand,
compared to other slow platforms like smart-cards, browsers offer an
amount of memory that is larger by orders of magnitude. Such constraints
motivated our study. We chosed to implement elliptic curve operations,
using precomputation to accelerate the point multiplication methods. We
also studied and optimized the performance of the different field
operations that are involved in elliptic curve arithmetic. Our results are
promising and the proposed libraries can be used in a wide range of
applications besides e-voting."

---
