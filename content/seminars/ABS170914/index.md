---
title: Spectral clustering techniques for biological data
slug: ABS170914-Spectral-clustering-techniques-for-biological-data
draft: false

# name of the speaker
speaker: "Nicolas Matz"

# date of the seminar
seminarDate: 2014-09-17T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_170914.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105) Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Earlier biological studies have shown a strong correlation between the structure of the nucleolus of a cell and the potential diseases affecting this cell. The biologist have generated a database by annihilating some specific genes of the cells and they have visually observed different conformations of the nucleolus. They are interested in grouping (clustering) the cells based on the conformation of their nucleolus. During this presentation, the spectral clustering techniques will be presented and applied on the biological data. After an image analysis processing, each cell is represented by a characteristics' vector. Those vectors are used to build a similarity matrix. The spectral graph theory is then performed to cluster the data. Results on both artificial and real data will be discuss."

---
