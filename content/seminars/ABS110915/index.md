---
title: Fusion-based techniques in computer vision
slug: ABS110915-Fusion-based-techniques-in-computer-vision
draft: false

# name of the speaker
speaker: "Cosmin Ancuti"

# date of the seminar
seminarDate: 2015-09-11T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Image fusion is a well-studied process that aims to blend seamlessly the input information by preserving only the specific features of the composite output image. In general image fusion combines multiple-source complementary imagery in order to enhance the information apparent in the respective source images, as well as to increase the reliability of interpretation and classification."

---
