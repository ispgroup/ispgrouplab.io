---
title: High-Performance Wireless Sensing with Low-Complexity Array Measurements
slug: ABS130318-High-Performance-Wireless-Sensing-with-Low-Complexity-Array-Measurements
draft: false

# name of the speaker
speaker: "Dr.-Ing. Manuel S. Stein(Mathematics Department, Vrije Universiteit Brussel, Belgium,& Chair for Stochastics, Universität Bayreuth, Germany)"

# date of the seminar
seminarDate: 2018-03-13T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "While the technological capabilities regarding digital data transmission, storage, and computation have exponentially increased during the last decades, the advances associated with analog wireless equipment were only moderate. Therefore, today hardware cost and power consumption of radio interfaces form the main obstacles for constructing future wireless systems featuring either ultra-low complexity or ultra-high performance. However, in the advent of the Internet of things (IoT), where cheap and small devices are supposed to perform wireless sensing, and with the increasing demands for performance in critical infrastructure applications, it is inevitable further pushing radio technology towards these extremes."

---
