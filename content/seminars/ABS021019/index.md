---
title: Can we scale open-source development by removing the need for iconic and self-sacrificing leaders?"  (slides)
slug: ABS021019-Can-we-scale-open-source-development-by-removing-the-need-for-iconic-and-self-sacrificing-leaders?"--(slides)
draft: false

# name of the speaker
speaker: "Simon Carbonnelle"

# date of the seminar
seminarDate: 2019-10-02T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_021019.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Today's most popular end-user applications are owned and controlled by private companies (e.g. Facebook, Youtube, Reddit, Gmail, Github,...)."

---
