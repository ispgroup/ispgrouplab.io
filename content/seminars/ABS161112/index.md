---
title: Measuring the brain connectivity
slug: ABS161112-Measuring-the-brain-connectivity
draft: false

# name of the speaker
speaker: "Maxime Taquet (ICTEAM/ELEN, UCL)"

# date of the seminar
seminarDate: 2012-11-16T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: ISPS161112.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "For decades, brain diseases have been studied in terms of abnormalities in particular regions. However, neurologists and psychiatrists faced challenges when attempting to elucidate the causes and mechanisms of some mental illnesses that do not seem to be bound to a particular locus. Among them, autism and schizophrenia are now identified as diseases which affect the connections -rather than the regions- of the brain. During this seminar, you will learn how brain connections can be detected, characterized and compared, based on medical images. Recent advances in brain imaging and connectivity modeling, as well as their applications to population studies will be presented."

---
