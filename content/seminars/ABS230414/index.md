---
title: Sensing matrix design criteria for adaptive compressed sensing
slug: ABS230414-Sensing-matrix-design-criteria-for-adaptive-compressed-sensing
draft: false

# name of the speaker
speaker: "Valerio Cambareri (U. Bologna, Italy)"

# date of the seminar
seminarDate: 2014-04-23T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_230414.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105) Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "The quest for optimal sensing operators is crucial in the design of efficient architectures that perform compressive sampling of analog signals. While exact theoretical results exist for general sensing operators that guarantee the recovery of sparse signals, such guarantees are strict and often neglected in practical implementations. Moreover, natural signals are only approximately sparse, but often exhibit correlation properties that suggest a further possible optimization of the sensing operator under some signal-domain priors."

---
