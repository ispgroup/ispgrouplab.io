---
title: "A 1-bit quantized compressive scheme for RADAR: From the hardware implementation to the signal model"
slug: "ABS201016-A-1-bit-quantized-compressive-scheme-for-RADAR:-From-the-hardware-implementation-to-the-signal-model"
draft: false

# name of the speaker
speaker: "Thomas Feuillen"

# date of the seminar
seminarDate: 2016-10-20T16:15:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "This seminar studies a 1-bit Quantized Compressive Sensing (QCS) scheme of a radar signal receiver that could enable novel systems with reduced complexities or cheaper design compared to high resolution strategies. In particular, the range of a sparse set of targets is estimated with a dense or randomized Stepped Frequency Modulation (SFM) with an acquisition process limited to only two 1-bit Analog to Digital Converters (ADCs) per antenna, one for each \"I\" and \"Q\" channels. This seminar will start by reviewing the basic principles of radar and the challenges related to the implementation. The signal model and its link to compressive sensing are then introduced. Second, we develop a complex variant of the Binary Iterative Hard Thresholding algorithm, or CBIHT, for the estimation of a sparse set of targets from complex QCS radar observations. Next, we show that the proposed 1-bit QCS framework reaches estimation errors similar to those obtained by both the common Maximum Likelihood Estimation (MLE) approach and the Iterative Hard Thresholding (IHT) algorithm when applied on full resolution radar observations. Finally, the feasibility of the QCS approach is studied over two different experimental setups in real conditions, with various sizes and quality."

---
