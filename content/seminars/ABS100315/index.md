---
title: Post-Reconstruction Deconvolution of PET Images by Total Generalized Variation Regularization
slug: ABS100315-Post-Reconstruction-Deconvolution-of-PET-Images-by-Total-Generalized-Variation-Regularization
draft: false

# name of the speaker
speaker: "Stéphanie Guérit"

# date of the seminar
seminarDate: 2015-03-10T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_100315.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Improving the quality of positron emission tomography (PET) images, affected by low resolution and high level of noise, is a challenging task in nuclear medicine and radiotherapy. The aim of this talk is to present a restoration method, achieved after tomographic reconstruction of the images and targeting clinical situations where raw data are often not accessible. This method relies on classical convex optimization tools and on inverse problem theory. The recently developed concept of total generalized variation (TGV) is introduced in the problem formulation to regularize PET image deconvolution. Some properties specific to PET imaging (such as positivity and photometry invariance) are also added to the model to stabilize the restoration. Theoretical results will be illustrated by experiments on both synthetic data and real patient images."

---
