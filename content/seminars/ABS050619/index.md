---
title: "The lensless endoscope: a playground for acquisition schemes based on compressed sensing principles"
slug: ABS050619-The-lensless-endoscope:-a-playground-for-acquisition-schemes-based-on-compressed-sensing-principles
draft: false

# name of the speaker
speaker: "Stéphanie Guérit"

# date of the seminar
seminarDate: 2019-06-05T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_050619.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Can you conceive of imaging brain activity at a cellular level using an extra thin probe built with an optical fiber? Researchers from Institut Fresnel in Marseille are currently working on designing such a device, called lensless endoscope (LE). During this talk, I will present a joint work initiated one year ago. The aim of this collaboration is to design new acquisition scheme inspired by compressed sensing theory. Traditionally, data are acquired using raster scanning method: each discrete localization of the field of view is illuminated sequentially with a focused light beam, and the fluorescence signal is then collected back in the fiber. This produces satisfactory images that does not need post-processing but require a time-consuming calibration step and a high number of measurements. The proposed acquisition framework is based on two key elements: (i) the ability to produce pseudo-random illumination patterns by removing the calibration step and (ii) the memory effect of the fiber, i.e., the ability to shift the illumination pattern in the field of view thanks to mirror galvanometer. It allows us to reach reconstruction quality similar to the traditional framework but using far less measurements. I will describe this framework, the considered sampling strategies and some numerical considerations (e.g., the use of cross-validation to set the main parameter of the reconstruction algorithm). Finally, I will show and comment some results on both synthetic and experimental data."

---
