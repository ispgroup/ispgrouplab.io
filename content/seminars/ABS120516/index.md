---
title: Dimensionality Reduction with t-SNE for data visualization
slug: ABS120516-Dimensionality-Reduction-with-t-SNE-for-data-visualization
draft: false

# name of the speaker
speaker: "Simon Carbonnelle"

# date of the seminar
seminarDate: 2016-05-12T14:15:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_120516.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "The t-SNE method addresses the problem of reducing the dimensionality of data to 2 or 3 dimensions while keeping as much local and global structure as possible. The main purpose of this dimensionality reduction technique is enabling human interpretation of datasets via 2D or 3D plots.  t-SNE is based on the Stochastic Neighbor Embedding (SNE) method. The main idea of those methods is to model the pairwise similarity of data points in the initial data space and in the low dimensional target space through conditional or joint probability distributions. The coordinates of the data points in the new low-dimensional space are then calculated such that the Kullback-Leibler divergence between the similarity models in high and low resolution is minimized. This forces the new representation to have approximately the same pair-wise similarities as the high-dimensional representation. t-SNE has gained a lot of popularity to understand representations, usually learned by deep learning methods. Results for word and document embeddings (NLP) as well as for image classification will be presented."

---
