---
title: Image Denoising using Non-Local Wavelet Bases
slug: ABS041110-Image-Denoising-using-Non-Local-Wavelet-Bases
draft: false

# name of the speaker
speaker: "Laurent Jacques"

# date of the seminar
seminarDate: 2010-11-04T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: SPS-041110-LJ.pdf.zip

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "None"

---
