---
title: "Topological and molecular characterization of pancreatic endothelial cells: a prerequisite for tissue bioprinting"
slug: "ABS110220-Topological-and-molecular-characterization-of-pancreatic-endothelial-cells-a-prerequisite-for-tissue-bioprinting"
draft: false

# name of the speaker
speaker: "Laura Glorieux (PhD student at De Duve, UCLouvain) (Invited talk)"

# date of the seminar
seminarDate: 2020-02-11T13:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: ABS110220_slides.pdf

# Location of the seminar
location: "Shannon seminar Room (Place du Levant 3, Maxwell Building, 1st floor)" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Developing therapies for pancreatic diseases is hampered by limited access to pancreatic tissue in vivo and engineered 3D tissue models have great potential for physiopathological and pharmaceutical research.  An important and limiting step in tissue engineering is tissue vascularization. This project is part of the Pan3DP consortium that aims to bioprint murine embryonic pancreatic tissue units. The first step of this project is to collect information on the 3D architecture and transcriptomic profile of a developing pancreas, then to bioprint pancreatic tissue units based on a digital 3D pancreatic atlas, and finally to mature these units into functional tissue under custom-optimized in vitro conditions. Here, we focus on the spatial characterization of the endothelial compartment and on the integration of the vascular network in bioprinted construct."

---
