---
title: Feature Point-Based 3D Mesh Watermarking that Withstands the Cropping Attack
slug: ABS040211-Feature-Point-Based-3D-Mesh-Watermarking-that-Withstands-the-Cropping-Attack
draft: false

# name of the speaker
speaker: "Mireia Montanola"

# date of the seminar
seminarDate: 2011-02-04T10:30:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: SPS040211.pdf.zip

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "State-of the-art robust 3D watermarking schemes already withstand
combinations of a wide variety of attacks (e.g. noise addition,
simplification, smoothing, etc). Nevertheless, there are practical
limitations of existing 3D watermarking methods due to their extreme
sensitivity to cropping. Spread Transform Dither Modulation (STDM) method
is an extension of Quantization Index Modulation (QIM). Besides the
simplicity and the trade-off between high capacity and robustness provided
by QIM methods, it is also resistant against re-quantization. This paper
focuses on two state-of-theart techniques which offer different and
complementary advantages, respectively QIM-based 3D watermarking and
feature point-based watermarking synchronization. The idea is to combine
both in such a way that the new scheme would benefit from the advantages
of both techniques and compensate for their respective fragilities. The
resulting scheme does not make use of the original 3D model in detection
but of some parameters as side-information. We show that robustness
against cropping and other common attacks is achieved provided that at
least one feature point as well as its corresponding local neighborhood is
retrieved."

---
