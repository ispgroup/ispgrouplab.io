---
title: A virtual tour of free viewpoint rendering
slug: ABS181212-A-virtual-tour-of-free-viewpoint-rendering
draft: false

# name of the speaker
speaker: "Cédric Verleysen (ICTEAM/ELEN, UCL)"

# date of the seminar
seminarDate: 2012-12-18T15:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_181212.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "A principal limitation of the conventional video technology is that the viewpoint is fixed by the camera that captures a scene.
Since the last decade, researchers have tried to overcome this limitation by allowing a remote viewer to interactively navigate across the scene.
The potential applications of such free viewpoint rendering are tremendous, ranging from 3D TV to scene analysis, and from interactive media, augmented reality to video content/game production, etc."

---
