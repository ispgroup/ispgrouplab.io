---
title: "Diffusion MRI and DKI to evaluate the axonal degeneration in vivo: general framework and focus on some processing tools"
slug: "ABS220114-Diffusion-MRI-and-DKI-to-evaluate-the-axonal-degeneration-in-vivo:-general-framework-and-focus-on-some-processing-tools"
draft: false

# name of the speaker
speaker: "Stéphanie Guérit (ICTEAM/ELEN, UCL)"

# date of the seminar
seminarDate: 2014-01-22T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_220114.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 40

# Abstract of the seminar
abstract: "A lot of incidents in everyday life can lead to spinal cord injuries: motor vehicle accidents, falls, sport injuries, etc. In some cases, the consequences are superficial or reversible but they can also have an important impact on the quality of life of the patients by the permanent loss of functionality. At this time, the process of axonal degeneration in the central nervous system (CNS), responsible for this functional loss, is not well known. The current clinical methods to evaluate the extent of the damage are based on the sensorial perception of the patient and its ability to contract some of his muscles. There exists no objective method based on the observation of physiological processes."

---
