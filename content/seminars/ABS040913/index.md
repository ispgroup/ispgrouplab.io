---
title: "Computational inverse scattering and TV regularization: A cocktail party explanation"
slug: ABS040913-Computational-inverse-scattering-and-TV-regularization:-A-cocktail-party-explanation
draft: false

# name of the speaker
speaker: "Augustin Cosse (ICTEAM/ELEN, UCL)"

# date of the seminar
seminarDate: 2013-09-04T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Imaging by inverse wave scattering is a multidisciplinary area implying
concepts from both physics, mathematics and signal processing. Current
approaches to the problem fall into two categories : inverse obstacle
problem and inverse medium problem. In the former, the scattering object
is a homogeneous obstacle one wants to determine from the field on the
boundary. In the latter, the scattering object is an inhomogeneous medium
with respect to some physical parameters. The inverse problem then
consists in estimating those parameters from the field on some boundary.
The talk will be focusing on the second problem. Such problem is usually
not only non-linear but can also be severely ill-posed. Moreover,
obtaining accurate solutions usually requires huge datasets. In the light
of recent developments in the compressive sensing (CS) theory together
with the increased computing capabilities, we try to obtain high accuracy
images of an object with respect to variations in the physical parameters.
This is done using somehow advanced understanding of the wave propagation
physics while at the same time looking for a decrease in the complexity of
current methods."

---
