---
title: Optimal Dense Disparity Map Quantization and Residual Prediction for Lossless Stereo Image Coding".
slug: ABS151210-Optimal-Dense-Disparity-Map-Quantization-and-Residual-Prediction-for-Lossless-Stereo-Image-Coding".
draft: false

# name of the speaker
speaker: "Amit Kumar K.C."

# date of the seminar
seminarDate: 2010-12-15T10:30:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: SPS151210.pdf

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "This presentation is based on my thesis work, which is compression of stereo images."

---
