---
title: "Compressed Sensing of Low Complexity High Dimensional Data: Application to Hyperspectral Imaging"
slug: ABS061113-Compressed-Sensing-of-Low-Complexity-High-Dimensional-Data:-Application-to-Hyperspectral-Imaging
draft: false

# name of the speaker
speaker: "Kévin Degraux (ICTEAM/ELEN, UCL)"

# date of the seminar
seminarDate: 2013-11-06T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_061113.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "In numerous applications, acquiring and processing High Dimensional (HD) data is challenging. 3-dimensional imaging and in particular Hyperspectral Imaging (HSI) is an example where acquiring a whole volume made up of bilions of voxels is a hard task. Fortunately, in that case and in many others, the data volume is most often highly structured and full of redundancy. This framework is the ideal field of application for Compressed Sensing (CS). That theory allows to exploit these redundancy priors to blindly compress data at the analog acquisition stage. Provided that the right priors are used and that the CS sensor model respects some mathematical properties, we can strongly reduce the sampling rate (w.r.t. Nyquist rate) or equivalently increase target resolution and focus on reconstruction afterwards. In this talk, I will describe at a high level of abstraction every step of the CS acquisition and reconstruction of HD data and in particular Hyperspectral images with classical and more exotic priors like low rank or hybrid models."

---
