---
title: "Computational Imaging with Plug-and-play priors: Leverage the Power of Deep Learning"
slug: "ABS091220-Computational-Imaging-with-Plug-and-play-priors:-Leverage-the-Power-of-Deep-Learning"
draft: false

# name of the speaker
speaker: "Xiaojian Xu (Washington University in St. Louis)(Invited talk)"

# date of the seminar
seminarDate: 2020-12-09T16:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: ABS091220_slides.pdf

# Location of the seminar
location: "(Online) Microsoft Teams" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Plug-and-play priors (PnP) is a methodology for regularized image reconstruction that specifies the prior through an image denoiser. Unlike traditional regularized inversion, PnP does not require the prior to be expressible in the form of a regularization function. This flexibility, on one hand,  enables PnP algorithms to exploit the most effective image denoisers, such as the ones based on convolutional neural networks (CNNs), leading to their state-of-the-art performance in various imaging tasks.  While on the other hand, it also makes it hard to establish the theoretical analysis on the convergence for an arbitrary denoiser. So in this talk, we will show some recent achievement about PnP regarding these issues. We will introduce a denoiser scaling technique, which in practice could greatly enhance the performance and flexibility of PnP for various denoisers, especially those CNN-based ones. We will also introduce the first theoretical convergence result for the iterative shrinkage/thresholding algorithm (ISTA) variant of PnP for MMSE denoisers,  where iterates produced by PnP-ISTA with an MMSE denoiser converge to a stationary point of some global cost function."

---
