---
title: On the use of Gabor Filters in SOS Corrected Ultrasound Image Segmentation
slug: ABS121211-On-the-use-of-Gabor-Filters-in-SOS-Corrected-Ultrasound-Image-Segmentation
draft: false

# name of the speaker
speaker: "Augustin Cosse"

# date of the seminar
seminarDate: 2011-12-12T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "In many surgery centers, reduced quality images are acquired during surgery in order to track the tissue deformation
in real time.  Among those intraoperative imaging modalities, ultrasounds are the subject of an increasing amount of research
and the detection of organ contours in those images thus constitutes a point of interest. A new approach for
correcting and segmenting US images will be presented. This approach is based on a speed of sound (SOS) correction
followed by Gabor filtering and SVM-based classification of the corrected image voxels.
A brief introduction to the physics of ultrasound shall be provided as well."

---
