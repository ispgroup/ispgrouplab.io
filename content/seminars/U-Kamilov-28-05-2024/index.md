---
title: "Computational Imaging: Restoration Deep Networks as Implicit Priors"
slug: "U-Kamilov-28-05-2024"
draft: false

# name of the speaker
speaker : Ulugbek Kamilov (Washington University, St. Louis, USA) 

# date of the seminar
seminarDate: 2024-05-28T14:00:00          

# external link (eg web page of the speark)
externalLink: https://engineering.wustl.edu/faculty/Ulugbek-Kamilov.html

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files : []

# Location of the seminar
location: "Shannon seminar room (Maxwell)"

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: Many interesting computational imaging problems can be formulated as imaging inverse problems. Since these problems are often ill-posed, one needs to integrate all the available prior knowledge for obtaining high-quality solutions. This talk will explore a series of techniques that leverage deep neural networks for image restoration as data-driven, implicit priors for images. The methods discussed originate from the well-known plug-and-play (PnP) methodology, known for its effectiveness in addressing imaging inverse problems. We will extend the conversation to the generalization of PnP methods, moving beyond traditional use of additive white Gaussian noise (AWGN) denoisers to include a variety of other restoration networks. This expansion not only enhances imaging performance but also offers the flexibility to train priors in the absence of clean data. Additionally, the talk will cover the theoretical underpinnings of using deep restoration networks and their applications in biomedical image reconstruction. 
---

**Biography**: Ulugbek Kamilov is the Director of Computational Imaging Group and an Associate Professor of Electrical & Systems Engineering and Computer Science & Engineering at Washington University in St. Louis. He is currently a Visiting Professor at the Data Science Center at École Normale Supérieure in Paris. He obtained the BSc/MSc degree in Communication Systems in 2011 and the PhD degree in Electrical Engineering in 2015 from EPFL. He was a Visiting Research Faculty at Google Research in 2023-2024 and a Research Scientist at Mitsubishi Electric Research Laboratories in 2015-2017. He was an Exchange Student at Carnegie Mello University in 2008, Visiting Student Researcher at MIT in 2011, and Visiting Scholar at Stanford University in 2013.

He is a recipient of the NSF CAREER Award and the IEEE Signal Processing Society’s 2017 Best Paper Award. He was among 55 early-career researchers in the USA selected as a Fellow for the Scialog initiative on “Advancing Bioimaging” in 2021. His PhD thesis was selected as a finalist for the EPFL Doctorate Award in 2016. He was awarded Outstanding Teaching Award from the Department of Electrical & Systems Engineering at WashU in 2023. He is currently a Senior Member of the Editorial Board of IEEE Signal Processing Magazine and is on IEEE Signal Processing Society’s Bioimaging and Signal Processing Technical Committee. He has previously servedas an Associate Editor of IEEE Transactions on Computational Imaging and on IEEE Signal Processing Society’s Computational Imaging Technical Committee.
