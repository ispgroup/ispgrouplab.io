---
title: Using Approximate Computing to Improve the Efficiency of LSTM Neural Networks
slug: ABS040320-Using-Approximate-Computing-to-Improve-the-Efficiency-of-LSTM-Neural-Networks
draft: false

# name of the speaker
speaker: "S. Abolfazl Ghasemzadeh"

# date of the seminar
seminarDate: 2020-03-04T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: ABS040320_slides.pdf

# Location of the seminar
location: "Shannon seminar Room (Place du Levant 3, Maxwell Building, 1st floor)" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "As the growing field of Artificial Neural Networks, Recurrent Neural Networks are often used for sequence-related applications. Long Short-Term Memory (LSTM) neural networks are improved and widely used versions of Recurrent Neural Networks. To achieve high accuracy, researchers always build large-scale LSTM networks which are time-consuming and power-consuming."

---
