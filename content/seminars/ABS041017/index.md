---
title: Dedicated image processing tools for proton imaging
slug: ABS041017-Dedicated-image-processing-tools-for-proton-imaging
draft: false

# name of the speaker
speaker: "Sylvain Deffet"

# date of the seminar
seminarDate: 2017-10-04T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Thanks to the finite range of the protons, proton-therapy is of undeniable therapeutic interest. However, proton treatment planning suffers from uncertainties in the computation of the protons range. As a means of measuring the proton energy loss along the path of the beam, proton radiography could be used to significantly increase the accuracy of proton therapy. We developed with this aim in mind a new prototype of proton imaging system based on multi-layer ionization chambers. The first tests on patients are planned end of this year."

---
