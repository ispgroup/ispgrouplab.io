---
title: '"Screening" and beyond: A strategy to deal with large-scale optimization problems'
slug: C-Herzet-23-08-23-Screening-and-beyond
draft: false

# name of the speaker
speaker: "Cédric Herzet (INRIA/IRMAR - UMR 6625, France)"

# date of the seminar
seminarDate: 2023-08-23T11:00:00

# external link (eg web page of the speark)
externalLink: "https://people.rennes.inria.fr/Cedric.Herzet/Cedric.Herzet/Main.html"

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
# files:

# Location of the seminar
location: "Euler Seminar Room (A002)" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "In my presentation, I will develop the concept of « safe screening », a technique to reduce the dimensionality of large-scale optimization problems that exploits the sparsity of the solutions. I will first explain the fundamental ingredients of screening and highlight some recent advances in this field. In a second part, I will present how the concept of screening can be extended by exploiting partial knowledge about the solution of the optimization problem."

---
