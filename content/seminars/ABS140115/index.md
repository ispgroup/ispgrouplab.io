---
title: Normalized cuts for unsigned and signed graphs
slug: ABS140115-Normalized-cuts-for-unsigned-and-signed-graphs
draft: false

# name of the speaker
speaker: "Amit Kumar"

# date of the seminar
seminarDate: 2015-01-14T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_140115.zip

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "In this talk, I will first rapidly recall few terminologies of graph theory and then talk about the normalized cuts in a graph due to Shi and Malik.
Then, I will discuss of how we can generalize to handle signed graphs, in which the weights can be negative too."

---
