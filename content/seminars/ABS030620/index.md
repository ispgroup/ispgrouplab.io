---
title: Synthetic corruption of images for anomaly detection using autoencoders
slug: ABS030620-Synthetic-corruption-of-images-for-anomaly-detection-using-autoencoders
draft: false

# name of the speaker
speaker: "Anne-Sophie Collin"

# date of the seminar
seminarDate: 2020-06-03T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: ABS030620_slides.pdf

# Location of the seminar
location: "(Online) Microsoft Teams" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Anomaly detection can be defined as the process of identifying rare items, events or observations that differ significantly from the majority of the data. In industrial vision, this problem can be addressed with an autoencoder trained to map an arbitrary image (with or without any defect) to a clean image (without any defect). In this approach, anomaly detection relies conventionally on the reconstruction residual or, alternatively, on the reconstruction uncertainty. The higher the reconstruction residual/uncertainty, the higher the likelihood of a region being defective."

---
