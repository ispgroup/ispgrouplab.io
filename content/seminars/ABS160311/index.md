---
title: Scale & Affine Invariant Interest Point Detectors
slug: ABS160311-Scale-&-Affine-Invariant-Interest-Point-Detectors
draft: false

# name of the speaker
speaker: "Rémy Labbé"

# date of the seminar
seminarDate: 2011-03-16T10:30:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_160311.pdf

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Local features have been shown to be well suited to matching and recognition as well as to many other applications as they are robust to occlusion, background clutter and other content changes. The difficulty is to obtain invariance to viewing conditions. This SPS presents a scale and affine invariant detection algorithm test bench for particle disease localization and extraction in the context of infected hip prosthesis. Infections are observed on 2D X-ray images. The detector is based on the following results : (1) Interest points extracted with the Harris detector can be adapted to affine transformations and give repeatable results (geometrically stable). (2) The characteristic scale of a local structure is indicated by a local extremum over scale of normalized derivatives (the Laplacian). (3) The affine shape of a point neighborhood is estimated based on the second moment matrix."

---
