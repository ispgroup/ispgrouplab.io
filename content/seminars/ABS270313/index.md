---
title: A Deconvolution Problem in Astronomy
slug: ABS270313-A-Deconvolution-Problem-in-Astronomy
draft: false

# name of the speaker
speaker: "Adriana Gonzalez"

# date of the seminar
seminarDate: 2013-03-27T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_270313.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Optical sensors distort the observation of an object by the imperfection of their elements and by the natural physics of the observation. The acquired image is frequently corrupted by the noise coming from the sensor itself and by the Point Spread Function (PSF) of the instrument, which filters the hypothetically pure image. In most situations, we do not have any knowledge on this PSF, hence we face a blind deconvolution problem when estimating the pure image."

---
