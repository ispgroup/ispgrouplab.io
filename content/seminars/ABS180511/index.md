---
title: "Imaging techniques for LOW-DOSE and COLOR tomography: new challenges for Cone-Beam CT and PET with hybrid pixels"
slug: ABS180511-Imaging-techniques-for-LOW-DOSE-and-COLOR-tomography:-new-challenges-for-Cone-Beam-CT-and-PET-with-hybrid-pixels
draft: false

# name of the speaker
speaker: "Prof Yannick Boursier (Université Aix-Marseille 2 / ESIL)"

# date of the seminar
seminarDate: 2011-05-18T10:30:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_180511.pdf

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Cone Beam Computerized Tomography (CBCT) and Positron Emission Tomography (PET) Scans are medical imaging devices that require solving ill-posed inverse problems. 
The models considered come directly from the physics of the acquisition devices, and take into account the specificity of the Poisson noise.
We propose various fast numerical schemes to compute the solution. In particular, we show that a new algorithm recently introduced by A. Chambolle and T. Pock is well suited in the PET case when considering non differentiable regularizations such as total variation or wavelet l1-regularization. Numerical experiments indicate that the proposed algorithms compare favorably with respect to well-established methods in tomography.
Moreover, the new generation of hybrid pixel detectors working in a photon counting mode makes possible to select photons according to their energy, thus offering the opportunity to go beyond the classical reconstruction techniques that provides an absorption reconstruction volume. We will study the open questions of reconstructing colored X-ray volumes."

---
