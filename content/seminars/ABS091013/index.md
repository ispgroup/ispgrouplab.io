---
title: Continuous parameter estimation from compressive samples
slug: ABS091013-Continuous-parameter-estimation-from-compressive-samples
draft: false

# name of the speaker
speaker: "Prasad Sudhakar (ICTEAM/ELEN, UCL)"

# date of the seminar
seminarDate: 2013-10-09T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_091013.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "For several applications, it is sufficient only to extract a few parameters of a signal, from its compressive measurements, instead of having a full reconstruction, thereby saving a lot of computational effort. Often, the underlying parameters that characterize the signal are drawn from a continuous space. However, the standard compressive sensing formalism is discrete in nature and hence the parameter estimates are confined to a predefined grid. In order to go off the grid, one has to exploit the underlying continuous model and perform either gradient descent or interpolation. In this talk, I will consider a very simple signal model and describe how to estimate continuous parameters from compressive samples."

---
