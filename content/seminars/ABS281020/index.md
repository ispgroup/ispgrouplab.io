---
title: Recovery guarantees for linear and non-linear reconstruction from Walsh measurements
slug: ABS281020-Recovery-guarantees-for-linear-and-non-linear-reconstruction-from-Walsh-measurements
draft: false

# name of the speaker
speaker: "Laura Thesing (University of Cambridge)(Invited talk)"

# date of the seminar
seminarDate: 2020-10-28T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: ABS281020_slides.pdf

# Location of the seminar
location: "(Online) Microsoft Teams" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "In this talk we discuss recovery guarantees for the reconstruction of wavelet coefficients from Walsh measurements, which appear in fluorescence microscopy, lensless cameras and other analogue measurement advices with an “on-off” behaviour. We consider both linear and non-linear reconstruction methods. For the analysis we discuss the typical structure of signals under the wavelet transform and the properties of the change of basis matrix. The results offer a guideline on the choice of sampling pattern and an insight to the relationship between Walsh functions and wavelets."

---
