---
title: Sensing-based Resource Allocation in Cognitive Radio Networks
slug: ABS191115-Sensing-based-Resource-Allocation-in-Cognitive-Radio-Networks
draft: false

# name of the speaker
speaker: "Dr Nafiseh Janatian (DIGICOM, ICTEAM, UCL)"

# date of the seminar
seminarDate: 2015-11-19T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_191115.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "One of the main requirements of cognitive radio (CR) systems is the ability to reliably detect the presence of licensed primary users. After determining the availability of the licensed spectrum bands, CR users must select appropriate transmission parameters to better utilize these bands and avoid possible interference to the licensed users. In this presentation, the problem of joint sensing and resource allocation with the goal of energy consumption minimization in code-division multiple access (CDMA)-based CR networks is addressed. A network consisting of multiple secondary users (SUs) and a secondary base station (BS) implementing a two-phase protocol is considered. In the first phase, censor-based cooperative spectrum sensing is carried out to detect the PU’s presence. When the channel is estimated to be free, SUs transmit data in the uplink to the BS in the second phase by using CDMA. The sensing parameters and transmit power of SUs are optimized jointly to minimize the total energy consumption with the constraints on SUs’ quality of service (QoS) and detection probability of the PU. Furthermore, the separate optimization problem in which the sensing parameters of SUs are set regardless of the allocated resources is studied. The energy saving of joint versus separate optimization is investigated using numerical experiments. Numerical results show that joint optimization can save the energy consumption significantly in comparison with separate optimization in low sensing SNRs."

---
