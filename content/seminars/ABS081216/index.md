---
title: Deep learning and structured output problems
slug: ABS081216-Deep-learning-and-structured-output-problems
draft: false

# name of the speaker
speaker: "Soufiane Belharbi (INSA, Rouen, France)"

# date of the seminar
seminarDate: 2016-12-08T16:15:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_081216.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Deep neural networks have shown to be efficient models for learning 
complex mapping functions. It is known that adding more layers improves 
the performance but it comes with the cost of adding more parameters 
which requires more training labeled data. Pre-training techniques have 
shown to be helpful in training deeper networks by exploiting unlabeled 
data. However, in practice, this technique requires a lot of effort to 
setup the raised hyper-parameters. We present in this talk a 
regularization scheme to alleviate this problem. We extend our approach 
for structured output problems."

---
