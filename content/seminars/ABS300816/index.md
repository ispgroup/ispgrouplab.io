---
title: Inner Ear modelling with MRI
slug: ABS300816-Inner-Ear-modelling-with-MRI
draft: false

# name of the speaker
speaker: "Dr Jérôme Plumat (University of Auckland, New Zealand)  (invited talk)"

# date of the seminar
seminarDate: 2016-08-30T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "The inner ear (IE) is one of the most controlled organ, it is mostly fluid dominated and the barrier between blood and inner compartment is very tight. In this talk we will present recent results to quantify the permeability of the blood labyrinth barrier (BLB) and fluid mechanism parameters using the Dynamic Contrast Enhancement MRI (DCE-MRI). This safe imaging technic helps us to understand the IE, the transferts between blood and inner compartments as well as the propagation inside the cochlea. Also, we are able to estimate the BLB permeability and comparing control with patients with Ménière disease in order to understand more about this IE disorder. DCE-MRI models and methods will be discussed in this particular context."

---
