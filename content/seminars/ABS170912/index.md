---
title: Diffusion tensor Imaging and challenges to evaluate spinal repair
slug: ABS170912-Diffusion-tensor-Imaging-and-challenges-to-evaluate-spinal-repair
draft: false

# name of the speaker
speaker: "Damien Jacobs"

# date of the seminar
seminarDate: 2012-09-17T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_170912.pdf

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Diffusion tensor imaging (DTI) is a non-invasive promising technique for longitudinal study in the biomedical field. Physician and pharmacists apply this biomarker to evaluate the efficiency of drugs and to improve the analysis of preclinical experiments such as spinal repair. However, interleaved DTI sequence carried out at ultra high field are impaired by motion and off-resonance effects. In this case, this biomarker can not be validated for biomedical applications. The purpose is to suggest a new strategy for a robust acquisition and to confirm this potential biomarker."

---
