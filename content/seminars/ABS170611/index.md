---
title: Gait feature extraction in Parkinson's disease using low-cost accelerometers
slug: ABS170611-Gait-feature-extraction-in-Parkinson's-disease-using-low-cost-accelerometers
draft: false

# name of the speaker
speaker: "Julien Stamatakis"

# date of the seminar
seminarDate: 2011-06-17T14:30:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "There are currently no validated clinical instruments or device that allow a full characterization of gait disturbances in Parkinson's disease. As a step towards this goal, a four accelerometer-based system is proposed to increase the number of parameters that can be extracted to characterize parkinsonian gait disturbances such as Freezing of Gait or gait asymmetries. After developing the hardware, an algorithm has been developed, that automatically epoched the signals on a stride-by-stride basis and quantified, among others, the gait velocity, the stride time, the stance and swing phases, the single and double support phases or the maximum acceleration at toe-off, as validated by visual inspection of video recordings. The results obtained in a PD patient and a healthy volunteer will be presented."

---
