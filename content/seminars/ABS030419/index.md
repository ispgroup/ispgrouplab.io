---
title: Compressive Learning meets privacy
slug: ABS030419-Compressive-Learning-meets-privacy
draft: false

# name of the speaker
speaker: "Vincent Schellekens"

# date of the seminar
seminarDate: 2019-04-03T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_030419.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Compressive Learning (CL) is a framework where a target learning task (e.g., clustering or density fitting) is not performed on the whole dataset of signals, but on a heavily compressed representation of it (called sketch), enabling training with reduced time and memory resources. Because the sketch only keeps track of general tendencies (i.e., generalized moments) of the dataset while discarding individual data records, previous work argued that CL should protect the privacy of the users that contributed to the dataset, but without providing formal arguments to back up this claim. This work aims to formalize this observation."

---
