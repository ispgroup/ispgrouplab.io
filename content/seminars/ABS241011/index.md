---
title: On the use of 4D imaging in cancer treatment
slug: ABS241011-On-the-use-of-4D-imaging-in-cancer-treatment
draft: false

# name of the speaker
speaker: "Guillaume Janssens"

# date of the seminar
seminarDate: 2011-10-24T10:45:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "This presentation will give a description of the multiple steps and image processing tools needed in the complex workflow that makes use of mutli-modal 4D imaging techniques to provide the needed insight for radiotherapy treatment of cancer."

---
