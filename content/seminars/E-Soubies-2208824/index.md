---
title: "Exact Continuous Relaxations of $L_0$-Regularized Generalized Linear Models"
slug: "E-Soubies-2208824"
draft: false

# name of the speaker
speaker : Emmanuel Soubies (IRIT, CNRS, Université de Toulouse) 

# date of the seminar
seminarDate: 2024-08-22T14:00:00      

# external link (eg web page of the speark)
externalLink: https://www.irit.fr/~Emmanuel.Soubies/

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files : []

# Location of the seminar
location: "Euler seminar room (a.002)"

# duration of the seminar in minutes
duration: 50

# Abstract of the seminar
abstract: Sparse generalized linear models are widely used in fields such as statistics, computer vision, signal/image processing and machine learning. The natural sparsity promoting regularizer is the $L_0$ pseudo-norm which is discontinuous and non-convex. In this talk, we will present the $L_0$-Bregman relaxation (B-Rex), a general framework to compute exact continuous relaxations of such $L_0$-regularized criteria. Although in general still non-convex, these continuous relaxations are qualified as exact in the sense that they let unchanged the set of global minimizer while enjoying a better optimization landscape. In particular, we will show that some local minimizers of the initial functional are eliminated by these relaxations. Finally, these properties will be illustrated on both sparse Kullback-Leibler regression and sparse logistic regression problems. This is joint work with M'hamed Essafri and Luca Calatroni. 
---

