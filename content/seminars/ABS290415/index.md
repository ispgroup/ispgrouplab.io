---
title: Learning Deep Neural Network
slug: ABS290415-Learning-Deep-Neural-Network
draft: false

# name of the speaker
speaker: "Romain Hérault, Maître de conférences, INSA, Rouen, France (invited talk)"

# date of the seminar
seminarDate: 2015-04-29T10:45:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "In this talk we will addresses the problem of learning Deep Neural Network (DNN) through the use of smart initializations or regularizations. Moreover, we will look at recent applications of DNN to structured output problems (such as image labeling or facial landmark detection).

- Introduction to supervised learning, why using regularization ? why looking for sparsity ?
- Introduction to perceptron, multilayer perceptron and back-propagation
- Deep Neural Network and the vanishing gradient problem
- Smart initializations and topologies (stacked autoencoders, convolutional neural networks)
- Regularizing (denoising and contractive AE, dropout, multi-obj)
- Deep architecture for high dimensional output or structured output problems "

---
