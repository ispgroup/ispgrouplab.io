---
title: Solving imaging problem with Graph Cuts
slug: ABS040311-Solving-imaging-problem-with-Graph-Cuts
draft: false

# name of the speaker
speaker: "Jérôme Plumat"

# date of the seminar
seminarDate: 2011-03-04T14:30:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_040311.pdf

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "In this seminar, Jérôme will review the Graph Cuts theory and different Graph Cuts solutions for common image processing problems like image segmentation, volumetric data reconstruction in X-Ray parallel beam computed tomography, and image registrations."

---
