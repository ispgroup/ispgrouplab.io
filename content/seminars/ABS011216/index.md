---
title: An Introduction to Deep Learning
slug: ABS011216-An-Introduction-to-Deep-Learning
draft: false

# name of the speaker
speaker: "Simon Carbonnelle"

# date of the seminar
seminarDate: 2016-12-01T16:15:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_011216.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "In the last 5 years, Deep Learning has revolutionized many fields such as computer vision, speech processing and language modelling. Some works have also shown that Deep Learning methods can be combined with external memories and reason over complex data structures such as graphs. Based on these successes, Deep Learning is considered by many as the most promising approach for Artificial Intelligence. The goal of this talk is to give you a broad, high-level overview of Deep Learning. More specifically, the following three questions will be addressed: What is Deep Learning and how does it work? What is Deep Learning currently capable of? And finally why does it work so well?"

---
