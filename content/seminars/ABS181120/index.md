---
title: Compressive Independent Component Analysis
slug: ABS181120-Compressive-Independent-Component-Analysis
draft: false

# name of the speaker
speaker: "Michael Sheehan (University of Edinburgh)(Invited talk)"

# date of the seminar
seminarDate: 2020-11-18T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: ABS181120_slides.pdf

# Location of the seminar
location: "(Online) Microsoft Teams" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Compressive learning forms the exciting intersection between compressed sensing and statistical learning where one exploits forms of sparsity and structure to reduce space and/or time complexities associated with learning based methods. In this talk, I will discuss the task of independent component analysis (ICA) through the compressive learning lens. In particular, I will show that solutions to the cumulant based ICA model have a particular structure that induces a low dimensional model set that resides in the cumulant tensor space. A compact representation, or a so called sketch, of the ICA cumulants can be formed and it is shown that identifying independent source signals can be achieved with high probability when the compression size is of the optimal order of the ICA model set. Finally, an iterative projection gradient style algorithm is proposed to decode the sketch to obtain the parameters of the ICA model, reducing the space complexity compared to other ICA methods."

---
