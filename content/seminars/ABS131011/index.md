---
title: The use of sparsity hypothesis for source separation
slug: ABS131011-The-use-of-sparsity-hypothesis-for-source-separation
draft: false

# name of the speaker
speaker: "Prasad Sudhakar"

# date of the seminar
seminarDate: 2011-10-13T10:45:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Sparsity has been traditionally exploited for data compression, and a popular hypothesis to solve under-determined linear inverse systems (sparse recovery problem). Considerable amount of work has been done, both theoretical and algorithmic, in this regard. Of late, sparsity is being used to perform more complicated tasks such as source separation, learning, etc. The focus of this talk will be the usage of sparsity hypothesis for source separation."

---
