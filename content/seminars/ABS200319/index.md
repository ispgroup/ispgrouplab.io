---
title: "Quantity over Quality: Dithered Quantization for Compressive Radar Systems"
slug: "ABS200319-Quantity-over-Quality:-Dithered-Quantization-for-Compressive-Radar-Systems"
draft: false

# name of the speaker
speaker: "Thomas Feuillen"

# date of the seminar
seminarDate: 2019-03-20T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Nyquist Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "In this talk, we investigate a trade-off between the number of radar observations (or measurements) and their resolution in the context of radar range estimation. To this end, we introduce a novel estimation scheme that can deal with strongly quantized received signals, going as low as 1-bit per signal sample. We leverage for this a dithered quantized compressive sensing framework that can be applied to classic radar processing and hardware. This allows us to remove ambiguous scenarios prohibiting correct range estimation from (undithered) quantized base-band radar signal. Two range estimation algorithms are studied: Projected Back Projection (PBP) and Quantized Iterative Hard Thresholding (QIHT). The effectiveness of the reconstruction methods combined with the dithering strategy is shown through Monte Carlo simulations. Furthermore we show that: (i), in dithered quantization, the accuracy of target range estimation improves when the bit-rate (i.e., the total number of measured bits) increases, whereas the accuracy of other undithered schemes saturate in this case; and (ii), for fixed, low bit-rate scenarios, severely quantized dithered schemes exhibit better performances than their full resolution counterparts. These observations are confirmed using real measurements obtained in a controlled environment, demonstrating the feasibility of the method in real ranging applications."

---
