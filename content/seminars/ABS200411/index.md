---
title: Recognition of sport players' numbers using fast color segmentation
slug: ABS200411-Recognition-of-sport-players'-numbers-using-fast-color-segmentation
draft: false

# name of the speaker
speaker: "Cédric Verleysen"

# date of the seminar
seminarDate: 2011-04-20T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_200411.pdf

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "For a few years, automatic human identification has become a hot scientific topic in the field of computer vision.
Recognition of players on a sport ground is a special case of human recognition that a computer needs to be able to understand how sport games proceed.
More precisely, distinguishing between players' numbers is relevant to interpret autonomously a sport game using various tasks such as tracking of players, control of active cameras, event recognition, etc.
The purpose of this presentation is to introduce some methods used to label automatically each player based on the recognition of the digit printed on the jersey.
The first part of the seminar will explain how color segmentation can be applied to isolate numbers from the jersey. Afterwards, digit recognition will be focused.
Our contribution to color segmentation uses a training phase during which colors on the jersey are learnt to improve the speed of the next segmentations. Knowing the principal colors of the jersey, color masks are successively applied which enables real-time isolation of the numbers, as opposed to most of the other current segmentation methods.
Once the number is extracted, the decision about its value is taken, thanks to a feature-based classification. More precisely, discriminant features such as the ratio between the width and the height of the number, number of holes, projective histograms and some central moments are used to separate numbers among them.
This combination between a short time online training that learns the colors of the jerseys and the use of an offline trained features-based classifier on the digits speeds up the recognition of sport players' numbers compared to existing other methods.
Finally, the real-time efficiency of the proposed method is validated on a basketball match in which good rejection ratio is more than 95% and number recognition performance exceeds 70%."

---
