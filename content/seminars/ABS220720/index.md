---
title: Re-Identification for Multi-Person Tracking
slug: ABS220720-Re-Identification-for-Multi-Person-Tracking
draft: false

# name of the speaker
speaker: "Vladimir Somers"

# date of the seminar
seminarDate: 2020-07-22T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: ABS220720_slides.pdf

# Location of the seminar
location: "(Online) Microsoft Teams" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Continuous identification of sport players during a game is essential to produce personalised content and collect individual statistics. With images provided by a single camera view setup, player identification relies on two components: (1) player recognition, which can be performed only sporadically, when discriminative appearance features are available and (2) individual player tracking during the game using detections generated at each frame. Individual tracking is easy when the player is isolated on the court, but becomes difficult or impossible when there are occlusions and complex interactions with other players. For that reason, long-term tracking is generally implemented based on the temporal association of shorter players tracks, also called tracklets, which are obtained by non ambiguous association of detections from the same player. Numerous reliable and efficient solutions exist to generate these short tracklets. We will therefore focus our work on the temporal association of tracklets based on their underlying detections appearance. This requires estimating the affinity between pairs of tracklets, to identify the pairs that correspond to the same identity or to distinct ones, but also to embed those cues in a graph-based formalism to jointly optimize the tracks of multiple players. In this presentation, we will explain how to use state of the art CNN based models for person re-identification in order to address the question of visual affinity estimation between pairs of tracklets in a team sport multi-person tracking context."

---
