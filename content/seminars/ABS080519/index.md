---
title: Low-complexity signal processing for direct imaging of stellar systems
slug: ABS080519-Low-complexity-signal-processing-for-direct-imaging-of-stellar-systems
draft: false

# name of the speaker
speaker: "Benoît Pairet"

# date of the seminar
seminarDate: 2019-05-08T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Nyquist Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Direct imaging of stellar systems is an emerging and multi-disciplinary field of research. Stellar systems consist of faint objects lying in close vicinity of a very bright object, namely the host star. Hence, obtaining a faithful image of such a system not only requires state-of-the-art imaging hardware and dedicated observation strategy but also heavily relies on tailored post-processing techniques."

---
