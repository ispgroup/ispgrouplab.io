---
title: On the recent progress of object detection using neural networks
slug: ABS180418-On-the-recent-progress-of-object-detection-using-neural-networks
draft: false

# name of the speaker
speaker: "Maxime Istasse"

# date of the seminar
seminarDate: 2018-04-18T15:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Since the success of convolutional neural networks for image classification on the ImageNet dataset in 2012, the computer vision community has made use of neural networks to raise the state-of-the-art performance for a wide range of problems, e.g. classification, segmentation, ROI detection, object detection, image captioning, ... In this seminar, after a brief introduction to the neural network paradigm, we will dicuss the recent progress in object detection through some particular models. We will observe that modern neural networks can learn to infer jointly the bounding box and the class of each object of a scene in an end-to-end manner."

---
