---
title: Cytomine for collaborative and semantic analysis of multi-gigapixel images
slug: ABS090616-Cytomine-for-collaborative-and-semantic-analysis-of-multi-gigapixel-images
draft: false

# name of the speaker
speaker: "Raphaël Marée, Montefiore Institute, University of Liège  (invited talk)"

# date of the seminar
seminarDate: 2016-06-09T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_090616.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Cytomine (http://www.cytomine.be/) is an open-source, rich internet
application, for remote visualization of high-resolution images (à la
Google Maps), collaborative and semantic annotation of regions of
interest using user-defined ontologies, and semi-automated image
analysis using machine learning."

---
