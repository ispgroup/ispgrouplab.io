---
title: Phase Transition Analysis of Recently Proposed Compressed Sensing Recovery Algorithms
slug: ABS160211-Phase-Transition-Analysis-of-Recently-Proposed-Compressed-Sensing-Recovery-Algorithms
draft: false

# name of the speaker
speaker: "Adriana Gonzalez Gonzalez"

# date of the seminar
seminarDate: 2011-02-16T10:30:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_160211.pdf

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Compressed Sensing (CS) is a very important technique in signal processing that represents a change in the philosophy behind the acquisition process. By incorporating compression into the measurements, CS allows taking samples at a much lower rate than the Nyquist rate, which diminishes the amount of samples, the acquisition time and data dimension. This gives CS its own place in several applications of signal processing, such as imaging techniques. The main issue in CS is the recovery of the original signal, which has become a matter of solving underdetermined linear systems by having a sparse prior knowledge on the original signal. In this seminar two recently proposed CS recovery algorithms are presented and compared with IHT, one of the most used algorithms in this field due to its simplicity. The analysis is presented in the way of Phase Transition Diagrams, which allow observing the behavior of the algorithm regarding CS: the amount of measurements to take in order to recover a signal with a certain amount of information."

---
