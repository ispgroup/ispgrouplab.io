---
title: Joint use of semantic and geometric information for object localization using time-of-flight cameras
slug: ABS180520-Joint-use-of-semantic-and-geometric-information-for-object-localization-using-time-of-flight-cameras
draft: false

# name of the speaker
speaker: "Victor Joos de ter Beerst"

# date of the seminar
seminarDate: 2020-05-18T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: ABS180520_slides_corrected.pdf

# Location of the seminar
location: "(Online) Microsoft Teams" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Time-of-Flight (ToF) cameras are great for the mapping of close-up objects, but when applied to the analysis of large scenes, the task becomes more challenging due to low resolution, distance errors, and occlusion among others."

---
