---
title: Blind Deconvolution of PET Images using Anatomical Priors
slug: ABS280416-Blind-Deconvolution-of-PET-Images-using-Anatomical-Priors
draft: false

# name of the speaker
speaker: "Adriana González and Stéphanie Guérit"

# date of the seminar
seminarDate: 2016-04-28T09:30:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_280416.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Positron emission tomography (PET) imaging provides clinicians with relevant information about the metabolic activity of a patient. This imaging modality suffers, however, from a low spatial resolution due to both physical and instrumental factors. These factors can be modeled by a blurring function, often estimated experimentally by imaging a radioactive linear source. This estimate is useful to restore the PET image, either during the reconstruction process or in a post-processing phase. Nevertheless, in some situations (e.g., in the context of multicentric studies where images come from different clinical centers) we do not have access to raw data neither to the scanner and, hence, the blurring function cannot be directly estimated. Is it still possible to enhance the PET image when the blurring function is unknown? Blind deconvolution methods allow to simultaneously estimate the blurring function and restore the image. By using the available prior information we are able to regularize the considered inverse problem and, hence, reduce the number of potential solutions to those that are meaningful. In this talk, we will explain the blind deconvolution method that we developed, which uses prior information on the image based on the usual distribution of the radiotracer in the body and on the high resolution anatomical images from CT imaging."

---
