---
title: Dictionary learning methods for single-channel source separation
slug: ABS291112-Dictionary-learning-methods-for-single-channel-source-separation
draft: false

# name of the speaker
speaker: "Augustin Lefèvre (INMA/ICTEAM, UCL)"

# date of the seminar
seminarDate: 2012-11-29T10:45:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_291112.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "We present three main contributions to single-channel source separation. Our ﬁrst contribution is a group-sparsity inducing penalty speciﬁcally tailored for nonnegative matrix factorization (NMF) with the Itakura-Saito divergence: in many music tracks, there are whole intervals where at least one source is inactive. The group-sparsity penalty we propose allows identifying these intervals blindly and learn source speciﬁc dictionaries. As a consequence, those learned dictionaries can be used to do source separation in other parts of the track were several sources are active. These two tasks of identiﬁcation and separation are performed simultaneously in one run of group-sparsity Itakura-Saito NMF."

---
