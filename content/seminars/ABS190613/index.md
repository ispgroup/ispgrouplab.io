---
title: Detection of facial expressions and computer graphics animation
slug: ABS190613-Detection-of-facial-expressions-and-computer-graphics-animation
draft: false

# name of the speaker
speaker: "Kaori Hagihara (ICTEAM/ELEN, UCL)"

# date of the seminar
seminarDate: 2013-06-19T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_190613.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "I will introduce my current project collaborating with a 3D Computer Animator.
In 3D Computer Animation, the production stage involves building, rigging and texturing models, animating characters, and setting up and lighting scenes.
One of the task, animating characters, requires huge amount of manual manipulations.
In order to facilitate this task, limited to facial animations, we are developing a semi-automate system by capturing an actor with a kinect camera.
The facial action of the actor will drive facial animation of the character.
Technically, the system consists of two parts: detection of facial actions and animation/morphing of a character.
The talk will be mainly about the detection of facial expressions using sequences of RGB and depth images from kinect camera."

---
