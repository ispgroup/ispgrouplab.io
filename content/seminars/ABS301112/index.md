---
title: Compressed Sensing in RF Communication and Analog-to-Digital Conversion
slug: ABS301112-Compressed-Sensing-in-RF-Communication-and-Analog-to-Digital-Conversion
draft: false

# name of the speaker
speaker: "Thomas Arildsen (TPS/DES, Aalborg U., Denmark)"

# date of the seminar
seminarDate: 2012-11-30T11:15:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: ISPS301112.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Compressed sensing has experienced explosive growth in interest from
the research community in recent years. It is currently being
investigated in numerous, typically signal processing-related areas."

---
