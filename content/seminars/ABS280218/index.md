---
title: Tone mapping methods for X-ray images
slug: ABS280218-Tone-mapping-methods-for-X-ray-images
draft: false

# name of the speaker
speaker: "Tahani Madmad"

# date of the seminar
seminarDate: 2018-02-28T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Images are captured on a high dynamic range (HDR), divided into thousands of levels (typically 14 or 16 bits), when the sensor’s sensitivity allows it and when the application requires it. This is typically the case for X-ray radiography (medical or industrial), and this is a source of concern in terms of image rendering, since the human visual system perceives little more than 256 shades of gray."

---
