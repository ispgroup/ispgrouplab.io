---
title: Fitting on manifolds with Bézier functions
slug: ABS160317-Fitting-on-manifolds-with-Bézier-functions
draft: false

# name of the speaker
speaker: "Pierre-Yves Gousenbourger"

# date of the seminar
seminarDate: 2017-03-16T16:15:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Fitting and interpolation are well known topics on the Euclidean space. However, when it turns out that the data points are manifold valued (understand: when the data point belongs to a certain manifold), most of the algorithms are no more applicable because even the notion of distance is not as simple as on the Euclidean space."

---
