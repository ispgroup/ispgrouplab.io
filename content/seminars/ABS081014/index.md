---
title: Interpolation on Riemannian manifolds with a {$C^1$} piecewize Bézier path
slug: ABS081014-Interpolation-on-Riemannian-manifolds-with-a-{$C^1$}-piecewize-Bézier-path
draft: false

# name of the speaker
speaker: "Pierre-Yves Gousenbourger"

# date of the seminar
seminarDate: 2014-10-08T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_081014.pdf

# Location of the seminar
location: "Nyquist Seminar Room (a164, close to Shannon seminar room) Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Nowadays, more and more problems are solved through specific manifold formulation. This often allows important reduction of computation time and/or memory management compared to classical formulations on the classical Euclidean space (because of non-linear constraints like restricting the solutions to a certain subdomain of a larger ambiant space). Interpolation and optimization tools can be useful for solving some of these problems (like defining the optimal trajectory of a humanitory plane dropping supplies, or fitting two objects orientations). However, current procedures are only defined on the Euclidean space.
In this presentation, I focus on interpolation methods and, more precisely, I propose a new general framework to fit a path through a finite set of data points lying on a Riemannian manifold. The path takes the form of a continuously-differentiable concatenation of Riemannian Bézier segments. This framework will be illustrated by results on the Euclidean space, the sphere, the orthogonal group and the shape manifold.​ The content of this presentation meets also very recent research carried out in this institute for providing novel efficient manifold-based optimization methods."

---
