---
title: Compressed sensing methods for cardiac C-arm computed tomography
slug: ABS050214-Compressed-sensing-methods-for-cardiac-C-arm-computed-tomography
draft: false

# name of the speaker
speaker: "Cyril Mory (Creatis, Lyon & ImagX)"

# date of the seminar
seminarDate: 2014-02-05T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_050214.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 40

# Abstract of the seminar
abstract: "In cardiac C-arm CT, electrocardiogram gating leads to a limited view reconstruction problem. Few projections are available to reconstruct each phase, and their angular distribution is not optimal. In such conditions, traditional reconstruction algorithms like SART or FDK prove insufficient. Compressed sensing allows using a priori information in the reconstruction, potentially compensating for the loss of information caused by ECG-gating. This talk will be a quick tour of the 3D compressed sensing methods available for cardiac C-arm CT and of the 3D+time methods developed during my PhD thesis."

---
