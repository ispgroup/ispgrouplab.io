---
title: Bit-Based Learning Machines
slug: ABS040919-Bit-Based-Learning-Machines
draft: false

# name of the speaker
speaker: "Sercan Aygün"

# date of the seminar
seminarDate: 2019-09-04T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Nyquist Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Stochastic computing (SC) is a re-emerging computation methodology uses the hardware-based approaches in the presence of bit-streams. In recent years, the stochastic computing is being used from communication systems to the neural networks including hardware design issues, device testing & modelling, image processing and finally the learning machines. SC is basically advantageous for the area and power consumption criterion on the hardware, however, especially for the applied areas such as computer vision and neural systems, they require investigation and deep analysis. In this talk, stochastic computing basics on the neural network systems will be given to extend it into multilayer neural networks including the binary network case. Preserving streams through all of the network layers preserves robustness to the noise. Basic operations such as multiply-and-accumulate are handled with simple logic elements. Hardware structure both in well-known conventional binary neural networks and in the stream-based neural networks is very similar. Thus, bit stream injected XNOR gates and counters are used to obtain noise-resistant architecture in a same way."

---
