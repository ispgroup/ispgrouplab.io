---
title: Compressive learning (e.g., clustering) from a (quantized) sketch of the dataset
slug: ABS151117-Compressive-learning-(e.g.,-clustering)-from-a-(quantized)-sketch-of-the-dataset
draft: false

# name of the speaker
speaker: "Vincent Schellekens"

# date of the seminar
seminarDate: 2017-11-15T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_151117.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Machine learning algorithms, such as the k-means clustering, typically require several passes on a dataset of learning examples (e.g., signals, images, data volumes). These must thus be all acquired, stored in memory, and read multiple times, which becomes prohibitive when the number of examples becomes very large. On the other hand, the machine learning model learned from this data (e.g. the centroids in k-means clustering) is usually simple and contains relatively few information compared to the size of the dataset."

---
