---
title: Integrated H.264 Region-of-Interest Detection, Tracking and Compression for Surveillance Scenes".
slug: ABS011210-Integrated-H.264-Region-of-Interest-Detection,-Tracking-and-Compression-for-Surveillance-Scenes".
draft: false

# name of the speaker
speaker: "Ivan Alen Fernandez"

# date of the seminar
seminarDate: 2010-12-01T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "''Motion detection and tracking is an important vision topic for many applications such as video surveillance. When this process takes place during video encoding and transmission, Regions-of-Interest (RoIs) turn to be a very useful tool in order to favor the encoding of such regions compared to the fixed background. In this presentation, we show that, in conjunction
with effective spatio-temporal filters, H264 Motion Estimation can be efficiently used for a robust and coarse-grain detection and tracking of moving objects. The integration of the compression and detection modules enables the prediction of ROIs positions from previous frames, offering therefore tracking at a very low computational cost. In the case of high resolution sequences affected by severe quality degradation (such as improper interlacing, light reflections and camera shaking), the global video compression ratio can be dramatically improved without damaging the ROI. This is especially the case when appropriate encoding options, i.e. appropriate Flexible Macroblock Ordering (FMO) types, are exploited. Different proposals are offered to maximize the quality of the RoI facing a dynamic constraint of the network bandwidth.''"

---
