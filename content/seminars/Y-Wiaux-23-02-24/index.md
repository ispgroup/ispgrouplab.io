---
title: "AI in computational imaging: from algorithms to radio astronomy"
slug: "Y-Wiaux-23-02-24"
draft: false

# name of the speaker
speaker : "Yves Wiaux (Heriot-Watt University Edinburgh, UK)"

# date of the seminar
seminarDate: 2024-02-23T11:00:00     

# external link (eg web page of the speark)
externalLink: https://researchportal.hw.ac.uk/en/persons/yves-wiaux

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
# files : []

# Location of the seminar
location: "Euler Seminar Room (A002)"

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: Endowing advanced imaging instruments such as telescopes and medical scanners with an acute vision that enables them to probe the Universe or human body with precision is a complex mathematical endeavour. It requires solving challenging inverse problems for image formation from observed data. In this talk, we will dive into this field of computational imaging, and its specific application in radio astronomy, where algorithms are currently facing a multi-faceted challenge for the robust reconstruction of images at extreme resolution and dynamic range, and from extreme data volumes. We will discuss advanced algorithms at the interface of optimisation and deep learning theories, from SARA, an optimisation algorithm propelled by handcrafted regularisation priors, to AIRI, plug-and-play algorithm relying on learned regularisation denoisers, and the newborn deep neural network series R2D2, which can be interpreted as a learned version of the Matching Pursuit algorithm. If time allows, we will also briefly illustrate the transfer of such algorithms to medical imaging. Last but not least, we will take a few seconds to unveil Star Wars hidden facts and misconceptions. 
---
**Biography**: Yves Wiaux received the MSc degree in Physics and the PhD degree in Theoretical Physics from the Université catholique de Louvain (UCL, Louvain-la-Neuve) in Belgium, in 1999 and 2002 respectively. He was a Senior Researcher at the Signal Processing Laboratories of the Ecole Polytechnique Fédérale de Lausanne (EPFL) in Switzerland from 2003 to 2013, where he created the Biomedical and Astronomical Signal Processing (BASP) group. In 2013, he moved as an Associate Professor at the School of Engineering and Physical Sciences of Heriot-Watt University where he currently runs the BASP group. He was promoted to Professor at Heriot-Watt in 2016. He is also an Academic Guest at EPFL and a Honorary Fellow at the University of Edinburgh (UoE). Among other responsibilities, Prof. Wiaux chairs the "BASP Frontiers" Conference series, and is an Associate Editor of the IEEE "Transactions in Computational Imaging" (TCI) journal and of the "Royal Astronomy Society Techniques and Instruments" (RASTI) journal. Since 2010, he has led numerous research projects funded by both the Swiss National Science Foundation (SNSF) and the UK Research and Innovation Councils (UKRI). The ethos of Prof. Wiaux’s BASP group is to develop cutting-edge research in computational imaging, from theory and algorithms to applications in astronomy and medicine.

