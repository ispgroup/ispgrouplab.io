---
title: Proximal methods for Poisson Intensity CBCT and PET 
slug: ABS021012-Proximal-methods-for-Poisson-Intensity-CBCT-and-PET-
draft: false

# name of the speaker
speaker: "Prof. Yannick Boursier (Université Aix-Marseille 2 / ESIL) (invited talk)"

# date of the seminar
seminarDate: 2012-10-02T10:45:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 55

# Abstract of the seminar
abstract: "Cone-Beam Computerized Tomography (CBCT) and Positron Emission Tomography (PET) are two complementary medical imaging modalities providing respectively anatomic and metabolic information of a patient. In the context of public health, one must address the problem of dose reduction of the potentially harmful quantities related to each exam protocol : X-rays for CBCT and radiotracer for PET.

Two demonstrators based on a technological breakthrough (acquisition devices work in photon-counting mode) have been developed and we investigate in this paper the two related tomographic reconstruction problems. We formulate separately the CBCT and the PET problems in two general frameworks that encompass the physics of the acquisition devices and the specific discretization of the object to reconstruct.

These objects may be observed from a limited number of angles of views and we take into account the specificity of the Poisson noise. We propose various fast numerical schemes based on proximal methods to compute the solution of each problem.

In particular, we show that primal-dual approaches are well suited in the PET case when considering non differentiable regularizations such as Total Variation. Experiments on numerical simulations and real data are in favor of the proposed algorithms when compared with the well-established methods and has contributed to establish the first results for spectral CT."

---
