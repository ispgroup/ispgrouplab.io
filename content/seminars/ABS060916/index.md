---
title: Are classifiers really robust to deformations in the data?
slug: ABS060916-Are-classifiers-really-robust-to-deformations-in-the-data?
draft: false

# name of the speaker
speaker: "Alhussein Fawzi, LTS4, EPFL, Switzerland  (invited talk)"

# date of the seminar
seminarDate: 2016-09-06T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "TVNUM Seminar Room (not Shannon!!), Place du Levant 2, Stévin Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "The robustness of classifiers to small perturbations of the datapoints is a highly desirable property when the classifier is deployed in real and possibly hostile environments. Despite achieving excellent performance on recent visual benchmarks, state-of-the-art classifiers are surprisingly unstable to small perturbations of the data."

---
