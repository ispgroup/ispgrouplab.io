---
title: High resolution microCT, and its past and future challenges in the biomedical field
slug: ABS120618-High-resolution-microCT,-and-its-past-and-future-challenges-in-the-biomedical-field
draft: false

# name of the speaker
speaker: "Prof. Greet Kerckhofs (UCLouvain/iMMC, KULeuven)"

# date of the seminar
seminarDate: 2018-06-12T15:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Biological tissues are 3D structures with complex spatial heterogeneity, for which traditional 2D imaging techniques such as standard histomorphometry are insufficient to comprehensively characterize them or assess their quality. Advanced 3D imaging is one of the enabling technologies that is of increasing importance in the biomedical field to assess tissue quality, and to provide better knowledge on the mechanisms behind tissue formation and regeneration. In this seminar, I will provide an overview of the process of implementing high resolution microCT as advanced 3D imaging tool for different biomedical applications, as well as of its optimization, both in terms of hardware (side equipment and contrast agents) and software (for image acquisition, reconstruction and analysis) specifically for different biomedical applications. I will highlight what we have achieved over the past 15 years within the KU Leuven microCT labs, but I will also discuss which the currently remaining challenges are and how we will aim to tackle (some of) them in the future."

---
