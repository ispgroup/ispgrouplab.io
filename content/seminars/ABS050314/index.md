---
title: Planar Tracking for 3-D Reconstruction
slug: ABS050314-Planar-Tracking-for-3-D-Reconstruction
draft: false

# name of the speaker
speaker: "Arnaud Delmotte"

# date of the seminar
seminarDate: 2014-03-05T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_050314.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105) Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "This talk presents a tracking method for 3-D reconstruction of planar surfaces in the context of video editing. Interaction with end-user is allowed in order to avoid occlusion of the tracked objects and to correct possible errors. The method takes into account all perspective transformations by a template matching method. This one proceeds first by estimating translation and rotation of the object from large templates before to estimate the perspective transformation with more localized template matching."

---
