---
title: Blind Interference Alignment for Cellular Networks
slug: ABS151015-Blind-Interference-Alignment-for-Cellular-Networks
draft: false

# name of the speaker
speaker: "Máximo Morales (DIGICOM, ICTEAM, UCL)"

# date of the seminar
seminarDate: 2015-10-15T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "As more dense and heterogeneous cellular networks are required to satisfy the user demands on mobile communications interference becomes to be the principal limiting factor. Several schemes such as Linear Zero Forcing Beamforming (LZFB) or Interference Alignment (IA) have been proposed as means of achieving enormous data rates. These transmission schemes are based on exploiting the Channel State Information at the Transmitter (CSIT) to achieve the optimal Degrees of Freedom (DoF), also known as multiplexing gain. It is interesting to remark that satisfying this requirement in cellular environments involves to waste resources for channel feedback and backhaul links among the BSs. In consequence, the increase on the rates achieved by these schemes results futile because of the costs of providing CSIT."

---
