---
title: Light Field Methods for the Visual Inspection of Transparent Objects
slug: ABS241017-Light-Field-Methods-for-the-Visual-Inspection-of-Transparent-Objects
draft: false

# name of the speaker
speaker: "(invited talk) Johannes Meyer (KIT, Karlsruhe, Germany)"

# date of the seminar
seminarDate: 2017-10-24T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_241017.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Components made of transparent materials play an important role in human’s every-day life. For example, they are employed to build windshields for automobiles and aircrafts or to guide light beams in high-precision optical systems and therefore have to meet high quality requirements. However, powerful automated visual inspection systems for transparent materials are still an open research question."

---
