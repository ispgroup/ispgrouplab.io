---
title: "A resource allocation framework for adaptive selection of point matching strategies for visual tracking"
slug: "ABS080513-A-resource-allocation-framework-for-adaptive-selection-of-point-matching-strategies-for-visual-tracking"
draft: false

# name of the speaker
speaker: "Quentin De Neyer (ICTEAM/ELEN, UCL)"

# date of the seminar
seminarDate: 2013-05-08T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_080513.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "The presentation introduces an object tracking framework based
on the matching of points between pairs of consecutive video frames. The
approach is especially relevant to support object tracking in close-view
video shots, as for example encountered in the context of the PTZ camera
autotracking problem. In contrast to many earlier related works, we
consider that the matching metric of a point should be adapted to the
signal observed in its spatial neighborhood, and introduce a cost-benefit
framework to control this adaptation with respect to the global target
displacement estimation objective. Hence, the proposed framework
explicitly handles the trade-off between the point-level matching metric
complexity, and the contribution brought by this metric to solve the
target tracking problem. As a consequence, and in contrast with the common
assumption that only specific points of interest should be investigated,
our framework does not make any a priori assumption about the points that
should be considered or ignored by the tracking process. Instead, it
states that any point might help in the target displacement estimation,
provided that the matching metric is well adapted. Measuring the
contribution reliability of a point as the probability that it leads to a
crorrect matching decision, we are able to define a global successful
target matching criterion. It is then possible to minimize the probability
of incorrect matching over the set of possible (point,metric) combinations
and to find the optimal aggregation strategy."

---
