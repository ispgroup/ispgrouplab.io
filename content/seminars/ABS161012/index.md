---
title: A 3D MRI segmentation method and a 2D/3D X-ray image registration method for orthopaedic applications
slug: ABS161012-A-3D-MRI-segmentation-method-and-a-2D/3D-X-ray-image-registration-method-for-orthopaedic-applications
draft: false

# name of the speaker
speaker: "Taha Jerbi (ELI, UCL)"

# date of the seminar
seminarDate: 2012-10-16T10:45:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_161012.pdf

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "First, I present a new segmentation method of 3D MRI images that evaluates the necrotic area in the femoral head. This method is based on PDE equations and a dynamic robust estimator of the noise and the irrelevant contours. I show the advantages of our results by using entropy based evaluation technique. Second, I present a registration method in frequency domain between 2D and 3D data. This method is used to determine the positions of the bones during flexion (pseudo static). It is well suited to the acquisition system which produces 3D reconstructions of the bones in the initial standing position and only 2D frontal and sagittal radiographic acquisitions for other positions."

---
