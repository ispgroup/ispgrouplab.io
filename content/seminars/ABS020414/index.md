---
title: Which features to discriminate nucleolus phenotypes?
slug: ABS020414-Which-features-to-discriminate-nucleolus-phenotypes?
draft: false

# name of the speaker
speaker: "Pascaline Parisot"

# date of the seminar
seminarDate: 2014-04-02T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room (a105) Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Earlier biological studies have shown a strong correlation between the structure of the nucleolus of a cell and the potential diseases affecting this cell. During this presentation, I will present a set of 'few' features, based on the shape and the texture of the nucleolus, that might help to discriminate nucleolus phenotypes."

---
