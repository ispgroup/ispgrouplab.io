---
title: Pilots allocation for sparse channel estimation in multicarrier systems 
slug: ABS051115-Pilots-allocation-for-sparse-channel-estimation-in-multicarrier-systems-
draft: false

# name of the speaker
speaker: "François Rottenberg (DIGICOM, ICTEAM, UCL)"

# date of the seminar
seminarDate: 2015-11-05T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Wireless channels experience multipath fading which can be modeled by a sparse
discrete multi-tap impulse response. Estimating this channel is of crucial importance
to allow the receiver to properly recover the transmitted signal. This presentation
investigates the issue of allocating the pilots for sparse channel estimation applied
to multicarrier systems. When the number of pilots is larger than or equal to
the channel maximal length, this issue is well-known and the optimal allocation
is equispaced. However for long channels, this would require a very large number
of pilots decreasing the throughput of the system. Therefore, compressed sensing
(CS) techniques are considered to estimate the sparse channel from a limited
number of pilots. In that case, the problem of placing the pilots remains an
open issue. This paper proposes a two-step hybrid allocation of the pilots that
takes the maximal channel length into account to restrict the frequency candidates.
The performance of this allocation is demonstrated through simulations
and comparisons with other classical allocations."

---
