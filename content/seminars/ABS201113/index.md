---
title: Collaborative algorithms, bees & NodeJS
slug: ABS201113-Collaborative-algorithms,-bees-&-NodeJS
draft: false

# name of the speaker
speaker: "Jérôme Plumat (ICTEAM/ELEN, UCL)"

# date of the seminar
seminarDate: 2013-11-20T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_201113.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 30

# Abstract of the seminar
abstract: "Collaborative algorithms are very powerful tools to rapidly investigate a huge unknown space of solutions. The presentation introduces a very simple collaborative problem: path finding in graph. The proposed solution is based on bees and analogy to the pollen gathering. An algorithm is presented in order to take into consideration information collected by others and incorporates it in a prior model. A very brief introduction to efficient programming tools aims you to realize how such tools may be easily implemented."

---
