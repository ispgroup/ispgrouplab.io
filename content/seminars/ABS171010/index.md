---
title: Gene network reconstruction from expression microarray data
slug: ABS171010-Gene-network-reconstruction-from-expression-microarray-data
draft: false

# name of the speaker
speaker: "Jérôme Ambroise"

# date of the seminar
seminarDate: 2010-11-17T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: SPS-171110-LJ.pdf.zip

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "In this seminar, I will present several algorithms enabling to reconstruct gene regulatory network from gene expression data. A gene regulatory network focuses on interactions between transcription factors and their target genes. Unsupervised methods will be first presented including relevance networks and Gaussian Graphical Models. If the number p of variables (genes) is much larger than the number n of microarrays experiments, standard approaches to compute Gaussian Graphical Models are inappropriate. Suitable alternative based either on regularized estimation of the inverse covariance matrix, or on regularized high dimensional regression will be briefly introduced. Then, I will present the supervised algorithm recently proposed by F. Mordelet and J.-P. Vert. Using this supervised approach conducts to an improvement of predictive performances but this method cannot be used to predict interactions involving an 'orphan' transcription factor. Finally I will present the 'TNIFSED' method that infers gene regulatory network from the integration of correlation and partial correlation coefficients with the gene functional similarity through a supervised classifier. Compared to the supervised SIRENE algorithm, TNIFSED performed slightly worse when transcription factors are associated with a wide range of yet identified target genes. However, unlike SIRENE, predictive performance of TNIFSED does not decrease with the number of target genes, a feature which makes TNIFSED suitable to discover target genes associated with ’orphan’ transcription factors."

---
