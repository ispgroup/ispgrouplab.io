---
title: Greedy algorithms for multi-channel sparse recovery
slug: ABS100118-Greedy-algorithms-for-multi-channel-sparse-recovery
draft: false

# name of the speaker
speaker: "Jean-François Determe (ULB & UCL, Belgium)"

# date of the seminar
seminarDate: 2018-01-10T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_100118.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Research on “compressive sensing” (CS) has shown that it is possible to retrieve high-dimensional signals using a limited set of (often random) linear measurements. For CS to work properly, the signal to be retrieved must be sparse, which means that most of its components (e.g., 90 % of them) are zero. Many algorithms relying on CS can reliably recover sparse signals on the basis of a number of measurements that essentially scales with sparsity level (instead of scaling with the number of dimensions)."

---
