---
title: On the decomposition of blurring operators in wavelet bases
slug: ABS110316-On-the-decomposition-of-blurring-operators-in-wavelet-bases
draft: false

# name of the speaker
speaker: "Prof. Pierre Weiss, ITAV, U. Toulouse, France. (invited talk)"

# date of the seminar
seminarDate: 2016-03-11T10:45:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Image deblurring is a fundamental image processing problem usually solved with computationally intensive procedures. After significant progresses due to advances in convex optimization (e.g. accelerated gradient schemes à la Nesterov), this problem however seemingly stalled for the last 5 years.  In this talk, I will first provide a few properties of blurring operators (convolutions or more generally regularizing linear integral operators) when expressed in wavelet bases. These properties were one of the initial motivations of Y. Meyer to develop the wavelet theory. Surprisingly, they have been used extremely scarcely in image processing, despite the success of wavelets in that domain. We will then show that those theoretical results may have an important impact on the practical resolution of inverse problems and in particular deblurring. As an example, we will show that they allow gaining from one to two orders of magnitude in the speed of resolution of convex l1-l2 deblurring problems."

---
