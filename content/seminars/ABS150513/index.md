---
title: Discriminative Label Propagation for Multi-Object Tracking with Sporadic Appearance Features
slug: ABS150513-Discriminative-Label-Propagation-for-Multi-Object-Tracking-with-Sporadic-Appearance-Features
draft: false

# name of the speaker
speaker: "Amit Kumar K.C. (ICTEAM/ELEN, UCL)"

# date of the seminar
seminarDate: 2013-05-15T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_150513.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Given a set of plausible detections, detected at each time instant independently, we investigate how to associate them across time. This is done by propagating labels on a set of graphs that capture how the spatio-temporal and the appearance cues promote the assignment of identical or distinct labels to a pair of nodes. The graph construction is driven by the locally linear embedding (LLE) of either the spatio-temporal or the appearance features associated to the detections. Interestingly, the neighbourhood of a node in each appearance graph is defined to include all nodes for which the appearance feature is available (except the ones that coexist at the same time). This allows to connect the nodes that share the same appearance even if they are temporally distant, which gives our framework the uncommon ability to exploit the appearance features that are available only sporadically along the sequence of detections."

---
