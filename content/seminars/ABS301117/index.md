---
title: Computational Imaging in Atomic Force Microscopy
slug: ABS301117-Computational-Imaging-in-Atomic-Force-Microscopy
draft: false

# name of the speaker
speaker: "Thomas Arildsen (TPS/DES, Aalborg U., Denmark)"

# date of the seminar
seminarDate: 2017-11-30T11:15:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Atomic force microscopy (AFM) is an imaging technique which can measure the surface structure of a specimen of interest down to nano-scale. It does this by scanning a tiny probe across the surface and thereby measuring a “height map” or other properties of the surface."

---
