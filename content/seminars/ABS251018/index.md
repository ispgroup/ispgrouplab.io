---
title: Post-processing techniques for exoplanet imaging.
slug: ABS251018-Post-processing-techniques-for-exoplanet-imaging.
draft: false

# name of the speaker
speaker: "Faustine Cantalloube (MPIA, Heidelberg, Germany)"

# date of the seminar
seminarDate: 2018-10-25T14:15:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Nyquist Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Imaging exoplanets provides unique insights to our knowledge about planet formation and evolution. However this is an extremely challenging task: the angular separation between the star and the planet is of a few hundred milli-arcseconds and the brightness ratio is of a few billion. To tackle this, a very specific instrument is needed but still the images we obtain are corrupted by the so-called “speckle noise” within which planets are hidden. Adapted post-processing is therefore necessary to unveil the planets in the images. The current post-processing techniques used have limitations that we need to address to image more exoplanets but also the environment in which they evolve to, in the end, understand better our universe."

---
