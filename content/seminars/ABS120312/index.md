---
title: Depth stitching for extended field of view for highly curved IOL charaterization
slug: ABS120312-Depth-stitching-for-extended-field-of-view-for-highly-curved-IOL-charaterization
draft: false

# name of the speaker
speaker: "Alexandre Saint"

# date of the seminar
seminarDate: 2012-03-12T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Multifocal intraocular lenses are implanted in the eye to correct
vision deficiencies. They are a viable alternative to spectacles and
contact lenses. Some advanced IOLs are made of diffractive steps with
different resolving power to allow a vision from near to far
distances. But these IOLs also come with highly curved and
discontinuous surfaces. It is an industrial challenge to be able to
charaterize such IOLs precisely and in a short time."

---
