---
title: (invited talk) "Applications of PCA and low-rank plus sparse decompositions in high-contrast Exoplanet imaging
slug: ABS040216-(invited-talk)-"Applications-of-PCA-and-low-rank-plus-sparse-decompositions-in-high-contrast-Exoplanet-imaging
draft: false

# name of the speaker
speaker: "Carlos Gomez, VORTEX Project, Department of Astrophysics, Geophysics & Oceanography, ULg"

# date of the seminar
seminarDate: 2016-02-04T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_040216.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Only a small fraction of the confirmed exoplanet candidates known to date have been discovered through direct imaging. Indeed the task of observing planets is very challenging due to the huge difference in contrast between the host star and its potential companions, the small angular separation and image degradation caused by Earth’s turbulent atmosphere.  
Post-processing algorithms play a critical role in direct imaging of exoplanets by boosting the detectability of real companions in a noisy background. Among these data processing techniques, the most recently proposed is the Principal Component Analysis (PCA), a ubiquitous statistical technique already used in background subtraction problems. Inspired by recent advances in machine learning algorithms such as robust PCA, we propose a local three-term decomposition (LLSG) that surpasses current PCA-based post-processing algorithms in terms of detectability of companions at near real-time speed. We test the performance of our new algorithm on a training dataset and show how LLSG decomposition reaches higher signal-to-noise ratio and has an overall better performance in the Receiver Operating Characteristic (ROC) space."

---
