---
title: 4D signal processing for lung cancer treatment by radiotherapy
slug: ABS100413-4D-signal-processing-for-lung-cancer-treatment-by-radiotherapy
draft: false

# name of the speaker
speaker: "Nicolas Gallego-Ortiz (ICTEAM/ELEN, UCL)"

# date of the seminar
seminarDate: 2013-04-10T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_100413.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Cancer treatment by radiotherapy relies on accurate patient positioning. The aim of radiotherapy is to target, with a high energy X-ray beam, cancerous cells while sparing surrounding healthy tissue. This requirement is especially hard to accomplish for lung tumors. The thorax is subject to respiratory motion, heart beating and deformations due to organ fillings. Tumor position uncertainty implies the addition of margins to the target volume. The largest the margins the more of healthy tissue is irradiated reaching even organs risk.

Research to assure treatment quality and safety to moving tumors is currently very active. Efforts range from treatment planning and delivery protocols, to technological developments of dynamic imaging modalities. The development of 4D Computed Tomography during the last decade opened the door to image-guided therapy for lung tumors. At the same time, tumor tracking in real-time is still not feasible in most of the cases, thus there are still many open issues.

In this talk 4D signal processing is going to be discussed in the context of radiotherapy. 4D signals (3D + time) come from a variety of imaging modalities implemented in the treatment room. The state of the art of imaging and tumor motion models is presented and a discussion on research questions in the field is proposed."

---
