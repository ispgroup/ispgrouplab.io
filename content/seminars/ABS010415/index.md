---
title: Hyperspectral Unmixing
slug: ABS010415-Hyperspectral-Unmixing
draft: false

# name of the speaker
speaker: "Muhammad Arsalan"

# date of the seminar
seminarDate: 2015-04-01T10:45:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "The sensors of spectral imaging devices have often large sizes of pixel in which numerous materials contribute to the spectrum obtained at a single pixel. In most of applications we need to identify the constituent materials present along with their proportions in such mixed pixels. The process by which the mixed pixel is decomposed into a collection of constituent spectra (endmembers) and a set of corresponding fractions (abundance, indicate the proportion of each endmember present in the pixel). It is not an easy task because of its ill-posed inverse problem nature due to model inaccuracies, noise in the observation, environmental conditions, data size and endmember variability."

---
