---
title: An outlook on deep reinforcement learning
slug: ABS060919-An-outlook-on-deep-reinforcement-learning
draft: false

# name of the speaker
speaker: "(invited talk) Dr Vincent François-Lavet (McGill University, Canada & Mila)"

# date of the seminar
seminarDate: 2019-09-06T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_060919.pdf

# Location of the seminar
location: "Euler building (room A.002)" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Deep reinforcement learning is the combination of reinforcement learning (RL) and deep learning. This field of research has been able to solve a wide range of complex decision-making tasks that were previously out of reach for a machine. In this talk, I will provide an introduction to deep reinforcement learning models, algorithms and techniques. I will particularly focus on the aspects related to generalization of the learned policy to slightly different situations than the ones encountered during training."

---
