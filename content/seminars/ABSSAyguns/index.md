---
title: Approximate digital system design with hardware-based stochastic computing for computer vision
slug: ABSSAyguns-Approximate-digital-system-design-with-hardware-based-stochastic-computing-for-computer-vision
draft: false

# name of the speaker
speaker: "Sercan Ayguns, PhD student (Visitor from İstanbul Technical University, Maslak, İstanbul, Turkey)"

# date of the seminar
seminarDate: 2018-09-13T15:30:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Stochastic Computing (SC) is an emerging research topic as a new calculation paradigm for numerical systems and for application-specific structures such as hardware depended implementation of image processing and learning science. The stochastic calculation, although based on the 1960s, has increased considerably in years for its importance and applicability. The approach on this computing paradigm is to approximate results using fewer circuitry treating either transfer function or the algorithm itself. The computing paradigm is treated in three stages from bit streams to a logic system itself and from the system to the output bit streams. The design methodology is also in the scope of nanoscale devices to measure the probabilistic behavior of the atomic structures. Moreover, test and verification of conventional logic systems are controlled for the fault and error analysis using the SC approach. SC is recently is applied to image processing algorithms to be realized in hardware with less power consumption and area. Image pre-processing such as noise reduction, or edge detection are some examples that are handled as application of SC like image compression, too. As a Ph.D. research, it is being researched how to apply this paradigm into some of several machine learning structures like deep learning for the treatment of images."

---
