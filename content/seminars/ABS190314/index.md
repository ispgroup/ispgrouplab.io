---
title: Using Shape Priors to Regularize Intermediate Views in Wide-Baseline Image-Based Rendering
slug: ABS190314-Using-Shape-Priors-to-Regularize-Intermediate-Views-in-Wide-Baseline-Image-Based-Rendering
draft: false

# name of the speaker
speaker: "Cédric Verleysen"

# date of the seminar
seminarDate: 2014-03-19T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_190314.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105) Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Nowadays, when a viewer watches a video content, his/her viewpoint is fixed to one of the cameras that have recorded the scene.
In order to increase the viewer's immersivity, the next generation of video content will allow him/her to interactively define his/her viewpoint.
This domain is known as free-viewpoint rendering, and consists of the interpolation of views from images captured by some real cameras.
However, the state-of-the-art solutions require that the real cameras share very similar viewpoints, meaning that a close scene can be rendered only with a dense camera network, and that far scenes can be rendered only with very high-resolution cameras. These requirements make free-viewpoint rendering an expensive technology, slowing down its entry on the market."

---
