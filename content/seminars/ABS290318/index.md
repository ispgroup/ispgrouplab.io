---
title: Hardware architecture for machine learning and image processing
slug: ABS290318-Hardware-architecture-for-machine-learning-and-image-processing
draft: false

# name of the speaker
speaker: "Martin Lefebvre (ECS Group, ICTEAM, UCL)"

# date of the seminar
seminarDate: 2018-03-29T16:15:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "For some decades, efficient machine learning and image processing algorithms have been successfully developed to achieve a broad range of tasks, e.g. prediction, classification, compression, ... However, the hardware used for running those algorithms used to be inefficient, both in terms of computational performance and power consumption.
In this seminar, we focus on the state-of-the-art implementation of electronic circuits making use of such algorithms. In particular, computational CMOS image sensors and deep- and machine learning processors are discussed. On the one hand, imagers focus on reducing the amount of data produced by the sensor either by performing in-pixel or embedded processing of the image, based on techniques such as edge detection, feature extraction, or compressive sensing. On the other hand, some cutting-edge deep- and machine learning processors rely on analog computations performed inside SRAM memories, to reduce the power consumption related to memory accesses and to increase the performance through parallelism, thus improving the energy efficiency of those processors."

---
