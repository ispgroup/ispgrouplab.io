---
title: Image-based PUFs for Anti-Counterfeiting
slug: ABS071111-Image-based-PUFs-for-Anti-Counterfeiting
draft: false

# name of the speaker
speaker: "Saloomeh Shariati"

# date of the seminar
seminarDate: 2011-11-07T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Random variations that appear on the profile of the physical objects can be exploited to prevent their cloning. We present Physical Unclonable Function (PUF) designs that exploit random variations which are characterized by images. This distinct category of PUFs, called Image-based PUFs are functional to protect many products such as luxury products, pharmaceuticals, clothing, watches, automotive spare parts etc. We present a general model of Image-based Physical Function System. We highlight Image-hashing as the particular component of the Image-based Extraction and present two practical non-adaptive and adaptive image-hashing methods namely Random Binary Hashing and Gabor Binary Hashing.  We explore experimentally the security properties, i.e., robustness and physical unclonability of an example of image-based physical function namely Laser-Written Physical Function. The results demonstrate significant effect of the image hashing method and parameters on the ultimate trade-off between robustness and physical unclonability."

---
