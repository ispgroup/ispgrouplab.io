---
title: Restricted Range Space Property Based Theory for 1-Bit Compressive Sensing
slug: ABS280916-Restricted-Range-Space-Property-Based-Theory-for-1-Bit-Compressive-Sensing
draft: false

# name of the speaker
speaker: "Chunlei Xu"

# date of the seminar
seminarDate: 2016-09-28T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Plenty works have been devoted to the study of compressive sensing over the past decades. Such a promising development of compressive sensing has a great impact on many aspects of signal and image processing. One of the key mathematical problems addressed in compressive sensing is how to reconstruct a sparse signal from linear/nonlinear measurements via a decoding algorithm. In the classic compressive sensing, it is well known that to exactly reconstruct the sparsest signal from a limited number of linear measurements is possible when the sensing matrix admits certain properties."

---
