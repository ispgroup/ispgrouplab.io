---
title: Implementation Aspects of the Random Demodulator for Compressive Sensing
slug: ABS260413-Implementation-Aspects-of-the-Random-Demodulator-for-Compressive-Sensing
draft: false

# name of the speaker
speaker: "Pawel Jerzy Pankiewicz (AAU, Aalborg, Denmark)"

# date of the seminar
seminarDate: 2013-04-26T11:15:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_260413.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "The compressive sensing paradigm has gained significant attention from the applied sciences and engineering in recent years. The random demodulator is one of the compressive sensing architectures providing efficient sub-Nyquist sampling of sparse band-limited signals using simple off-the-shelf components."

---
