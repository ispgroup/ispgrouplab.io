---
title: A very short introduction to digital topology
slug: ABS010611-A-very-short-introduction-to-digital-topology
draft: false

# name of the speaker
speaker: "Sébastien Lugan"

# date of the seminar
seminarDate: 2011-06-01T10:30:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "After a quick definition of basic key concepts (adjacency, connexity,
Jordan's curve theorem, etc.) of digital topology, we will quickly
explore some main features and applications of digital topology for
2D/3D image processing and some of its possible extensions."

---
