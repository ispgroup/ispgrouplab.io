---
title: How using DCE-MRI and registration to measure the concentration of the contrast agent ([CA]) inside the human and guinea-pigs (GP) cochlea?
slug: ABS010914-How-using-DCE-MRI-and-registration-to-measure-the-concentration-of-the-contrast-agent-([CA])-inside-the-human-and-guinea-pigs-(GP)-cochlea?
draft: false

# name of the speaker
speaker: "Jérôme Plumat"

# date of the seminar
seminarDate: 2014-09-01T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_010914.pdf

# Location of the seminar
location: "TVNUM Seminar Room (a124) Place du Levant 2, Stévin Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "How using DCE-MRI and registration to measure the concentration of the contrast agent (CA) inside the human and guinea-pigs (GP) cochlea? Due to the tightness of the blood-labyrinth barrier (BLB) few medium contrast reaches the inner ear resulting to a low received signal. Furthermore, the sizes of the different compartments inside the ear require a good spatial resolution. We are currently trying to design some T1-weigthed MRI sequences and a protocol to quantify the amount of CA along the time. We have performed two classes of experiments. Firstly by using a 4.7T scanner and GP and secondly using a Siemens 3T scanner and human controls. Primarily results highlight a BLB's porosity change in GP with ear infections. Also, we are currently using registration to precisely and automatically quantify the concentration of CA in the different compartments."

---
