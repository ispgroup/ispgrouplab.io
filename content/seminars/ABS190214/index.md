---
title: Adaptability to Improve Convergence
slug: ABS190214-Adaptability-to-Improve-Convergence
draft: false

# name of the speaker
speaker: "Adriana Gonzalez"

# date of the seminar
seminarDate: 2014-02-19T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_190214.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105) Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Optimization techniques have been used extensively throughout signal processing in many applications. One of the key challenges when implementing iterative optimization algorithms is to appropriately choose the step size(s) to improve the algorithm convergence. This pre- sentation is dedicated to the general problem of adaptive selecting the step size based on the works of Aghazadeh et al. [1] and Goldstein et al. [2]. In a first level, the presentation is focused on general iterative algorithms with only one step size, which can be adaptively selected via the ski rental problem [1], a popular class of problems from the computer science literature. In a second level, we discuss about the adaptivity of primal-dual algorithms, where the convergence is sensitive to two step sizes. The two parameters are automatically selected based on the optimality conditions of the problem [2, 3]."

---
