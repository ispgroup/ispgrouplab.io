---
title: "V-DMC : the emerging MPEG standard for dynamic mesh compression"
slug: "P-Rondao-07-12-23"
draft: false

# name of the speaker
speaker : Patrice Rondao, Nokia Bell Labs 

# date of the seminar
seminarDate: 2023-12-07T14:30:00          

# external link (eg web page of the speark)
externalLink: 

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files : []

# Location of the seminar
location: Euler seminar room, a.002 

# duration of the seminar in minutes
duration: 45 

# Abstract of the seminar
abstract: MPEG is well-known for its success story in delivering video and audio compression standards that we all use every day, perhaps without noticing it, in our mobile phones, TV, cameras, computers… However, did you know MPEG also produced 3D and immersive representations compression standards? In this talk, I will first shortly explain why these 3D representations are important for a growing number of applications and devices and how MPEG has addressed their compression in a family of new standards called V3C. I will then focus on the ongoing standardization efforts related to the newest member of this family; the emerging V-DMC standard that is designed to efficiently compress 3D dynamic mesh sequences. We will also see how it compares with the relevant state of the art.  
---

