---
title: Fisheye stereovision to model 3D urban environments in the context of a GNSS positioning application
slug: ABS280518-Fisheye-stereovision-to-model-3D-urban-environments-in-the-context-of-a-GNSS-positioning-application
draft: false

# name of the speaker
speaker: "Dr Julien Moreau (IFSTTAR, France)"

# date of the seminar
seminarDate: 2018-05-28T13:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "This research deals with 3D modelling from an embedded fisheye vision system, used for a GNSS (Global Navigation Satellite Systems) application as part of CAPLOC project. Satellite signal propagation in urban area implies reflections on structures, impairing localisation’s accuracy and availability. The project purpose is to define an omnidirectional vision system able to provide information on urban 3D structure and to demonstrate that it allows to improve localisation.
This presentation addresses problems of (1) self-calibration, (2) matching between images, (3) 3D reconstruction; each algorithm is assessed on computer-generated and real fisheye images. Moreover, it describes a way to correct GNSS signals reflections from a 3D point cloud to improve positioning.
Calibration is handled by a two-steps process: the 9-point algorithm fitted to “equisolid” model coupled with a RANSAC, followed by a Levenberg-Marquardt optimisation refinement.
Dense matching is done by dynamic programming along conjugated epipolar curves. Distortions are not rectified in order to neither degrade visual content nor to decrease accuracy. In the binocular case it is possible to estimate full-scale coordinates. In the monocular case, we do it by adding odometer information."

---
