---
title: "Data fitting on manifolds: applications, challenges and solutions"
slug: PierreYvesGousenbourger-Data-fitting-on-manifolds:-applications,-challenges-and-solutions
draft: false

# name of the speaker
speaker: "Pierre-Yves Gousenbourger"

# date of the seminar
seminarDate: 2019-12-11T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: ABS111219_slides

# Location of the seminar
location: "Euler seminar room (Room A.002, Euler Building, Avenue Georges Lemaître 4-6) " 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Storm trajectories prediction, birds migrations follow-up, rigid rotations of 3D objects, wind field estimation, model order reduction of superlarge parameter-dependent dynamical systems, MRI 3D body volumes reconstruction... All these applications have two things in common: first, they have a geometrical data-structure, i.e., the data lives on a (generally) Riemannian manifold; second, they can benefit of parameter(s)-dependent fitting methods somewhere in the process. If data fitting is a basic problem in the Euclidean space (where natural cubic splines and thin plates splines are the superstars in the domain), it become more intricate when data structure constrains the problem.

This talk is an opportunity to present you an efficient, ready-to-use algorithm for data fitting on manifolds based on Bezier curves, applied to some of the aforementioned applications."

---
