---
title: "Diffusion weighted imaging challenges the neuro histology: dream or reality?"
slug: ABS060215-Diffusion-weighted-imaging-challenges-the-neuro-histology:-dream-or-reality?
draft: false

# name of the speaker
speaker: "Damien Jacobs"

# date of the seminar
seminarDate: 2015-02-06T10:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_060215.pdf

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Diffusion weighted imaging is largely used in medical imaging to diagnosis neuropathologies on patient. The signal of water diffusion along neurons is anisotropic. In the past 20 years, this signal is commonly estimated by a tensor and defined as Diffusion Tensor Imaging (DTI). The tensor groups the different cellular microstructures (neurons, myelin, astrocyte, microglia,...) in one compartiment. Recently, multi compartiments models have been proposed to take into account the different cellular microstructure and the heterogeneity. These models have been validated to the healthy subject. In this research, these multi compartiments models are assessed to characterize the Wallerian degeneration process in the spinal cord after the unilateral dorsal roots transection."

---
