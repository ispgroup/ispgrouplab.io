---
title: Computations using UCL's grid engine
slug: ABS290512-Computations-using-UCL's-grid-engine
draft: false

# name of the speaker
speaker: "Prasad Sudhakar"

# date of the seminar
seminarDate: 2012-05-29T15:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "As a part of our research work, most of us do intensive numerical simulations which require a lot of computation time and memory. Performing these simulations on local standalone computers have several drawbacks: 1) the computers may not be resourceful enough (computing power + memory) to handle the load, 2) several instances of a single piece of code have to be executed serially, one after the other because essentially there is just one computing resource, and hence simulations may last longer hours, 3) license issues for expensive tools such as Matlab, 4) frequent manual intervention, etc. In this talk we will see how to overcome these obstacles by using the UCL/CISM's computational grid, Lemaitre."

---
