---
title: Robust point correspondence based on template matching for image registration
slug: ABS040511-Robust-point-correspondence-based-on-template-matching-for-image-registration
draft: false

# name of the speaker
speaker: "Kaori Hagihara"

# date of the seminar
seminarDate: 2011-05-04T10:30:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "I introduce two algorithms presenting robust methods for automatically matching points over two images for image registration.
The subject scene is considered to be more or less planar or in distance so that the image transformation can be roughly approximated by a homography.
The basic principle is local correlation measurement by template matching, however many incorrect matches or outliers remain with real images.
In order to resolve this problem, hierarchical schemes are proposed.
One progressively evaluates all potential matches starting with non-local constraints, such as spatial consistency, global smoothness, and epipolar constraint, that should be approximately satisfied across the image.
The other progressively estimates image distortions starting with translation, rotation, affine, and homography by random voting followed by variable template matching compatible with the estimated distortions."

---
