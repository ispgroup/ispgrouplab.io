---
title: Tone-mapping for X-ray images
slug: ABS061119-Tone-mapping-for-X-ray-images
draft: false

# name of the speaker
speaker: "Tahani Madmad"

# date of the seminar
seminarDate: 2019-11-06T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room (Place du Levant 3, Maxwell Building, 1st floor)" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Inspecting X-ray images is an essential aspect for medical diagnosis and for nondestructive control of manufactured objects in the industrial field. However, X-ray images are characterized by a high dynamic range and a low contrast. Due to those characteristics, and the limitation of the human visual system, important aspects such as nodules, bones fractures, gas inclusions or other kind of defects and anomalies are difficult to identify for the human eyes.
Through my presentation, I will first introduce the challenges of X-ray images visualization and give an insight into the state of the art. At a later stage, I will present the fusion framework I adopted to address this issue and then focus on an instance of this vision that led to a tone-mapping operator based on a bilateral histogram equalization.  Finally, I will discuss the options I am considering to overcome the limitations of this tone-mapping algorithm and achieve the genericity needed to visualize highly contrasted X-ray images regardless of their content."

---
