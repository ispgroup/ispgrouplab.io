---
title: Statistical models of the spine with applications in medical image processing
slug: ABS170413-Statistical-models-of-the-spine-with-applications-in-medical-image-processing
draft: false

# name of the speaker
speaker: "Dr Fabian Lecron (UMons, Belgium)"

# date of the seminar
seminarDate: 2013-04-17T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:
    slides: slides_170413.pdf

# Location of the seminar
location: "Shannon Seminar Room (a105)
Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Statistical shape models are useful tools for the extraction of an object in a given image. In this respect, this presentation introduces two novel ways of modeling shapes with a particular interest for the representation of the spine in biomedical applications. The characteristic of the first proposed model is to represent the link existing between several items composing a global shape. It is particularly the case for the spine where the vertebrae are linked together. The advantage of the second model is that no assumption on the statistical distribution of the sample is required. It is based on One-Class Support Vector Machine and on the definition of an hyperplane that holds the data of a sample in its positive side. Part of the presentation is dedicated to the use of these models to extract the shape of the spine in 2D or in 3D. The aim of the first application is to automatically segment cervical vertebrae on radiographs in order to compute orientation angles and to assess the mobility of the cervical spine. The second application concerns the 3D reconstruction of the spine from bi-planar radiographs to assess the scoliosis of a patient with 3D clinical indices."

---
