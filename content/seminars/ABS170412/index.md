---
title: How to cheat with statistics? A practical guide to the skeptical reviewers.
slug: ABS170412-How-to-cheat-with-statistics?-A-practical-guide-to-the-skeptical-reviewers.
draft: false

# name of the speaker
speaker: "Maxime Taquet"

# date of the seminar
seminarDate: 2012-04-17T14:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "Null hypothesis statistical testing and p-values are used pervasively in research. In some fields of research, such as medicine and neuroscience, a p-value lower than 0.05 may simply be a good enough result to publish. But what does this p-value really mean? What does it rely on? Could we reach such a result by chance only? In this talk, I will present answers to these questions as well as their consequences in terms of the confidence we have in some published results. Finally, I will introduce some alternatives to null hypothesis statistical testing inspired from the Bayesian data analysis literature."

---
