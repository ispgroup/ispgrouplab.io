---
title: Advanced signal processing and VLSI implementation for wireless communication and biomedical applications
slug: ABS270715-Advanced-signal-processing-and-VLSI-implementation-for-wireless-communication-and-biomedical-applications
draft: false

# name of the speaker
speaker: "Prof. Daniel Massicotte, U. Québec, Canada"

# date of the seminar
seminarDate: 2015-07-27T11:00:00

# external link (eg web page of the speark)
externalLink:

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files:

# Location of the seminar
location: "Shannon Seminar Room, Place du Levant 3, Maxwell Building, 1st floor" 

# duration of the seminar in minutes
duration: 45

# Abstract of the seminar
abstract: "We present the research projects realized at the \"Laboratoire des signaux et systems intégrés\" du Groupe de recherche en électronique industrielle de l'Université du Québec à Trois-Rivières. This research laboratory develops advanced methods in signal processing and microsystems to target growing needs strategic areas such as telecommunications systems, biomedical, measurement and control."

---
