---
title: "Deep Learning for Anomaly Detection in Industrial Vision"
slug: "AnomalyDetection"
date: 2020-03-02
draft: false
type: "research"

# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of researches)
thumbnail: anomaly.png

# Add a cover picture [optional]
cover: 
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: "In order to detect anomaly in an unsupervised scheme, an autoencoder is trained to reconstruct clean images out of defect-free images corrupted with synthetic noise. During inference an arbitrary (with or without anomaly) image is projected onto the normal space of images. The intensity of the residual map between the original image and its reconstruction estimates the likelihood of a region to be defective."

# Add categories of the research post (e.g. image processing)
categories: ["Image Processing", "Deep Learning", "CNN", "Anomaly Detection"] 

# Add researchers involved
researchers: [anne-sophie-collin, christophe-de-vleeschouwer]

---

# Deep Learning for Anomaly Detection in Industrial Vision

Anomaly detection can be defined as the process of identifying rare items, events or observations that differ significantly from the majority of the data {{<cite type="P" id="chandola2009anomaly">}}. Such methods can be applied to a wide range of applications belonging to several areas: detection of bank frauds (financial field), pathological tissues {{<cite type="P" id="schlegl2017unsupervised">}} (biomedical field), manufacturing defects {{<cite type="P" id="staar2019anomaly">}} (industrial field), etc. 

## Why Opt for an Unsupervised Approach ? 

At first sight, anomaly detection problem is shaped to be addressed with a classification based approach. Unfortunately, the training of such supervised models require collecting significant amount of labeled data, which is a massive undertaking. Moreover, the intrinsic specification of the anomaly detection problem makes the supervised approach inappropriate for two reasons:
* The scarcity of abnormal events makes normal vs abnormal classes highly unbalanced
* The appearance of abnormal samples can be highly variable

Therefore, unsupervised approaches, based on the sole observations of data statistics, are most appropriated schemes to address the anomaly detection problem in real world applications. 

## Reconstruction-based Approach

Anomaly detection in images can be tackled by a reconstruction-based approach. In this strategy an CNN, usually an autoencoder, is used to reconstruct abnormal regions of an image with normal structures seen during training. Then, a residual map obtained by measuring the difference between the input image and its reconstruction can be interpreted as the likelihood that a region does not belong to the normal class. In order to achieve this, all normal regions should remain unaltered, while the abnormal ones should be replaced by clean content. 

## Our Method

The approach involves the training of an autoencoder with only defect-free images. Traditionally, it is expected that the dimensionality reduction regularizes by itself the reconstruction onto the space of clean images. 

However, in order to enhance the reconstruction of (part of) the clean images, we add skip-connections to the autoencoder. At the same time, we apply corruption to the training images with the aim of preventing the convergence toward an identity mapping. Several models of synthetic noise are compared as shown in Figure&nbsp;1. 

{{<image src="ad_workflow.png" width="50%" position="center" caption="Figure 1: The corruption of training images with the Stain, Stratch, Drops or Gaussian noise models combined with the addition of skip-connections allow to decrease reconstruction error of clean structures while modifying abnormal ones. ">}}

As seen in Figure 2 (example coming from MVTec AD dataset {{<cite type="P" id="bergmann2019mvtec">}}), we show that a structured stain-shaped noise, independent of the image content, combined with the addition of skip-connections to the autoencoder leads to better reconstruction and hence better anomaly detection. 

{{<image src="ad_MVTec.png" width="75%" position="center" caption="Figure 2: Impact of the corruption model on the reconstruction of a defective object (up) and a defective texture (down). Identity column is given as comparison and stands for an autoencoder trained with uncorrupted images.">}}

On the object (up row), we observe that a stain shaped corruption leads to the best reconstruction of the defective region, even if the defect looks like a scratch. It indicates that the synthetic noise generalizes well to detect real anomalies.\
The reconstruction of finely textured surface is also enhanced thanks to the skip-connections. However, a non structural noise such as the Gaussian noise model performs poorly on textures. 

Overall, the combination of skip-connections and a stain-shaped synthetic noise leads to the best reconstruction. 
