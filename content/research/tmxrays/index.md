---
title: "Tone Mapping for X-Ray images"
slug: "tmxrays"
date: 2020-03-04
draft: false
type: "research"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

thumbnail: TmrxLogo.png

# Add a cover picture [optional]
cover: 
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: "Inspecting X-ray images is an essential aspect for medical diagnosis and for nondestructive control of manufactured objects in the industrial field. However, X-ray images are characterized by a high dynamic range and a low contrast. Due to those characteristics, and the limitation of the human visual system, important aspects such as nodules, bones fractures, gas inclusions or other kind of defects and anomalies are difficult to identify for the human eyes.The challenge is therefore to reduce the image bit depth, while preserving subtle and fine-grained local variation in the image."

# Add categories of the research post (e.g. image processing)
categories: ["Medical Imaging", "Image Processing", "High Dynamic Range", "Image Fusion"]


# Researchers
researchers: ["Tahani Madmad", "christophe-de-vleeschouwer"]

---
#  Tone Mapping for X-Ray images

High Dynamic Range imaging offers the ability to capture weakly contrasted details, due to a relatively fine quantization of the luminosity range. The visualization of those details however remains challenging given the perceptual capabilities of the human visual system. Therefore, tone mapping constitutes a major element of any high dynamic range-imaging pipeline and has been the subject of numerous studies in the last two decades. The traditional tone mapping operators (TMOs) have been widely studied and evaluated from a qualitative and quantitative point of view, in most cases for natural image processing[]. The main objective of this research is to design a TMO dedicated to X-ray images inspection including the different applications of this technique (Medical diagnosis, non destructive testing...).

{{<image src="TmrxFig1.png" width="75%" position="center" caption="Figure 1. X-Ray images acquisition and visualization">}}


In contrast to conventional photography, which measures the light reflected by an object, X-ray imaging measures the amount of photons passing through the objects of interest. As a consequence, X-ray image signals generally mix-up a piecewise smooth component, reflecting the absorptive power of the objects in the scene, with fine-grained local variations, reflecting the texture/shape of the observed objects. The piecewise smooth component is easy to visualize; simply by using a linear mapping between the acquisition and rendering dynamic (see Fig. 1). Our work primarily aims at visualizing the second component, which is often the most informative regarding the characterization/ recognition of the observed objects.

##  Fusion-Based Framework

We have proposed a fusion-based strategy to approximate histogram equalization on each smooth segment of the first component. This is done in a seamless manner, without requiring explicit segmentation of the piecewise smooth signal. In short, multiple images, named slices in the following, are derived from the initial HDR image, by clipping and stretching distinct portions/segments of the HDR image histogram.


{{<image src="TMXRAYS2.png" width="75%" position="center" caption="Figure 2. Bilateral Histogram Equalization pipeline">}}

Unfortunately defining the weight maps to be equal to the bin indicator function induces dramatic artifacts, because distinct weight vectors and thus distinct mapping functions are assigned to neighboring pixels when those pixels belong to distinct bins. In that case, there is no guarantee regarding the preservation, after the mapping, of the relative ordering of those pixels that are mapped with distinct functions.

In order to avoid visual artifacts, typically by preserving the relative ordering of neighboring pixels, it is necessary to maintain similar weight vectors for neighboring pixels. A natural approach to prevent variation of weight vectors across neighboring pixels consists in filtering the indicator functions with a low-pass Gaussian kernel. Gaussian filtering indeed improves the output image quality. However, some ringing artifacts remain along the sharp edges that delimit uniform regions, due to the different mappings associated to the close-to-identical pixels of those regions. As a central contribution, we propose to control the diffusion of the indicator functions, so as to approximate histogram equalization on groups of pixels that are spatially connected and correspond to similar intensity ranges. Therefore, our work proposes to spread/diffuse the HDR segment indicator functions based on the cross-bilateral filter, also named joint-bilateral filter in the literature[]. Formally, for a pixel value $v=x(m,n)$ in the $k$-th bin, the mapping $T(v)=o(m,n)$ is written as follows :

$$T(v) = \sum\_{j=0}^{k-1} \frac{\beta\_j}{b\_j - b\_{j-1}} + \frac{v-b\_{k-1}}{b\_k-b\_{k-1}}.\beta\_k.$$

Where $\beta\_j$ corresponds to the percentage of image pixels that belong to the $k$-th bin.

This mapping function computes the cumulative distribution of image pixel intensities, and therefore corresponds to a global (quantized) histogram equalization. Figure 4 compares BHE to a set of well-known tone mapping approaches on a representative set of images, corresponding to a variety of application fields (medical, industrial, security, and archaeology). This qualitative assessment is completed by a qualitative assessment using the TMQI [5] and image entropy metrics (See our paper [1] for more details)


{{<image src="TMXRAYS3.png" width="75%" position="center" caption="Figure 3. Bilateral Histogram Equalization function">}}


{{<image src="TMXRAYS4.png" width="75%" position="center" caption="Figure 4. BHE results and comparison for a set of 6 X-ray HDR images from medical, industrial and archaeological applications. (a) Original HDR image, (b) Ashikhmin [3], (c) Reinhard [4] , (d) Reinhard [5], (e) Durand [6], (f) Fattal [7] , (g) BHE [2].">}}

### Related works


* [1] Tahani Madmad Nicolas Delinte Christophe De Vleeschouwer (2021).  [CNN-based morphological decomposition of X-ray images for details and defects contrast enhancement](http://hdl.handle.net/2078.1/245259). Proceedings of the IEEE/CVF workshops associated to the conference on Computer Vision and Pattern Recognition

* [2] Tahani Madmad Christophe De Vleeschouwer (2019).  [Bilateral Histogram Equalization for X-Ray Image Tone Mapping](http://hdl.handle.net/2078.1/219127). 2019 IEEE International Conference on Image Processing (ICIP)

* [3] Ashikhmin, M., & Goyal, J. (2006). A reality check for tone-mapping operators. ACM Transactions on Applied Perception (TAP), 3(4), 399-411.

* [4] Reinhard, E., & Devlin, K. (2005). Dynamic range reduction inspired by photoreceptor physiology. IEEE transactions on visualization and computer graphics, 11(1), 13-24.

* [5] Reinhard, E., Stark, M., Shirley, P., & Ferwerda, J. (2002, July). Photographic tone reproduction for digital images. In Proceedings of the 29th annual conference on Computer graphics and interactive techniques (pp. 267-276).

* [6] Durand, F., & Dorsey, J. (2002, July). Fast bilateral filtering for the display of high-dynamic-range images. In Proceedings of the 29th annual conference on Computer graphics and interactive techniques (pp. 257-266).

* [7] Farbman, Z., Fattal, R., Lischinski, D., & Szeliski, R. (2008). Edge-preserving decompositions for multi-scale tone and detail manipulation. ACM transactions on graphics (TOG), 27(3), 1-10.



