---
title: "Compressive Schlieren Deflectometry" 
slug: "csschlieren"
date: 2014-09-17
draft: false
type: "research"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of researches)
thumbnail: deflectionSpectrum.png

# Add a cover picture [optional]
cover: 
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: "This project concerns with the application of compressive sensing principles for characterizing transparent objects using schlieren deflectometry. This is an instance of real world applications of compressed sensing."

# Add categories of the research post (e.g. image processing)
categories: ["Compressive sensing", "Schlieren Deflectometry"]


# Add researchers involved
researchers: ["Prasad Sudhakar", laurent-jacques, "Adriana Gonzalez"]

---
# Compressive Schlieren Deflectometry

Transparent objects of uniform refractive indices can be characterized by specifying the light deflection patterns on their surfaces. For example, consider a plano-convex lens shown in fig. 1. For each location on the object, we can associate a deflection map or a deflection spectrum, which essentially tells us how much of the incoming light (parallel to the optical axis) is deflected in each direction. The objective of this project is to come up with a method to efficiently measure and reconstruct such maps for a class of objects.  

{{<image src="deflectionSpectrum.png" width="50%" position="center" caption="Figure 1">}}

To this end, we use schlieren deflectometry, an optical imaging modality to measure deflection patterns {{<cite type="P" id="joannes2003phase">}}. The commercially available NIMO system manufactured by Lambda-X, one of the partners of the project, actually implements a schlieren system. However, the deflection patterns are not available for direct observation and instead they can only be probed indirectly by measuring their correlations with programmable optical modulation patterns. In fact, the system is such that, once a specific modulation pattern is loaded, its correlations with all the deflection spectra (one for each object location) are simultaneously available in the output CCD array. The pixels in CCD array are in fact in one-to-one correspondence with the locations on the object. 

{{<image src="innerProd.png" width="50%" position="center" caption="Figure 2">}}

Mathematically, each fixed CCD pixel simply provides the inner products between the corresponding underlying deflection spectrum and the various modulation patterns that are used for probing, and it can be modelled as a system of linear equations (see fig. 2). This system of linear equations contain the used modulation patterns as the coefficient matrix. Remember, there are as many systems of linear equations as the number of CCD pixels, but all the systems have the same coefficient matrix because the modulation patterns are the same. To obtain an underlying spectrum, we need to solve the corresponding system of linear equations.  

In a naïve manner, one could think of using simple modulation patterns, such as shifted delta functions, so that the coefficient matrix is an identity matrix. However, this is very very inefficient in terms of the number of modulation patterns to be used and also the effective SNR of the measurement. To cut down the number of measurements, we rely on the sparsity property of the deflection spectrum. Note that for sufficiently smooth objects the deflection spectrum is sparse in nature and hence the technique of compressive sensing is applicable {{<cite type="P" id="baraniuk2007compressive">}}.  

For reasons supported by the compressive sensing literature and the limitations of the system under consideration, we use a small subset of the columns of a Hadamard matrix (of appropriate size), coupled with a spread spectrum matrix, as modulation patterns {{<cite type="P" id="puy2012universal">}}. These modulation patterns are binary in nature. Although the resulting linear system is highly under-determined, the deflection spectrum can still be recovered by solving an optimization problem, thanks to the sparse nature of the spectrum. This optimization problem encapsulates the sparsity prior of the spectrum and the system of linear equations.  

To solve the optimization problem, we make use of the Chambolle-Pock algorithm, which is a standard algorithm based on primal-dual formulation to solve saddle-point optimization problems {{<cite type="P" id="chambolle2011first">}}.

Fig. 3(a) is an example of a deflection spectrum, at some arbitrary location, of a plano-convex lens of optical power 60D, reconstructed with a full set (100%) of possible modulation patterns. Fig. 3(b) and 3(c) show the same deflection spectrum reconstructed with only 2% and 10% of the measurements. The reconstructions, even though not as perfect as the first one, are sufficient to make inferences about the object under consideration. 


{{<image src="reconResult.png" width="50%" position="center" caption="Figure 3">}}
	
  
For more details on the setup, compressed sensing formulations and results, please see the relevant publications {{<citedial type="P" id="boreal:145738">}} and {{<citedial type="P" id="boreal:134467">}}

**Collaborators:** 

* Philippe Antoine ([Lambda-X](https://lambda-x.com/), Belgium) 
* Xavier Dubois ([Lambda-X](https://lambda-x.com/), Belgium) 
* Luc Joannes ([Lambda-X](https://lambda-x.com/), Belgium) 

