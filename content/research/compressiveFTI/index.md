---
title: "Compressive Fourier Transform Interferometry for Hyperspectral Imaging"
slug: "compressiveFTI"
date: 2020-03-03
draft: false
type: "research"

toc: true
tocOpen: true
tocSide: left

thumbnail: cifti_sifti.png

# Add a cover picture [optional]
cover: cifti_sifti.png
# coverCaption: Accumulation of observations of the Venus transit captured by SDO/AIA on June 5th - 6th 2012

# Add a short description [optional]
description: "In this project we propose two variants of the Fourier Transform Interferometry (FTI) imager, i.e., coded illumination-FTI (CI-FTI) and structured illumination FTI (SI-FTI), based on the theory of compressive sensing (CS). These schemes efficiently modulate light exposure temporally (in CI-FTI) or spatiotemporally (in SI-FTI). Leveraging a variable density sampling strategy recently introduced in CS, we provide near-optimal illumination strategies, so that the light exposure imposed on a biological specimen is minimized while the spectral resolution is preserved."

# Add categories (e.g. image processing)
categories: [ "Inverse Problem", "Compressive Sensing", "Sensing", "Imaging", "Hyperspectral", "Interferometer"]

# Researchers
researchers: ["Amirafshar Moshtaghpour", "Valerio Cambareri", "laurent-jacques"]


---
# A Variable Density Sampling Scheme for Compressive Fourier Transform Interferometry

## Definition of the problem

A ''hyperspectral'' (HS) volume refers to a three-dimensional (3D) data cube associated with stacks of two-dimensional (2D) spatial images along the spectral axis, i.e., one monochromatic image for each wavenumber. Each spatial location records the variation of transmitted or reflected light intensity along a large number of spectral bands from that location in a scene.

Since chemical elements have unique spectral signatures, referred to as their fingerprints, observing the dense spectral content of an HS volume, instead of a red, green, blue (RGB) image, provides accurate information about the constituents of a scene. This has made HS imaging appealing in a wide range of applications, e.g., agriculture, food processing, chemical imaging, and biology.

Due to the extremely large amount of data stored in an HS volume, also referred to as HS cube, the acquisition and processing of these volumes encounter significant challenges. For example, in satellite or airborne applications it is common to transmit volumes with size of several gigabytes to a ground station. Additionally, in biological applications, designers of HS imagers face a myriad of trade-offs related to photon efficiency of the sensors, acquisition time, achievable spatiospectral resolution, and cost.

Compared to other techniques that often reach lower spectral resolutions, ''Fourier transform interferometry'' (FTI) is an HS imaging modality that has shown promising results in the acquisition of high spectral resolution biological HS volumes, specially in fluorescence spectroscopy. In this application a biological specimen is stained with dyes that fluoresce with a unique spectrum when they are exposed to light. Since each fluorescent dye binds to a
certain protein, this technique is highly used for determining the localization, concentration, and growth of certain target cells (e.g., malignant tumors), whose automatic characterizations are improved at higher spectral resolution.

{{<image src="fti_scheme.png" width="600px" position="center" caption="'''Figure 1:''' Operating principle of FTI.">}}

## The need for compressive FTI
FTI consists in a Michelson Interferometer (MI) [1] with one moving mirror controlling the optical path difference (OPD) of the light. Besides, the raw FTI
measurements are intermediate data, i.e., the Fourier coefficients of the actual HS volume with respect to the spectral domain. Shannon-Nyquist sampling theory states that for a fixed wavenumber range, the spectral resolution of an HS volume is proportional to the number of FTI measurements. This fact is simultaneously a blessing and a curse in fluorescence spectroscopy, where high spectral resolution is highly desirable for biologists. One may record more FTI measurements, thereby overexposing fluorescent dyes. As a consequence, the ability of the dyes to fluoresce fades during the experiments. This phenomenon, which is due to the photochemical alteration of the dyes, is called ''photo-bleaching''. Therefore, the resolution of an HS volume is limited by the durability of the fluorescent dyes when exposed to the illumination. An alternative is to reduce light intensity in a fashion that is inversely
proportional to the increase in the number of FTI measurements, resulting in a low input signal-to-noise ratio (SNR). Considering all these facts, our main motivation in this project is to reduce photo-bleaching in fluorescence spectroscopy using FTI. However, this requires delicate compromises between the goals of achieving high spectral resolution and high SNR while minimizing the light exposure imposed on the specimen.

{{<image src="photobleaching.png" width="700px" position="center" caption="**Figure 2:** Illustration of photobleaching, how it limits the resolution of FTI, and our idea to mitigate it.">}}

<!-- %lfloat width=700px %Attach:photobleaching.png | '''Figure 2:''' Illustration of photobleaching, how it limits the resolution of FTI, and our idea to mitigate it.
[[<<]] -->

## Our contributions
The main objective of this project is to establish a CS scheme for FTI, i.e., reducing the total number of measurement in the OPD domain compared to common
(Nyquist rate) FTI and still allowing reliable and robust estimation of HS data volumes. We show that this compressive FTI enables HS imaging when total light exposure is a critical parameter, such as in biological confocal microscopy where special fluorescent dyes inserted in a biological specimen suffer from photo-bleaching. In fact, our work also studies this context when light exposure is considered as a fixed resource to be distributed equally over
all measurements.

More precisely, our work is driven by the following questions: (i) For a fixed spectral resolution, to which extent can we reduce the amount of light exposure on the observed specimen and still allow robust reconstruction of HS data? (ii) For a fixed exposure intensity budget and spectral resolution, what is the best illumination allocation per OPD sample (or time interval) and per pixel? These questions are answered through these three main contributions.

* **Coded and Structured Illumination.** We propose two novel CS FTI frameworks, referred to as ''Coded Illumination-FTI'' (CI-FTI) and ''Structured Illumination-FTI'' (SI-FTI). The first system globally codes the light source, i.e., whether the full biological specimen is globally highlighted or not per time slot, while in the second system we allow the illumination of each spatial location of the specimen to be independently coded over time, e.g., thanks to an SLM structuring the illumination of the specimen, before entering the FTI. The reconstruction of the HS volume resorts to the theory of CS by leveraging an HS low-complexity prior, i.e., spectral or joint spatiospectral sparsity models. By using a particular ''Variable Density Sampling''(VDS) scheme [2] we derive uniform (i.e., valid for all biological HS volumes) near-optimal reconstruction bounds in the sense of minimum amount of light exposure and its distribution for both CI-FTI and SI-FTI models.
* '''Biologically-friendly constrained light exposure coding.''' In fluorescence spectroscopy the tolerance of the fluorescent dyes, in terms of the light exposure budget, can be fixed by the biologists, e.g., according to the specification of fluorescent dyes. In this case, the total light exposure is a fixed resource that must be consumed exactly over the compressive FTI measurements while ensuring good reconstruction performance. We propose illumination strategies, both for CI-FTI and SI-FTI, that meet this context. This is done by adapting the intensity delivered by the light source according to the sample complexity -- the sufficient number of measurements required for a successful HS data recovery -- of these schemes. Interestingly, in this scenario, we observe that full (Nyquist rate) sampling does not achieve the best HS reconstruction quality.

* **Noise level estimation in VDS.** In CS, the reconstruction quality of an HS volume, e.g., from an {$\ell_1$}-minimization constrained by an {$\ell_2$}-fidelity term, critically depends on an accurate bound on the {$\ell_2$}-norm of the noise. In VDS, this fidelity term integrates a random weighting matrix -- with entries directly connected to the frequency sampling density -- whose presence prevents the use of common noise power estimator (such as a {$\mathcal{X}^2$}-bound for Gaussian noise energy). We propose a new estimator that only depends on the unweighted {$\ell_2$}- and {$\ell_\infty$}-norms of the noise. Moreover, this bound is instantiated to the case of an additive Gaussian noise.

{{<image src="cifti_sifti.png" width="700px" position="center" caption="**Figure 3:** Schematic of two proposed compressive FTI systems.">}}

### Related References
* **A. Moshtaghpour**, L. Jacques, V. Cambareri, P. Antoine, and M. Roblin, "A variable density sampling scheme for compressive Fourier transform interferometry," ''in SIAM Journal on Imaging Sciences,'' vol. 12, no. 2, pp. 671-715, 2019.
* L. Jacques and **A. Moshtaghpour**, "Structured illumination and variable density sampling for compressive Fourier transform interferometry," ''in proceedings of the International Biomedical and Astronomical Signal Processing (BASP) Frontiers workshop,'' Villars-sur-Ollon, 2019.
* **A. Moshtaghpour** and L. Jacques, "Multilevel illumination coding for Fourier transform interferometry in fluorescence spectroscopy," ''in proceedings of 25th IEEE International Conference on Image Processing (ICIP),'' Athens, 2018.
* **A. Moshtaghpour**, V. Cambareri, L. Jacques, P. Antoine, and M. Roblin, "Compressive hyperspectral imaging using coded Fourier transform interferometry," ''in proceedings of the Signal Processing with Adaptive Sparse Structured Representations workshop (SPARS),'' Lisbon, 2017.
* **A. Moshtaghpour**, V. Cambareri, K. Degraux, A. Gonzalez, M. Roblin, L. Jacques, and P. Antoine, "Coded-illumination Fourier transform interferometry," ''in proceedings of the Golden Jubilee Meeting of the Royal Belgian Society for Microscopy (RBSM),'' Brussels, 2016, pp. 65-66.
* **A. Moshtaghpour**, K. Degraux, V. Cambareri, A. Gonzalez, M. Roblin, L. Jacques, and P. Antoine, "Compressive hyperspectral imaging with Fourier transform interferometry," ''in proceedings of the international Traveling Workshop on Interactions between Sparse models and Technology (iTWIST),'' Aalborg, 2016, pp. 27-29.

### External References
[1] F. Krahmer and R.Ward, ''"Stable and robust sampling strategies for compressive imaging,"'' IEEE transactions on image processing, vol. 23, no. 2, pp. 612–622, 2014. <br>
[2] B. Adcock, A. C. Hansen, C. Poon, and B. Roman, ''"Breaking the coherence barrier: A new theory for compressed sensing,"'' in Forum of Mathematics, Sigma, vol. 5. Cambridge University Press, 2017.
### Collaborators
* Philippe Antoine (Lambda-X SA, Belgium)
* Matthieu Robin (Lambda-X SA, Belgium)
