---
title: "Compressive Optical Deflectometric Tomography"
slug: "CompressiveODT"
date: 2017-09-19
draft: false
type: "research"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of researches)
thumbnail: OpticalFibers.png

# Add a cover picture [optional]
cover: 
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: "The refractive index map allows the optical characterization of complex transparent materials such as optical fibers or intraocular lenses. This research topic addresses the problem of reconstructing the refractive index map of a transparent object from few amount of optical deflectometric measurements. We aim at developing a numerical reconstruction method which makes Optical Deflectometric Tomography compressive and robust to noise."

# Add categories of the research post (e.g. image processing)
categories: ["Inverse Problem", "Deflectometry"] 

# Add researchers involved
researchers: ["Adriana González", "laurent-jacques", "christophe-de-vleeschouwer"]
---

{{<image src="IOL.png" width="25%" position="center">}}


# Compressive Optical Deflectometric Tomography

The refractive index map (RIM) allows the optical characterization of complex transparent materials such as optical fibers or intraocular lenses, which is of great importance in manufacture and control processes. Optical Deflectometric Tomography (ODT) is an imaging modality that aims at reconstructing the spatial distribution of the refractive index of a transparent object from the deviation of the light passing through the object. By measuring this deviation for a great number of incident parallel light rays and several incidence angles, it is possible to reconstruct the refractive index map of the observed object. 

Common reconstruction methods like the Filtered Back-Projection (FBP) need a sufficient amount of incident angles to be able to accurately reconstruct the image. For few amount of observed angles, i.e. for a very ill-posed problem, and in the presence of noise in the ODT observations, the FBP reconstructed RIM presents some spurious artifacts. 

In this research topic, we aim at developing a numerical reconstruction method which makes Optical Deflectometric Tomography "compressive" in a similar way other compressive imaging devices which, inspired by the Compressed Sensing (CS) paradigm, reconstruct high resolution images from few (indirect) observations of its content. This ability would lead of course to a significant reduction of the ODT observation time with potential impact, for instance, in fast industrial object quality control relying on this technology. 

 {{<image src="OpticalFibers.png" width="25%" position="center">}}

This objective of compressiveness can only be reached by regularizing the ODT inverse problem by an appropriate prior model on the configuration of the expected RIM. Interestingly, the actual RIM of most human made transparent materials (e.g., optical fiber bundles, lenses, optics, …) is composed by slowly varying areas separated by sharp boundaries (material interfaces). Since this can be interpreted with a "Cartoon Shape" model, the inverse problem can be regularized by promoting a small Total-Variation norm. 

Furthermore, the objective is not only to make ODT compressive but also robust to the noise present in the measurements. This can be reached by adding a fidelity constraint with the available observations and providing a good noise characterization. 

Moreover, in this research topic we deal with a problem that exists in all physical applications, which is the proper estimation of the ODT sensing operator that fits better the physical acquisition. 

**References:** {{<citedial type="P" id="boreal:143487">}} {{<citedial type="P" id="boreal:122987">}} {{<citedial type="P" id="boreal:87531">}} {{<citedial type="P" id="boreal:87530">}}

**Collaborators:** 
*   Philippe Antoine (Lambda-X, Belgium) 
