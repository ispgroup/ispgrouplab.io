---
title: "Consistent Basis Pursuit in Quantized Compressed Sensing"
slug: "CoBP"
date: 2015-10-01
draft: false
type: "research"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of researches)
thumbnail: cobp.png

# Add a cover picture [optional]
cover: 
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: "This project focuses on the estimation of low-complexity signals when they are observed through M uniformly quantized compressive observations. Among such signals, we consider 1-D sparse vectors, low-rank matrices, or compressible signals that are well approximated by one of these two models. In this context, we prove the estimation efficiency of a variant of Basis Pursuit Denoise, called Consistent Basis Pursuit (CoBP), enforcing consistency between the observations and the re-observed estimate, while promoting its low-complexity nature."
# Add categories of the research post (e.g. image processing)
categories: ["Compressive Sensing", "Quantization", "Inverse Problem"]

# Add researchers involved
researchers: ["Amirafshar Moshtaghpour", "Valerio Cambareri", "Kevin Degraux", christophe-de-vleeschouwer, laurent-jacques]

---

## Definition of the problem

The theory of *Compressed Sensing* (CS) shows that many signals of interest can be reconstructed from a few linear, and typically random, observations. Interestingly, this reconstruction is made possible if the number of observations (or measurements) is adjusted to the intrinsic complexity of the signal, e.g., its sparsity for vectors or its low-rankness for matrices. Thus, this principle is a generalization of the Shannon-Nyquist sampling theorem, where the sampling rate is set by the bandwidth of the signal.

However, a significant aspect of CS systems is the effect of ''quantization'' on the acquired observations, in particular for the purpose of compression and transmission. This quantization is a non-linear transformation that both distorts the CS observations and increases, specially at low bit rates, the reconstruction error of CS reconstruction procedures.


## Our contributions
This project focuses on minimizing the impact of (scalar) quantization during the reconstruction of a signal from its quantized compressive observations. While accurate, more efficient quantization procedures, e.g., $\Sigma\Delta,$ universal, binned or vector quantizations exist in the literature, scalar quantization remains appealing for its implementation simplicity in most electronic devices, and for its robustness against measurement lost. 

Conversely to former attempts, which consider quantization distortion as additive Gaussian measurement noise and promote a Euclidean $(\ell\_2)$ fidelity with the signal observations as in the Basis Pursuit Denoise (BPDN) program, better signal reconstruction methods are reached by forcing consistency between the re-observed signal estimate and the quantized observations.

The simulation results, shown below, clearly highlights the advantage of consistent signal reconstruction when $M=K$ is large. Moreover, CoBP approaches an error decay of $M^{-1}$ similar to the distance decay of consistent K-sparse vectors.

{{<image src="cobp_results.png" width="80%" position="center" caption="Figure 1: (left) Gaussian QCS of sparse signals. (middle) Bernoulli vs Gaussian QCS of sparse signals. (right) Gaussian QCS of rank-1 matrices.">}}


**Related References**: {{<citedial type="P" id="boreal:167939">}} {{<citedial type="P" id="boreal:187182">}}
