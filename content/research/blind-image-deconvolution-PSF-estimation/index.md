---
title: "Blind Image Deconvolution for Non-parametric PSF estimation in Solar Astronomy no numb"
slug: "blind-image-deconvolution-PSF-estimation"
date: 2018-10-05
draft: false
type: "research"

toc: true
tocOpen: true
tocSide: left

thumbnail: solar-transit.png

# Add a cover picture [optional]
cover: solar-transit.png
coverCaption: Accumulation of observations of the Venus transit captured by SDO/AIA on June 5th - 6th 2012

# Add a short description [optional]
description: This research proposes to estimate the point spread function (PSF) of an telescope from the observation of the transit of a celestial body (e.g., the Moon or Venus) in front of the Sun. This is achieved by solving a regularized blind deconvolution problem. 

# Add categories (e.g. image processing)
categories: [ "Imaging", "Inverse Problem", "Blind Deconvolution", "Sparsity"]

# Researchers
researchers: ["Adriana Gonzalez", "laurent-jacques"]

---

## Description {.hide}

Blind image deconvolution is an inverse problem ubiquitous in several imaging applications (e.g., microscopical and astronomical imaging, photo enhancement, ...), where the image measured by the real optical instrument is given by the convolution of the ideal image and the response of the instrument (also called point spread function – PSF). Additionally, the image acquisition process is also contaminated by other sources of noise (e.g., read-out and photon-counting).

One focus of this research topic lies on the astronomical imaging application, where the optical characterization of instrumental effects is of high importance in order to extract accurate physical information from the observations. The problem of estimating both the PSF and the true image, called blind deconvolution, is ill-posed.

A blind deconvolution scheme that relies on image regularization is proposed. Contrarily to most methods presented in the astronomical imaging literature, the proposed method does not assume a parametric model of the PSF and can thus be applied to any telescope. Our scheme uses a wavelet analysis prior model on the image and weak assumptions on the PSF. We use observations from a celestial transit, where the occulting body can be assumed to be a black disk. These constraints allow retaining meaningful solutions for the filter and the image, eliminating trivial, translated and interchanged solutions. Under an additive Gaussian noise assumption, they also enforce noise canceling and avoid reconstruction artifacts by promoting the whiteness of the residual between the blurred observations and the cleaned data. Our method is applied to synthetic and experimental data. The PSF is estimated for the SECCHI/EUVI instrument using the 2007 Lunar transit [attach images/data], and for SDO/AIA using the 2012 Venus transit [attach images/data]). Results show that the proposed non-parametric blind deconvolution method is able to estimate the core of the PSF with a similar quality to parametric methods proposed in the literature. We also show that, if these parametric estimations are incorporated in the acquisition model, the resulting PSF outperforms both the parametric and non-parametric methods. 

{{<image src="VenusDeconv2.png" position="center" caption="Image reconstruction results for the Venus transit observed by SDO/AIA at 00:02 UT on June 6th 2012. Left and right images are without and with deconvolution, respectively. (Fig. 8 in [[1]](#fn:1))">}}

{{<image src="VenusDeconv.png" position="center" caption="Reconstruction results for a SDO/AIA image containing an active region. Left and right images are without and with deconvolution, respectively. (Fig. 10 in [[1]](#fn:1))">}}

{{<image src="VenusDeconv3.png" position="center" caption="Reconstruction results for a SECCHI/EUVI image containing an active region. Left and right images are without and with deconvolution, respectively. (Fig. 14 in [[1]](#fn:1))">}}

## Other Applications:

The blind deconvolution techniques discussed above do not exclusively apply to solar telescopes that have observed a solar transit. There exists many imaging applications where we have some knowledge about the intensity of a certain area of pixels in the image and its exact location. Another focus of this research topic lies on the medical imaging application, more especifically, on positron emission tomography (PET). This imaging modality aims at obtaining metabolic information of the human body from measurements of gamma rays emitted by tracers that have been previously injected in the body. In PET imaging, the recovered image containing the metabolic information is affected by the PSF of the instrument, which needs to be accurately estimated in order to obtain useful information from the images. Interestingly, the urinary bladder can be used as an anatomical landmark, since in a PET image the bladder is observed as an area where the pixels intensity remains constant, i.e., where the gradient of the image is known to be zero. The location of the urinary bladder in the image can be known with high precision by performing image registration of the available PET image with a computed tomography (CT) image. 


### Datasets of PSFs

From [^1], we make available to the community the following data (all available in this [ZIP archive](https://sites.uclouvain.be/ispgroup/uploads//Research/PSF_AIA_Secchi.zip), 2.3MB, and saved in standard Matlab data files). 

{{<image src="PSF0.png" position="left" caption="Logarithm of the non-parametric PSF estimated for SDO/AIA telescope (this figure is Fig. 6-(center) in [[1]](#fn:1))">}}

{{<image src="PSF1.png" position="left" caption="Logarithm of filters for the SDO/AIA telescope. (left) Parametric PSF estimated by Poduval et al. [[2]](#fn:2), i.e., $h_{p_1}$ in [[1]](#fn:1). (center) Diffraction mesh pattern modeled using the parameters estimated by Poduval et al. [[1]](#fn:1), i.e., $h_{p_2}$ in [[1]](#fn:1). (right) Combined parametric/non-parametric filters using the parametric PSF shown on the center, i.e., $h^∗_{p_2−n_p}$ in [[1]](#fn:1) (this figure is Fig. 7 in [[1]](#fn:1))">}}

- `AIA_Venus_PSF_NP.mat` and `AIA_Venus_PSF_P_NP_Poduval2.mat`: These are the PSFs of the AIA instrument obtained thanks to the Venus transit captured by SDO/AIA on June 5th - 6th 2012 (see Figs. 6-7 in [^1]). The first file contains the estimated non-parametric PSF from [^1] (Fig. 6-(center) in [^1]). The second file contains three different PSFs: the parametric PSF obtained by considering only the mesh diffraction components in the PSF estimated by Poduval et al. in [^2] (Fig. 7-(center) in [^1]), the non-parametric PSF obtained by our method (cropped on its center), and the parametric/non-parametric PSF formed by the convolution of these two (Fig. 7-(right) in [^1]).<br/>
Note that the AIA PSFs from [^2] are available on [author&rsquo;s website](http://gemelli.spacescience.org/~bpoduval/index.html). 

{{<image src="PSF2.png" position="left" caption="Logarithm of the filters for the SECCHI/EUVI telescope. (left) Parametric PSF given by the <em>euvi_psf.pro</em> procedure of SolarSoft, i.e., $h_{p_1}$ in [[1]](#fn:1). (center) Parametric PSF given by the <em>euvi_deconvolve.pro</em> procedure of SolarSoft, i.e., $h_{p_2}$ in [[1]](#fn:1). (right) Non-parametric PSF, i.e., $h^∗_{n_p}$ in [[1]](#fn:1). (this figure is Fig. 12 in [[1]](#fn:1))">}}

{{<image src="PSF3.png" position="left" caption="Logarithm of the combined parametric/non-parametric filters for the SECCHI/EUVI telescope. (left) Using the parametric PSF given by the <em>euvi_psf.pro</em> procedure of SolarSoft, i.e., $h^∗_{p_1−n_p}$ in [[1]](#fn:1). (right) Using the parametric PSF given by the <em>euvi_deconvolve.pro</em> procedure of SolarSoft, i.e., $h^∗_{p_2−n_p} in [[1]](#fn:1). (this figure is Fig. 13 in [[1]](#fn:1))">}}

- `Secchi_Moon_PSF_NP.mat`, `Secchi_Moon_PSF_P_NP_euvi.mat`, `Secchi_Moon_PSF_P_NP_Shearer.mat`: These files are related to the PSFs obtained for the Secchi instrument from the Moon transit on February 25th 2007 by the Extreme Ultraviolet Imager (EUVI) [^3] (see Figs. 12-13 in [^1]). The first file contains the estimated non-parametric PSF from [^1] (Fig. 12-(right) in [^1]). The second file contains three PSFs: the parametric PSF provided by the euvi_psf.pro procedure of Solar-Soft [^3] (Fig. 12-(left) in [^1]), the non-parametric PSF obtained by our method (cropped on its center), and the parametric/non-parametric PSF formed by the convolution of these two (Fig. 13-(left) in [^1]). The third file contains three PSFs: the parametric PSF obtained by Shearer et al. [^4] (Fig. 12-(center) in [^1]), the non-parametric PSF obtained by our method (cropped on its center), and the parametric/non-parametric PSF formed by the convolution of these two (Fig. 13-(right) in [^1]).<br/>
Note that the EUVI PSF from [^4] is available within IDL/SSW using the 'euvi_deconvolve.pro' routine. 


[^1]: A. Gonzalez, V. Delouille (SIDC/ROB, Belgium), L. Jacques, "Non-parametric PSF estimation from celestial transit solar images using blind deconvolution",  [J. Space Weather Space Clim.](http://www.swsc-journal.org/), 6, A1 (2016) [doi:10.1051/swsc/2015040](http://dx.doi.org/10.1051/swsc/2015040), arXiv:1412.6279, dial:2078.1/169498 

[^2]: B. Poduval, C. E. DeForest, J. T. Schmelz, and S. Pathak. Point-spread Functions for the Extreme-ultraviolet Channels of SDO/AIA Telescopes. ApJ, 765:144, March 2013.

[^3]: R. A. Howard, J. D. Moses, A. Vourlidas, J. S. Newmark, D. G. Socker, S. P. Plunkett et al. Sun Earth Connection Coronal and Heliospheric Investigation (SECCHI). Space Sci. Rev., 136:67–115, April 2008.

[^4]: P. Shearer, R. A. Frazin, A. O. Hero, III and A. C. Gilbert. The First Stray Light Corrected Extreme-ultraviolet Images of Solar Coronal Holes. ApJ, 749:L8, April 2012. 

## Collaborators
[Véronique Delouille](http://homepage.oma.be/verodelo/) (Solar physics and Space weather department, Royal Observatory of Belgium) 

