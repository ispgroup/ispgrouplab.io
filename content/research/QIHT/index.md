---
title: "Bridging 1-bit and High-Resolution Quantized Compressed Sensing with QIHT"
slug: "QIHT"
date: 2017-10-23
draft: false
type: "research"

toc: true
tocOpen: true
tocSide: left

thumbnail: blocks.png

# Add a cover picture [optional]
cover: blocks.png
# coverCaption: Accumulation of observations of the Venus transit captured by SDO/AIA on June 5th - 6th 2012

# Add a short description [optional]
description: "In the framework of Quantized Compressed Sensing, we tried to bridge two extreme cases: 1-bit and high resolution quantization. The requirement of consistency of the reconstructed signal with quantized measurement led us to a new reconstruction algorithm called Quantized IHT (QIHT) that outperforms classical algorithms (IHT and BPDN) at low resolutions."

# Add categories (e.g. image processing)
categories: [ "Sensing", "Imaging", "Compressive Sensing"]

# Researchers
researchers: ["Kévin Degraux", "christophe-de-vleeschouwer", "laurent-jacques"] 

---


## Description {.hide}

Compressed Sensing (CS), as it was orriginally described, provides a nice theoretical framework that allows one to recover sparse or compressible signals from a limited number of samples (i.e., below Nyquist rate) called compressive measurements.

Beyond the fancy promises of that beautiful theory, it remains undeniable that when it comes to deal with real application, a whole world of unexpected issues arises. Among these inevitable challenges, quantization has been the focus of much recent research. Quantization is a mapping of the continuous real axis to a finite discrete set of values that can be indexed and, for instance, represented with finite length binary words. It is, so to say, the translation of the continuous analog world into the discrete (finite precision) digital world.

In the framework of compressed sensing, quantization is naturally applied to the compressive measurements in order to be able to store, transmit and process them afterward. There are several concerns and possible tradeoffs:
* minimize the number of bits in the "measurement bitstream";
* maximize the reconstruction quality (e.g., measured in terms of SNR or Normalized SNR);
* minimize the acquisition time or the device complexity and power consumption;
* keep the reconstruction tractable by minimizing the reconstruction time (e.g. using greedy iterative algorithms);
* be robust to noise, perturbations, distortions, model non idealities and non linearity.

With this in mind, we tried to bridge two extreme cases that were already extensively studied in the litterature: 1-bit and high resolution quantization. We based our research on the idea that the reconstructed signal has to be '''consistent''' with the quantized measurements. This guiding principle led us to the development of an ''inconsistency energy'' and a new variant of the Iterative Hard Thresholding (IHT) called Quantized IHT (QIHT) which really bridges BIHT (Binary IHT i.e. for the 1-bit case) and IHT (for high resolutions i.e. when quantization error is small). Our numerical simulations showed that at small resolutions (when quantization error is far from negligible), QIHT outperforms classical IHT and the well known convex optimization problem called BPDN.

<div id="banner">
<div style="display:inline-block;">
    <image src="SNR-B-BPDN.png"  width="250px">
</div>
<div style="display:inline-block;">
    <image src="SNR-B-IHT.png" width="250px">
</div>
<div style="display:inline-block;">
    <image src="SNR-B-QIHT.png" width="250px">
</div>
</div>

[^1]: L. Jacques, K. Degraux, C. De Vleeschouwer. "Quantized Iterative Hard Thresholding: Bridging 1-bit and High-Resolution Quantized Compressed Sensing." Proceedings of the 10th International Conference on Sampling Theory and Applications (SampTA 2013) pp. 105-108. arXiv:1305.1786. 


