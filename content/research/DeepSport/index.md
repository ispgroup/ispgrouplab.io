---
title: "Deep learning for detection and analysis in team sport images"
slug: "DeepSport"
date: 2019-03-25
draft: false
type: "research"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of researches)
thumbnail: DeepSport.png

# Add a cover picture [optional]
cover: 
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: "In the context of indoor team sport events, detection and characterization of objects of interest for game strategy analysis and autonomous production and broadcasting from Convolutional Neural Networks."

# Add categories of the research post (e.g. image processing)
categories: ["Intelligent Vision", "Analysis", "Interpretation"]

# Add researchers involved
researchers: ["Christophe De Vleeschouwer", "Julien Moreau", "gabriel-vanzandycke", "Maxime Istasse"]
---
In the context of indoor team sport events, our goal is to detect and characterize objects of interest for game strategy analysis and autonomous production and broadcasting. Convolutional Neural Networks are considered for this purpose, taking panoramic views of sport scenes as inputs. Basketball images are challenging due to the lack of contrast between players and the background, high dynamics, occlusions and strong light reflections.

## Team discrimination with associative embedding

Team classification of players based on deep learning generally requires a fine-tuning to characterize the teams of each new game. To circumvent this constraint, we propose in {{<citedial type="P" id="boreal:219160">}} to extend a real-time players segmentation network to assign a descriptor vector, named embedding, to each pixel so as to support team clustering. Embeddings are trained to assign similar embeddings to teammates (same jersey color) while pushing away embeddings of opponents.

$K$-fold cross-game and cross-arena validation reveals that our method largely overcomes a standard clustering based on RGB player histograms, achieving more than 90% accuracy.

{{<image src="teamAE-illus.jpg" width="95%" position="center">}}


## Efficient ball detection

In the context of sport analytics, knowing the position of the ball is of major interest. But more specifically, knowing its position with high accuracy and with a small delay becomes critical for real-time automated production of sports events. This paper considers the task of ball detection from a single viewpoint in the challenging but common case where the ball interacts frequently with the players while being poorly contrasted with respect to the scene background. We propose in {{<citedial type="P" id="boreal:219159">}} to formulate the problem as a segmentation problem and adopt a multi-scale Convolutional Neural Network (CNN) architecture to find the ball whatever its actual size in the image.

Our inference model runs on a single capture, without temporal information. To take advantage of the ball dynamics, the difference to the previous image is added to the network input.

{{<image src="BallSeg-illus.jpg" width="95%" position="center">}}


## Projects

* [DeepSport](http://recherche-technologie.wallonie.be/fr/menu/acteurs-institutionnels/service-public-de-wallonie-services-en-charge-de-la-recherche-et-des-technologies/departement-de-la-recherche-et-du-developpement-technologique/direction-des-programmes-de-recherche/programmes-clotures/le-programme-walinnov/projets-finances/resume-walinnov-2016-deepsport.html)

