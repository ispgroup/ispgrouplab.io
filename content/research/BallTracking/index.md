---
title: "Ball detection and tracking: Foreground detector reinforcement through trajectory analysis"
slug: "BallTracking"
date: 2016-09-01
draft: false
type: "research"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of researches)
thumbnail: BallTracLogo.jpg

# Add a cover picture [optional]
cover: 
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: "In the context of team sport events monitoring, the various phases of the game must be delimited and interpreted. In the case of a basketball game, the detection and the tracking of the ball are mandatory. However, one of the difficulties is that the ball is often occluded by players. This project deals with the detection of the ballistic trajectory of a ball thrown between two players or toward the basket. Ballistic trajectories are build on the 3D ball candidates previously detected at each time-stamp from a foreground detector."

# Add categories of the research post (e.g. image processing)
categories: ["Intelligent Vision", "Analysis", "Interpretation"]


# Add researchers involved
researchers: ["christophe-de-vleeschouwer", "Amit Kumar K.C.", "Pascaline Parisot"]

---

{{<image src="ball_from_3D.png" width="50%" position="center">}}
{{<image src="ball_from_2D.png" width="50%" position="center">}}

In the context of team sport events monitoring, the various phases of the game must be delimited and interpreted. In the case of a basketball game, the detection and the tracking of the ball are mandatory.

The detection of the ball in a basketball game is not easy compared to the detection of the ball in a football or a tennis game. In fact, the ball is often occluded by players. Thus we deal with the detection of the ballistic trajectory of a ball thrown between two players or toward the basket.

We address the problem of the detection and the tracking of a ball in single-view and multi-view setting.

Our method is based on a foreground detector. It is divided into two consecutive parts and follows the “detect before tracking” approaches. The first part consists in 3D ball candidates detection and the second one in ballistic trajectory filtering based on a motion model. The second part is essential because, at some time-stamp, multiple detections as well as none happen.


Concerning the first part, two approaches, based on the foreground detector, are investigated to detect the 3D ball candidates.  The correlation-based (```Corr```) and the connected component
analysis (```CCA```) methods fundamentally differ in the way they
process the foreground masks:

* The correlation (```Corr```) approach scans a relatively dense grid of 3D points, and correlates each one of the 2D views with an approximated template of the ball silhouette. Such a process is computationally realistic in a limited 3D area, e.g. around the basket or the goal in a basket-ball or soccer game for example, but becomes intractable over large scenes.

* The  connected component analysis (```CCA```) approach starts by picking-up 2D ball position candidates from 2D images, and then triangulates to infer 3D candidates. This method might suffer when the ball silhouette is split into several pieces of foreground mask, but it scales reasonably well with the size of the scene.

Concerning the second part, to remove false positive detection, temporal analysis is performed. Generally, the sources of false positives can be accredited to players reflect, digit on the score board, noise, etc. Fortunately, these false detections do not follow the typical ballistic trajectory of the ball in the air. We have proposed two approaches to detect this trajectory. 

* The first one is based on the RANSAC approach. In the case of a multi-view setting, the ballistic trajectory model is express in 3D (```RANSAC```). In the case of a single-view setting, the ballistic trajectory model is express in 2D (```RANSAC 2D```).

* The second one is based on a graph (```Graph```).

## Video samples presenting the results of&nbsp;{{<citedial type="P" id="boreal:177090">}}

The ```filter``` videos refer to the performance obtained by filtering out the candidates that do not fit a ballistic model, while the ```trajectory``` videos display the performances by interpolating missed detections between the inliers, i.e. the candidate positions that follow a ballistic model, in addition to rejecting outliers. The results are computed on the APIDIS dataset composed of 7 cameras (available [here](code/apidis/)). Part of the code is available [here](https://drive.google.com/file/d/0BwvW-R5hWSBmZU9iY2hHS2huc1E/view?usp=sharing|here).

### Quantitative assessment based on a 3-minutes groundtruth

We present here under the videos associated to the tables measuring the ball detection accuracy in {{<citedial type="P" id="boreal:177090">}}.
* Red = detected candidates.
* Green = candidates that are validated based on trajectory analysis.

#### a) Whole field (see Fig. 6-(a) in the article)


**Trajectory**
| | <div style="width:300px">RANSAC</div> | <div style="width:300px">Graph</div> |
| --- | --- | --- |
|CCA | {{< youtube 7FG8eDiax7g >}}     | {{< youtube gh1B1CrWCNw >}}      |

**Filter**
| | <div style="width:300px">RANSAC</div> | <div style="width:300px">Graph</div> |
| --- | --- | --- |
|CCA | {{< youtube CBiURcwPjO4 >}}     | {{< youtube H_AwqY5qm40 >}}      |

#### b) Basket area (see Fig. 6-(b) in the article)

**Trajectory**
| | <div style="width:300px">RANSAC</div> | <div style="width:300px">Graph</div> |
| --- | --- | --- |
|Corr | {{< youtube upcyCy5l1Zs >}}     | {{< youtube ZkIjtUMIDTs >}}      |
|CCA | {{< youtube SZnfXnL1Goo >}}     | {{< youtube dQWZpHgOS6o >}}      |


#### c) Basket area, single-view setting (cam 7) (see Fig. 7 in the article)


**Trajectory**
| | <div style="width:300px">Corr</div> | <div style="width:300px">CCA</div> |
| --- | --- | --- |
|RANSAC 2D  | {{< youtube X8XXG_rxpnQ >}}     | {{< youtube VdAetVLRGbo >}}      |

### Qualitative assessment

We present videos for which the ground truth is not available. It extends the quantitative assessment on a longer APIDIS video segment, and on video segments captured in other basketball courts.

#### a) Whole field, multi-view setting (APIDIS second quarter) 

**Trajectory**
| | <div style="width:300px">RANSAC</div> | <div style="width:300px">Graph</div> |
| --- | --- | --- |
|CCA  | {{< youtube 3NHu7MQmzu0 >}}     | {{< youtube STEHWdSANDE >}}      |

#### b) Basket area, single-view setting, Corr, RANSAC 2D 

**Trajectory**
| | <div style="width:300px">seq. 1</div> | <div style="width:300px">seq. 2</div> |
| --- | --- | --- |
|cam 7   | {{< youtube 3toctF9JdrY >}}     | {{< youtube hpmtQRCoZQs >}}      |
| | <div style="width:300px">seq. 3</div> | <div style="width:300px">seq. 4</div> |
|cam 1   | {{< youtube _v1YzgzDRtk >}}     | {{< youtube KbNuD9KOB3k >}}      |
| | <div style="width:300px">seq. 5</div> | <div style="width:300px"></div> |
|cam 1   | {{< youtube llqOTymJmMY >}}     |      |

**References**: {{<citedial type="P" id="boreal:88937">}} {{<citedial type="P" id="boreal:125035">}} {{<citedial type="P" id="boreal:177090">}}
 
