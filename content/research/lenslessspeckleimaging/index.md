---
title: "Compressive Lensless Speckle Imaging"
slug: "lenslessspeckleimaging"
date: 2020-02-28
draft: false
type: "research"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

thumbnail: LE_principle.png

# Add a cover picture [optional]
cover: LE_principle.png
# coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: "The  lensless  endoscope (LE)  is  a  promising  device  to acquire _in vivo_ biological  images  at  a  cellular  scale.  In  addition to its high resolution, the tiny size of the probe allows  a  deep  exploration  of  the  tissues. This research aims at exploring acquisition strategies inspired by the compressive sampling theory  and  relying  on  two  key  properties  of  the  LE: (_i_) the ability to easily generate unstructured illumination patterns  by  randomly  programming  the  spatial  light  modulator and  (_ii_)  the  robustness  of  the  fiber  to  spatial  and  temporal distortion  allowing  the  use  of  fast  galvanometer  mirrors  to shift  light  patterns."

# Add categories of the research post (e.g. image processing)
categories: ["Medical Imaging", "Inverse Problem", "Speckle Imaging", "Compressive Sensing"]

# Researchers
researchers: ["Stéphanie Guérit", "john-lee", "laurent-jacques"]


---
# Compressive Lensless Speckle Imaging
## An imaging modality to explore the tissues at the cellular level
Imaging neural activity in functionally connected regions of the brain with cellular resolution is a long sought after goal in neuroscience. Tools such as two-photon fluorescence imaging of calcium activity has offered remarkable insight into the functional mapping of brain circuits. However, the scattering nature of brain tissue (Svaasand and Ellingsen, 1983) has precluded the functional imaging at regions deep in the brain such as CA3 layers of the hippocampus or the entorhinal cortex, and this is exacerbated in the case of freely behaving animals interacting with their environment.

While current devices suffer from physical limitations restricting either their spatial resolution or their imaging depth (Andresen _et al._, 2016), recent developments in adaptive optics, optical fibers and computational imaging techniques have opened a new class of imaging systems called _lensless endoscopes_ (LE) (Andresen _et al._, 2016). In these implementations, a bare fiber (multimode or multicore fiber bundle) in combination with wavefront shaping devices, is employed as an ultrathin imaging system. The extreme miniaturization of the imaging probe (diameter $\leq 200~\mu$m)  offers a minimally invasive route to imaging deep in freely moving animals.

Particularly, when employed in conjunction with wavefront shaping devices such as liquid crystal spatial light modulators (SLM), fiber-based lensless endoscopes offer the ability to focus a light beam and scan the sample volume with no access to the distal end of the fiber. This modality is reminiscent of confocal imaging systems and requires a careful calibration procedure, namely, one must measure the transmission matrix of the fiber system (_e.g._, with interferometric measurements). This knowledge is crucial to compensate the intrinsic mode scrambling in fibers and focus the beam at the distal end by accurately programming (beamforming) the SLM - hence avoiding a speckled intensity distribution instead of a focused beam (see Figures 1.a and 1.b).  However, this places several constraints on the usage of lensless endoscopes in realistic situations, the chief of which are (_i_) the interferometric sensitivity to perturbations during calibration and imaging, and (_ii_) the slow update rates of conventional SLMs that limit the imaging speed increasing in the same way the acquisition time.  

{{<image src="LE_PSF_focused.png" width="50%" position="center" caption="Figure 1.a: Simulated focused light beam.">}}
{{<image src="LE_PSF_speckle.png" width="50%" position="center" caption="Figure 1.b: Simulated speckle pattern obtained without fiber calibration.">}}


##  A playground for acquisition schemes based on compressed sensing principles

To acquire observations with a LE, we usually resort to point-scanning, also called raster scanning (RS). This acquisition strategy consists in scanning at constant rate each (discretized) position in field-of-view (FOV). This is achieved by focusing the light beam on a RS trajectory made of evenly spaced parallel lines. For each position, the fluorescence signal emitted by the biological sample towards the distal end travels back in the outer part of the optical fiber and is measured by a single-pixel detector. The image is then readily reconstructed, pixel by pixel, without any extra post-processing step (see Figure 2). This scanning technique requires preliminary beamforming calibration: specific sequences of proximal wavefronts are optimized to focus the light from all single mode cores on specific positions of the FOV.

Focusing the light beam is quite restricting for the design of the fiber as well as for the acquisition task (Sivankutty, 2018). Can we remove this demanding requirement of interferometric calibration required for generating a focused spot? Without such a  calibration, the illumination pattern generated by the fiber distal end is the result of the interference of multiple beams with random phases - a speckle pattern with multiple bright 'grains' of light and dark regions with no well-defined global peak as illustrated on Figure 1.b. 

This work explores new acquisition strategies relying on such speckles by combining elements of compressive sampling and the nice properties of the LE, namely its robustness to spatial and temporal distortion. This robustness combined with the memory effect of the fiber allows us to shift an illumination pattern at the distal end of the MCF by simply adding an extra phase ramp to the incoming light. The first strategy is unstructured compressive sampling (U-CS) acquisition leveraging $M$ distinct speckles as illumination patterns. The second one is a hybrid method, developing a structured CS strategies (S-CS), which reduces the number of distinct speckles by re-using them in a limited raster-scan mode. In both cases, an observation $y\_i$ can be seen as the scalar product of an illumination pattern and the object of interest $\boldsymbol x \in \mathbb R^N$,

$$\boldsymbol y = \mathcal P(\boldsymbol {\Phi V x}), $$

where rows of $\boldsymbol \Phi \in \mathbb R^{M\times N}$ correspond to the light patterns and $\boldsymbol V \in \mathbb R^{N\times N}$ is a diagonal matrix modeling the attenuation of both the illumination pattern and the sensitivity of the light collection (as induced by the fiber structure) closer to the periphery of a certain field-of-view.

What changes between the traditional and those two strategies is the generation of the patterns, and thus the definition of the acquisition matrix $\boldsymbol \Phi$. The structure of this sensing matrix has a huge impact on both the acquisition duration and the reconstruction time.


{{<image src="LE_tissue_xgt.png" width="50%" position="center" caption="Figure 2.a: Synthetic tissue image">}}
{{<image src="LE_tissue_y_RS.png" width="50%" position="center" caption="Figure 2.b: Observations acquired in simulated raster scanning mode.">}}

##  Related References

*  S. Guérit, S. Sivankutty, C. Scotté, J. A. Lee, H. Rigneault, and L. Jacques, “Compressive Sampling Approach for Image Acquisition with Lensless Endoscope,” in _the 4th International Traveling Workshop on Interactions between Sparse models and Technology (iTWIST)_, 2018.

##  External References

*  L. O. Svaasand and R. Ellingsen, “Optical properties of human brain,” _Photochemistry and photobiology_, vol. 38, no. 3, pp. 293–299, 1983.
*  E.   R.   Andresen,   S.   Sivankutty,   V.   Tsvirkun,   G.   Bouwmans,   and H.  Rigneault,  “Ultrathin  endoscopes  based  on  multicore  fibers  andadaptive optics: a status review and perspectives,” _Journal of Biomedical Optics_, vol. 21, no. 12, p. 121506, 2016.
*  S.  Sivankutty,  V.  Tsvirkun,  O.  Vanvincq,  G.  Bouwmans,  E.  Andresen, and H. Rigneault, “Nonlinear imaging through a Fermat’s golden spiral multicore fiber,” _Optics letters_, vol. 43, no. 15, pp. 3638–3641, 2018.


##  Collaborators

*  [Siddharth Sivankutty](https://www.fresnel.fr/spip/spip.php?rubrique424&act=sivankutty&lang=fr) (Institut Fresnel, Centrale Marseille, CNRS, Aix-Marseille Université)
*  [Camille Scotté](https://www.fresnel.fr/spip/spip.php?rubrique424&act=scotte&lang=fr) (Institut Fresnel, Centrale Marseille, CNRS, Aix-Marseille Université)
*  [Hervé Rigneault](https://www.fresnel.fr/spip/spip.php?rubrique424&act=rigneault&lang=fr) (Institut Fresnel, Centrale Marseille, CNRS, Aix-Marseille Université)

