---
title: "Automatic Team Sport Coverage"
slug: "AutonomousProduction"
date: 2017-09-01
draft: false
type: "research"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of researches)
thumbnail: final_four_moscou.jpg

# Add a cover picture [optional]
cover: 
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: Computer vision tools drive the automatic and democratic production of sport events video reports, by using scene analysis to select an appropriate viewpoint in a static and/or dynamic multi-camera infrastructure.

# Add categories of the research post (e.g. image processing)
categories: ["Intelligent Vision", "Content Acquisition Rendering", "Analysis", "Interpretation"]

# Add researchers involved
researchers: ["christophe-de-vleeschouwer", "Fan Chen"]
---
Smartphones and tablets promote a personalized consumption of video content {{<citedial type="P" id="boreal:89807">}}, making the democratic production of targeted content an exciting challenge for content providers. This research builds on computer vision tools to face this challenge in controlled scenarios, as most notably encountered for sport events. 

As depicted in the figure below, the automatic production is in charge of two main decisions, namely camera selection and image cropping parameters.

{{<image src="AutomaticProduction.png" width="75%" position="center">}}


In short, the production decisions in a static-camera infrastructure are derived from player (and ball) detection tools {{<citedial type="P" id="boreal:133326">}}, so as to optimize a trade-off between three factors:
*	*Completeness* counts the number of salient objects in the scene, and thereby measures the integrity of the camera/viewpoint selection; 
*	*Fineness* refers to the amount of details provided about the rendered scene. It is measured in terms of pixel resolution of the salient objects, and thereby favors close views selection. Increasing the fineness of a video does not only improve the viewing experience, but is also essential in guiding the emotional involvement of viewers through the use of close-up shots.
*	*Smoothness* refers to the graceful displacement of the camera viewpoint, and to the continuous story-telling resulting from the camera switching. Preserving smoothness is important to avoid distracting the viewer from the story with abrupt changes of viewpoint.

<br />

{{< youtube r_ovylcfUnk >}}

<br />

Based on the related scientific contributions {{<citedial type="P" id="boreal:88434">}},{{<citedial type="P" id="boreal:34394">}}, a fully automatic sport production solution has been developed within a European research project (www.apidis.org). The solution is now commercialized by Keemotion (www.keemotion.com) to cover team-sport events in a cost-effective manner, making the creation of video reports affordable, even in case of small or medium-size audience.

 
The following figures present two image samples resulting from the patented technology developed in those projects. In the first image, the technology is used to decide how to crop images in a panoramic view of the basket-ball field. In the second image, the production decisions include both camera selection and definition of image cropping parameters, as a function of the scene at hand.

{{<image src="CropPanoramicView.png" width="75%" position="center">}}
{{<image src="APIDISproduction.jpg" width="50%" position="center">}}

