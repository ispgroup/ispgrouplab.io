---
title: "Multiple object tracking with prior detections and graph formalisms"
slug: "MultiObjectTracking"
date: 2013-07-01
draft: false
type: "research"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of researches)
thumbnail: MultiObjectTracking.png

# Add a cover picture [optional]
cover: TrackingAPIDIS_small.png
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: "This project considers the tracking of multiple objects within video sequence(s). Fundamentally, it aims at formalising application scenarios in which reliability and the discriminability of the object features vary over time. In order to address problems involving large number of targets, and because automatic detection algorithms have gained maturity, our work assumes that a set of prior and plausible targets detections are available at each time instant."

# Add categories of the research post (e.g. image processing)
categories: ["Intelligent Vision", "Multiple Object Tracking", "Analysis", "Interpretation"]

# Add researchers involved
researchers: ["Amit Kumar K.C.", "christophe-de-vleeschouwer"]
---

Our research considers the tracking of multiple objects in video sequences. Even though fairly good solutions have been proposed in the literature to track a small number of isolated objects, tracking remains challenging when the target features’ ability to reliably discriminate objects varies across time and scene context. This is because most of the existing solutions assume that the features' relevance remains constant along the time, which generally simplifies the tracking formulation dramatically, typically by limiting the investigation of appearance similarities on short time neighbourhoods. In doing so, they assume that the inconsistency, measured along a path can be accurately measured by accumulation of the appearance dissimilarities measured between pairs of consecutive nodes.

In practice, however, most features are subject to non-stationary noise, and their relevance varies with time and space. This, for example, happens due to occlusions, illumination changes, scene complexity, etc. For example, color histograms appear to be quite noisy in presence of occlusions, and object positions do not help to disambiguate a clutter of targets. In other cases, highly discriminant features are only available sporadically. This occurs, for example, when cells are captured under changing illumination, each lightning condition emphasizing some specific characteristics of the cell. This case also happens when a feature is only visible in some observation configurations (e.g., a digit on a jersey of a player is only visible when facing the camera).


{{<image src="SporadicFeatures_new.png" width="75%" position="center" caption="Figure 1: Appearance features can be noisy and/or sporadic.">}}
{{<image src="SporadicFeaturesHelp_new.png" width="75%" position="center" caption="Figure 2: Sporadic features help to prevent incompatible associations.">}}


To address the above limitation, our research primarily aims at developing generic multi-object tracking methods that are able to cope with the variability of features relevance while matching the detections across time. In order to limit the scope of our research, we focus on the scenarios in which the plausible detections are known a-priori. Obviously, to fit the practical constraints of real-life cases, we do not assume that the pre-computed detections are reliable. They are just plausible.

In this context, we are currently investigating a deterministic framework using graph-based formalism.

## Related works and videos 

* {{<citedial type="P" id="boreal:134640">}} {{< youtube pgeYfUZkw8s>}}

* {{<citedial type="P" id="boreal:117017">}} Demo 1: {{< youtube UnIzDMIWZe4 >}} Demo 2: {{< youtube Mvj4GBBcQHU >}}

* {{<citedial type="P" id="boreal:114844">}} Demo: {{< youtube K2tYrEDwtLg >}}


## Collaborators

* Damien Delannay (Keemotion, Belgium)
