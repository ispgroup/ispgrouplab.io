---
title: "Imaging the Brain Microstructure"
slug: "BrainMicro"
date: 2017-09-19
draft: false
type: "research"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of researches)
thumbnail: microstructure.png

# Add a cover picture [optional]
cover: microstructure_alterations2.png
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: "Diffusion tensor imaging, the most common model of the diffusion signal, is unable to represent the signal arising from water molecules diffusing in different compartments such as multiple fascicles and the extra-axonal space. Multi-fascicle models overcome this limitation by providing a parametric representation for each compartment, allowing the development of new brain network modeling." 

# Add categories of the research post (e.g. image processing)
categories: ["Medical Imaging", "Image Processing"]

# Add researchers involved
researchers: ["Maxime Taquet", "benoit-macq"]

---
#  Imaging the Brain Microstructure

The brain function is supported by an architecture of interconnected neurons which transmit signals to one another. This transmission is enabled by a complex organization of cellular structures consisting of neurites, blood vessels and glial cells that altogether form the brain microstructure. Many neurological disorders are thought to be rooted in alterations of this microstructure. The brain microstructure can be characterized in vivo by fitting a model to diffusion-weighted magnetic resonance signals, sensitive to the diffusion of water molecules.

Diffusion tensor imaging, the most common model of the diffusion signal, is unable to represent the signal arising from water molecules diffusing in different compartments such as multiple fascicles and the extra-axonal space. Multi- fascicle models overcome this limitation by providing a parametric representation for each compartment. They are of great interest in population studies to characterize and compare properties of the brain microstructure.

Exploiting multi-fascicle models in population studies raises numerous challenges. First, the image acquisition required to reliably estimate multi-fascicle models is unclear and estimations from clinical data appear to fail. Second, selecting between different multi-fascicle models of various complexity and granularity is known to be challenging. Third, generating an atlas representing the microstructure of a standard anatomy and spatially aligning all estimated multi-fascicle models to this atlas require specific developments. Finally, comparing properties of multi-fascicle models between groups needs to be done in an anatomically and statistically sound manner.

This project addresses these challenges by providing methods to estimate multi-fascicle models from widely available clinical data, to reliably select the appropriate model, and to register and analyze multi-fascicle models. These methods form a comprehensive framework that harnesses multi-fascicle models in population studies of the brain microstructure. We show that this framework reveals unprecedented group differences in population studies of autism spectrum disorders. These differences can be replicated even with clinically available data.

The presented framework opens opportunities for new investigations of the brain microstructure in normal development and in disease and injury. It paves the way to the definition of microstructure-based biomarkers of neurological disorders.

## References

* Diffusion Tensor Imaging and Related Techniques in Tuberous Sclerosis Complex: Review and Future Directions, Jurrriaan M. Peters^*, Maxime Taquet^*, Anna K. Prohl, Benoit Scherrer, Agnies M. van Eeghen, Sanjay P. Prabhu, Mustafa Sahin, and Simon K. Warfield, Future Neurology, 2013, *: Equal contributions

* Estimation of a Multi-Fascicle Model from Single B-Value Data with a Population-Informed Prior, Maxime Taquet, Benoît Scherrer, Nicolas Boumal, Benoît Macq, Simon K. Warfield, to appear in proceedings of 16th international conference on Medical Image Computing and Computer Assisted Intervention (MICCAI), Setpember 2013, Nagoya, Japan

* Characterizing the DIstribution of Anisotropic MicrO-structural eNvironments with Diffusion-weighted imaging (DIAMOND), Scherrer B., Schwartzman A., Taquet M., Prabhu S.P., Sahin M., Akhondi-Asl A., Warfield S.K., Proc. of the 16th Int Conf Med Image Comput Comput Assist Interv (MICCAI), Nagoya, Japan, 2013

* Reliable Selection of the Number of Fascicles in Diffusion Images by Estimation of the Generalization Error, Benoît Scherrer*, Maxime Taquet*, Simon K. Warfield, in proceedings of 23rd biennial International Conference on Information Processing in Medical Imaging (IPMI), July 2013, Asilomar, USA, * Equal Contributions

* Registration and Analysis of White Matter Group Differences with a Multi-Fiber Model, Maxime Taquet, Benoît Scherrer, Olivier Commowick, Jurriaan Peters, Benoît Macq, Simon K. Warfield, in proceedings of Medical Image Computing and Computer Assisted Intervention, 2012 

* Interpolating Multi-Fiber Model by Gaussian Mixture Simplification, Maxime Taquet, Benoît Scherrer, Christopher Benjamin, Sanjay Prabhu, Benoît Macq, Simon K. Warfield, in proceedings of IEEE International Symposium on Biomedical Imaging, 2012

## Collaborators

* Simon K. Warfield, Professor, Boston Children's Hospital, Harvard Medical School
* Benoît Scherrer, PhD, Boston Children's Hospital, Harvard Medical School
* Jurriaan M. Peters, MD, Boston Children's Hospital, Harvard Medical School
