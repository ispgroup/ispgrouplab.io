---
title: "Imaging the Wallerian degeneration in the spinal cord"
slug: "SpinalCord"
date: 2013-10-01
draft: false
type: "research"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of researches)
thumbnail: SpinalCord.png

# Add a cover picture [optional]
cover: 
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: "This research is focused on the Wallerian degeneration process in the spinal cord. The purpose is to identify the best biomarkers into the observations of Wallerian degeneration process by diffusion weighted imaging in magnetic resonance imaging."

# Add categories of the research post (e.g. image processing)
categories: ["Medical Image Processing"]

# Add researchers involved
researchers: ["Damien Jacobs", "Stéphanie Guérit", benoit-macq]

---
In the Central Nervous System, CNS, MRI can be highly helpful to assess the white matter and the grey matter integrity and to observe its evolution in the course of time. These tissues are characterized by different cellular morphologies. The observation of water molecules diffusion by Diffusion Weighted Imaging (DWI) gives indirectly information on the different tissue morphologies. The figure 1 illustrates the contrast by DWI between the white and grey matters acquired on the spinal cord.

{{<image src="DWI_SC2.png" width="50%" position="center">}}

In the grey matter, the main presence of cell body makes an isotropic diffusion process of water molecules while in the white matter, the diffusion is anisotropic due to the presence of myelins and axonal fibers. Depending on the size of axonal diameter and myelin sheaths, the water mobility is modified. The figure 2 illustrates a Luxol Fast Blue staining with a Cresyl Violet counterstaining of the spinal cord at T10 level. The myelin is colored in blue and this substance is highly present in the white matter while the purple substance (Nissl body) is more present in the grey matter. By modeling several diffusion-weighted images, we can correlate DWI observations with histological staining.

{{<image src="LFB2.png" width="50%" position="center">}}

Our research is focused on different models such as Diffusion Tensor Imaging (DTI), Diffusion Kurtosis Imaging (DKI) and multi-fascicles model to get a high reliability between DWI and histological observations in the spinal cord. Because the axonal fibers mainly have a uniaxial orientation in the spinal cord, DTI and DKI models provide a good approximation for this structural organization. Theses models make possible to highlight different areas in the white matter linked to the axonal diameter and fibers densities.


After a spinal cord injury, the nervous tissues are altered and the wallerian degeneration (WD) process is triggered off. This phenomenon is characterized by different sequential steps and followed by the demyelination of axons. Imaging at successive periods provides longitudinal information on this WD process in the CNS. The purpose is to quantify the degeneration process by measuring the rate of demyelination depending on the time and the location. Intermediate step of WD such as the clearance in the nervous tissue by phagocytosis has be also assessed in the course of time.


These developments for assessing the white matter integrity non-invasively are performed in collaboration with Louvain Drug Research Institute (PhD Annes des Rieux, UCL, Belgium). Several studies develop new pharmaceutical strategies to recover sensor and motor functionalities after SCI. One of further application for DWI could be used to assess the efficiency of new strategies for spinal cord regeneration in the course of time.


### Related references
* D. Jacobs, A. des Rieux, A. Jankovski, J. Magat, B. Macq, V. Préat , B. Gallez, ''Echo Planar Imaging applied to high resolution diffusion tensor tractography of the spinal cord'', Magn. Res. Med., submitted.
* D. Jacobs, A. des Rieux, A. Jankovski, J. Magat, B. Macq, V. Préat , B. Gallez,''High Resolution Diffusion Tensor Imaging and Tractography In Vivo of Rat Spinal Cord Injury at 11.7T'', 5th Annual Meeting Benelux ISMRM, Rotterdam, January 2013
* D. Jacobs, B. Macq, B. Gallez, ''Removing diffusion weighted images of the spinal cord impaired by motion artifacts can bias the tensor estimation depending on gradient directions'', ISMRM Scientific Workshop, Podstrana, October 2013
* D. Jacobs, S. Guérit, M. Taquet, A. des Rieux, B. Macq, B. Gallez, ''Diffusion Kurtosis Imaging to evaluate non invasively the Wallerian degeneration in spinal cord injury : in vivo longitudinal study'', ISMRM Scientific Workshop, Podstrana, October 2013

### Collaborators
* Bernard Gallez, Professor, Louvain Drug Research Institute
* A. des Rieux , PhD, Louvain Drug Research Institute
