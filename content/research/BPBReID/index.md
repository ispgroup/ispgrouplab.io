---
title: "Body Part-Based Representation Learning for Occluded Person Re-Identification"
slug: "BPBReID"
date: 2023-07-03T15:06:40+02:00
draft: false
type: "research"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of researches)
thumbnail: part_based_reid_methods.jpg

# Add a cover picture [optional]
cover: part_based_reid_methods.jpg
coverCaption: A part-based re-identification method, offering fine-grained feature representation, explicit feature alignement and robustness to occlusions.

# Add a short description that will be shown on the list page [optional]
description: "BPBReID, a part-based re-identification method using body part feature representations to compute to similarity between two person images."

# Add categories of the research post (e.g. image processing)
categories: ["Deep Learning", "Computer Vision", "Person Re-Identification", "Multi-Object Tracking"]

# Add researchers involved
researchers: ["vladimir-somers", "christophe-de-vleeschouwer"]

# Mastodon comments
comments:
#   host: sigmoid.social
#   username: ispgroup
#   id: 

---
# Body Part-based Representation Learning for Occluded Person Re-Identification

## Introduction
Person re-identification, or ReID, is a retrieval task which aims at matching an image of a person-of-interest (the "query"), with other person images from a large database (the "gallery").
In this work, we propose BPBreID, a part-based method for occluded person re-identification.
We make two contributions:
1. An attention module to extract one feature vector for each body part.
2. The GiLt loss, a novel loss to train any part-based method.

Our code, poster, video are available on [GitHub](https://github.com/VlSomers/bpbreid) and paper on [arXiv](https://arxiv.org/abs/2211.03679).

## Motivations
Training part-based methods is challenging for two main reasons.

{{<image src="challenges.jpg" caption="">}}


Our two contributions aim at addressing these challenges.

## Our proposed method: BPBReID
Overview of the architecture of our model. 
BPBreID outputs K body part-based embeddings and three holistic embeddings.

{{<image src="architecture.jpg" width="80%" position="center">}}


### First contribution: The body parts attention module
Two training signals, from both the ReID objective and the part prediction objective. 
Learned attention is more accurate and ReID specialized!

{{<image src="attention_mechanism.jpg" width="50%" position="center">}}

### Second contribution: The GiLt loss
GiLt stands for Global-identity Local-triplet. 
Robust to occlusions and non-discriminative parts, because has always access to global information for solving the Re-ID objective.

{{<image src="gilt_loss.jpg" width="50%" position="center">}}

## Visualizations
Here are some ranking compared to other methods: gallery samples are ranked according to their similarity to the query, with the top retrieved sample on the left. 
As illustrated here, BPBReID is less sensitive to occlusions than previous methods.

{{<image src="visualization.jpg" caption="">}}
