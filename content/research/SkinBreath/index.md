---
title: "Respiratory motion model of lungs and tumor from skin surface as surrogate for radiotherapy"
slug: "SkinBreath"
date: 2014-09-19
draft: false
type: "research"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of researches)
thumbnail: thumbnail.png 

# Add a cover picture [optional]
cover: 
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: Respiratory induced motion of organs and tumors is a technical challenge in radiation therapy since it compromises treatment accuracy. Recently available optical surface scanners have opened the door of modeling and predicting internal motion from external skin surface motion.

# Add categories of the research post (e.g. image processing)
categories: ["Medical Image Processing"]

# Add researchers involved
researchers: ["Nicolas Gallego", "Jonathan Orban", "benoit-macq"]

---
Respiratory induced motion of organs and tumors is a technical challenge in radiation therapy since it compromises treatment accuracy. Currently, to measure internal motion during treatment, ionizing imaging should be used, but imaging doses could reach unacceptable levels. Recently available in room, optical surface scanners have opened the door to study motion models based on full tridimensional, time varying surface of the patient skin surface.

Tracking internal motion with the aid of external surrogates (as the surface) is possible in short term, during a treatment fraction, given that respiration is stabilized by means of audio/video coaching. In this case the amplitude and phase of the signal are observable from the surface. The main challenge involves inter-fraction variations, specifically the change on tumor baseline position, from one day to another. For these cases in fact, the models based only on surface do not predict the change. Then it must be estimated with the aid of several surrogate signals.

The objective of this project is the formulation of a motion model, based both surface and image as surrogates, that reduces imaging dose while improving motion estimates, not only to tumor but also to lungs and organs at risk {{<citedial type="P" id="boreal:190408">}}. The framework studied for the solution will be an adaptive model that allows changing the dependence on imaging and surface. Redundancy between surrogates will be studied to then exploit it, to rely more on surface data and less on imaging. The proposed methodology involves the extraction of surface deformation fields, then the relation to internal deformation fields, computed by deformable registration of 4D image and finally a model that maximizes on the covariance between surrogate and motion data.

**Collaborators:**

* In-vivo Dose Guided Therapy project: partnership between Université catholique de Louvain (UCL), Ion-Beam Applications (IBA) and Wallonia Region.
* Centre de Recherche en Acquisition et Traitement de l'Image pour la Sante (CREATIS), INSA Lyon.
* Center of Molecular Imaging, Radiotherapy & Oncology (MIRO) (UCL)
