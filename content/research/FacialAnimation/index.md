---
title: "Detection of facial expressions and CG animation"
slug: "FacialAnimation"
date: 2017-09-01
draft: false
type: "research"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of researches)
thumbnail: FacialAnimation.png

# Add a cover picture [optional]
cover: 
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: "This work is aimed at developing a semi-automate system to animate facial expressions. The system consists of detection of facial expressions and Computer Graphics animations of a facial charactor."

# Add categories of the research post (e.g. image processing)
categories: ["Image Analysis", "Interpretation"]


# Add researchers involved
researchers: ["Kaori Hagihara", "Pascaline Parisot"]

---

{{<image src="faceanim.png" width="75%" position="center">}}

For Computer Graphics (CG) film production, animation is a crucial step giving a dynamic representation into the scene. In terms of facial animations, different deformations to a facial model is applied in order to represent expressions following the scripts.  Even though this task is more or less mechanical, it requires a huge amount of manipulations on a graphical user interface.  In order to facilitate this task, we are developing a semi-automate system for animating a face of a CG character by capturing an actor with a kinect camera.  The facial action of an actor directly drives facial animation of a character.

Our system consists of a kinect camera, recording application, CG animation preview monitor, and a post-processing application.  The kinect camera inputs; RGB and depth images, are basically processed in two steps.  Firstly, the input images are analyzed to obtain a set of levels along facial action units (FACS), which are the fundamental actions of individual muscles or groups of muscles. In other words, we express actions at defined facial parts such as cheeks, lips, and eye blows numerically.  Secondly, using this level set, we project the expression of the actor to a character by deforming its 3D shape.  The character is deformed by merging its topologically identical 3D shape models at different expressions such as neutral, stretched lip, puff out a cheek, widely open an eye, etc at the maximum.  The merging weight to each shape model is computed from the level set of FACS.

**Collaborators**:
* Luc Petitot (RealReality.com)
