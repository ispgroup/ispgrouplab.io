---
title: "Adaptive Video Access and Personnalized Summarization"
slug: "PersonalizedSummarization"
date: 2017-09-19
draft: false
type: "research"


# Table of content
toc: true       # do you want to show a ToC ? [true, false]
tocOpen: true   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of researches)
thumbnail: PersonnalizedAccess.jpg

# Add a cover picture [optional]
cover: 
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: "Today’s media consumption evolves towards increased user-centric adaptation of contents, to meet the requirements of users having different expectations in terms of story-telling and heterogeneous constraints in terms of access devices. We propose personnalized summarization mechanisms, and adaptive streaming solutions to address this trend." 

# Add categories of the research post (e.g. image processing)
categories: ["Intelligent Vision", "Video Summarization", "Content Acquisition Rendering"]
# Add researchers involved
researchers: ["christophe-de-vleeschouwer", "Fan Chen"]

---

# Personalized Video Access and Summarization

Today’s media consumption evolves towards increased user-centric adaptation of contents, to meet the requirements of users having different expectations in terms of story-telling and heterogeneous constraints in terms of access devices. Individuals and organizations want to access dedicated contents through a personalized service that is able to provide what they are interested in, at the time when they want it and through the distribution channel of their choice.  

In this research, we consider two complementary schemes to distribute personnalized content to end-users:

* On the one hand, video summarization methods are envisioned to prepare a condensed and personalized video report from an extended raw video content. As depicted in the figure below, we formulate video summarization as a resource allocation problem, which uses the limited time resources to tell a story with the maximum benefit {{<citedial type="P" id="boreal:88435">}}{{<citedial type="P" id="boreal:88434">}}{{<citedial type="P" id="boreal:67327">}}. The main advantages offered by our ressource allocation framework are the efficiency of the resulting divide-and-conquer approach, and the genericity and flexibility resulting from the arbitrary definition of candidate sub-summaries and associated benefits.

{{<image src="SummarizationScheme.png" width="50%" position="center">}}

* On the other hand, specific client-server infrastructures are deployed to adapt the streamed video content to the interactive user requests {{<citedial type="P" id="boreal:89807">}}{{<citedial type="P" id="boreal:111346">}}{{<citedial type="P" id="boreal:111346">}}. In this framework, a 'content enhancement unit' spits the initial video content into (semantically consistent) segments that are encoded independently (=content segmentation) and potentially with distinct parameters (=content versioning). The server can then decide on the fly which segment to send as a function of the network constraints and/or user requests (playback speed control, replay request, etc). 

{{<image src="InteractiveContentAccess.png" width="50%" position="center">}}

Actual implementations of those schemes build on automatic video segmentation and (semi-)automatic video tagging {{<citedial type="P" id="boreal:88434">}}{{<citedial type="P" id="boreal:67327">}}{{<citedial type="P" id="boreal:111346">}} to offer semantically relevant and personalized access capabilities, thereby increasing the attractiveness and value of the native raw content with very limited human-ressources involvement.
