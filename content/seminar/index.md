---
title: Seminars @ISPGroup
mainClass : ISP-layout
contentClass: flex flex-column align-start
---
The ISPGroup organizes regular seminars, reading and watching groups through, respectively, 
- the [ISP Seminars (ISPS)](#ISPS), 
- the [IMAGe processINg rEading group](#imagine) (IMAGINE), 
- and the [Gauss\'IP watching group](#gaussip). 

## ISP Seminars (ISPS) {#ISPS}
{{<image src="ISPS-logo.png" style="width:80%; max-width:350px" position="float">}}

The ISP Seminars (ISPS) aim at jointly motivating PhD students to present their research (or another research of interest) and inviting external speakers on more specialized topics. They usually focus on intelligent vision, inverse problems and sparsity, biomedical signal & medical image processing, community detection for hierarchical image segmentation, biomedical date analysis and medical imaging. Generally, the ISP Seminars take place in the *Shannon seminar room (M-a105)*, Building Maxwell, 1st floor, place du Levant 3, Louvain-la-Neuve.
<!--
{{<image src="ISPS-logo.png" width="20%">}}
{{<image src="ISPS-logo.png" style="width:80%; max-width:350px" position="center">}}
-->

### Upcoming seminars 
{{<nextseminars limit="3">}}

### Previous seminars
{{<previousseminars limit="3">}}

A full list of all seminars is available [here](/seminars/)

### Location

{{<image src="seminar_location_map.png">}}


If you come by car, we advise you to park in the "Baudouin 1er parking". You'll find a map of the pedestrian path joining this parking to the seminar room [here](https://www.google.be/maps/dir/Parking+Malin+-+Baudouin/50.6686133,4.6233972/@50.6664823,4.6213553,17z/am=t/data=!3m1!4b1!4m9!4m8!1m5!1m1!1s0x0:0x76bcf6cdebb9768a!2m2!1d4.6220855!2d50.6643445!1m0!3e2?shorturl=1) (10' walk).


## IMAGINE: IMAGe processINg rEading group {#imagine}

## Gauss'IP {#gaussip}
