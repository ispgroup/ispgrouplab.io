---
title: "Vladimir Somers"
slug: "vladimir-somers"
date: 2023-07-03T14:42:37+02:00
draft: false
type: "people"

# fill the position
# must be one of the following: 'Professor', 'Academic' (for associate academic members), 'Post-doctoral researcher', 'Ph.D. Student', 'Administrative staff'
position: Ph.D. Student

# fill the picture (file inside folder)
picture: vladimir-somers.jpg

# array of affiliations (optional - each entry must have a 'text' and may have 'url')
affiliations:
    -
        text: "UCLouvain, Belgium"
        url: https://uclouvain.be/
    -
        text: "EPFL, Switzerland"
        url: https://www.epfl.ch/
    -
        text: "Sportradar"
        url: https://sportradar.com/
        
# add social links 
# this should be an array of map e.g. 
# [
#     {homepage: "..."},
#     {github: "..."},
#     ...
# ]
social: [
  {email: "vladimir.somers@gmail.com"},
  {github: "https://github.com/VlSomers"},
  {linkedin: "https://www.linkedin.com/in/vladimirsomers/"},
  {strackoverflow: "https://stackoverflow.com/users/6510944/vsomers"},
]

---
