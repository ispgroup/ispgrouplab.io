---
title: "John A. Lee"
slug: "john-lee"
date: 2022-11-18T23:16:33+01:00
draft: false
type: "people"

# fill the position
# must be one of the following: 'Professor', 'Academic' (for associate academic members), 'Post-doctoral researcher', 'Ph.D. Student', 'Administrative staff'
position: "Academic"

# fill the picture (file inside folder)
picture: picture.jpg

# array of affiliations (optional - each entry must have a 'text' and may have 'url')
affiliations:
    -
        text: "Institut de recherche expérimentale et clinique (IREC)"
        url: "https://uclouvain.be/fr/instituts-recherche/irec"
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics  (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "UCLouvain, Belgium"
        url: https://uclouvain.be/

# add social links 
# this should be an array of map e.g. 
# [
#     {homepage: "..."},
#     {github: "..."},
#     ...
# ]
social: [
    {homepage: https://uclouvain.be/fr/repertoires/john.lee},
	{linkedin: https://www.linkedin.com/in/john-lee-01230b1},
    {email: john.lee@uclouvain.be},
    {googlescholar: ""}
]
---
**John A. Lee** was born in 1976 in Brussels, Belgium. He received the M.S. degree in Applied Sciences (Computer Engineering) in 1999 and the Ph.D. degree in Applied Sciences (Machine Learning) in 2003, both from the Université catholique de Louvain (UCL, Belgium). His main interests are dimensionality reduction, intrinsic dimensionality estimation, clustering, vector quantization, as well as applications of image processing and artificial intelligence to radiation oncology and proton therapy. He is a member of the UCL Machine Learning Group and a Research Director with the Belgian F.N.R.S. (Fonds National de la Recherche Scientifique). Together with Michel Verleysen, he wrote a monography entitled ‘Nonlinear Dimensionality Reduction’ published by Springer-Verlag in 2007. His current work aims at developing image-guided, multi-modal, adaptive treatment strategies in the center of Molecular Imaging, Radiotherapy, and Oncology (IREC/MIRO).
