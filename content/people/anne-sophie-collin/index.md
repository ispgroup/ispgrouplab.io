---
title: "Anne-Sophie Collin"
slug: "anne-sophie-collin"
date: 2022-10-31T11:19:49+01:00
draft: false
type: "people"

# fill the position
# must be one of the following: 'Professor', 'Post-doctoral researcher', 'Ph.D. Student', 'Administrative staff'
position: Ph.D. Student

# fill the picture (file inside folder)
picture: annesophie-collin.jpg

# array of affiliations (optional - each entry must have a 'text' and may have 'url')
affiliations:
    -
        text: "Teaching Assistant at Ecole Polytechnique de Louvain (EPL)"
        url: https://uclouvain.be/fr/facultes/epl/
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "UCLOUVAIN, BELGIUM"
        url: https://uclouvain.be/


# add social links 
social: [
    {email: "anne-sophie.collin@uclouvain.be"},
    {github: "https://github.com/anncollin"},
    {googlescholar: "https://scholar.google.com/citations?user=KzWcitgAAAAJ&hl=en" }
]
---

I am a PhD student in Image Processing and Deep Learning at [UCLouvain](https://uclouvain.be/) under the supervision of Prof. <a href="../christophe-de-vleeschouwer">Christophe De Vleeschouwer</a>. My research investigates the use of a deep learning approach to address the anomaly detection problem in industrial vision. 

Besides, I am also teaching assistant at [Ecole Polytechnique de Louvain (EPL)](https://uclouvain.be/fr/facultes/epl/) of several courses related to: 
 - Image and signal processing
 - Machine learning
 - Medical imaging
 - Statistics

With a strong interest for education, I am a teaching assistant representative at the faculty.

## Code in Open Access
 Our Anomaly Detection framework is available on [Github](https://github.com/anncollin/AnomalyDetection-Keras). It uses an autoencoder trained on images augmented with our synthetic corruption noise. 
 The full paper is available on [ArXiv](https://arxiv.org/pdf/2008.12977.pdf) and has been presented at the International Conference on Pattern Recognition (ICPR) in 2020.


## Education
- MSc in Biomedical engineering, 2018
UCLouvain, Belgium
- BSc in engineering, 2016
UCLouvain, Belgium

## During free time
Since the age of 7, I always have been a passionnate musician playing multiple wind instruments. Active member of two bands, I involve myself fully to allow us to share our enthousiasm, humour and... music!

More recently, I discovered the joys of cycling and playing badminton. 
