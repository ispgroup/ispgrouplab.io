---
title: "Seyed Abolfazl Ghasemzadeh"
slug: "abolfazl-ghasemzadeh"
date: 2022-11-10T11:30:33+01:00
draft: false
type: "people"

# fill the position
# must be one of the following: 'Professor', 'Post-doctoral researcher', 'Ph.D. Student', 'Administrative staff'
position: Ph.D. Student

# fill the picture (file inside folder)
picture: abolfazl-ghasemzadeh.jpg

# array of affiliations (optional - each entry must have a 'text' and may have 'url')
affiliations:
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"

    -
        text: "FRIA, FNRS"
        url: "https://www.frs-fnrs.be/fr/"

    -
        text: "UCLouvain, Belgium"
        url: https://uclouvain.be/

# add social links 
# this should be an array of map e.g. 
# [
#     {homepage: "..."},
#     {github: "..."},
#     ...
# ]
social: [
    {email: "seyed.ghasemzadeh@uclouvain.be"},
    {github: "https://github.com/aghasemzadeh"},
    {googlescholar: "https://scholar.google.com/citations?user=famd3lQAAAAJ&hl=en" }, 
    {linkedin: "https://www.linkedin.com/in/aghasemzadeh/"},
    {gitlab: "https://gitlab.com/s.abolfazl.gh"},
]

---

## :books: Interests

* Computer vision
* 3D Human Pose Estimation
* Image Processing
* Machine Learning

## :mortar_board: Education

* MSc in Electrical Engineering, 2019
    *University of Tehran, Iran*
* BSc in Electrical Engineering, 2017
    *Isfahan University of Technology, Iran*

## Code

* [DeepSportLab](https://github.com/ispgroupucl/DeepSportLab)
    A framework for Pose Estimation and Segmentation of humans
