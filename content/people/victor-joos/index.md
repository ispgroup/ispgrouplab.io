---
title: "Victor Joos"
slug: "victor-joos"
date: 2022-10-27T14:59:42+02:00
draft: false
type: "people"

# fill the position
position: Ph.D. Student

# fill the picture (file inside folder)
picture: victor-joos.jpg

# array of affiliations (optional - each entry must have a 'text' and may have 'url')
affiliations:
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "ContrasT Team"
        url: "https://contrast-team.be/?page_id=3633"

# add social links 
social: [
    {email: "victor.joos@uclouvain.be"},
    {github: "https://github.com/victorjoos"},
]
---

## Interests
 - Biomedical image processing
 - Segmentation
 - Semi-supervised learning

## Code
 - [Deep Blueprint](https://github.com/ispgroupucl/deep-blueprint)
