---
title: "Mathieu Simon"
slug: "mathieu-simon"
date: 2024-08-15T20:13:16+02:00
draft: false
type: "people"

# fill the position
# must be one of the following: 'Professor', 'Post-doctoral researcher', 'Ph.D. Student', 'Administrative staff'
position: "Ph.D. Student"

# fill the picture (file inside folder)
picture: "ms.jpg"

# array of affiliations (optional - each entry must have a 'text' and may have 'url')
affiliations:
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "UCLouvain, Belgium"
        url: https://uclouvain.be/
    -
        text: "EPFL, Switzerland"
        url: https://www.epfl.ch/

# add social links 
social: [
    {email: "mathieu.simon@uclouvain.be"},
    {linkedin: https://www.linkedin.com/in/mathieu-simon-55412b1b6/},
    {googlescholar: "https://scholar.google.com/citations?user=Qlecmc8AAAAJ&hl=en&oi=ao"},
]
---

## :books: Interests

* Computer vision
* Disentangled Representation Learning
* Video and image compression

## :mortar_board: Education

* MSc in electrical engineering, 2021
    *UCLouvain, Belgium*
* BSc in engineering, 2019
    *UCLouvain, Belgium*
