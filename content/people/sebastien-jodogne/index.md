---
title: "Sebastien Jodogne"
slug: "sebastien-jodogne"
date: 2023-03-09T17:44:39+01:00
draft: false
type: "people"

# fill the position
# must be one of the following: 'Professor', 'Academic' (for associate academic members), 'Post-doctoral researcher', 'Ph.D. Student', 'Administrative staff'
position: "Academic"

# fill the picture (file inside folder)
picture: sjo.jpg

# array of affiliations (optional - each entry must have a 'text' and may have 'url')
affiliations:
    -
        text: "Computer Science Engineering (INGI)"
        url: "https://uclouvain.be/fr/instituts-recherche/icteam/ingi"
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "UCLouvain, Belgium"
        url: https://uclouvain.be/


# add social links 
# this should be an array of map e.g. 
# [
#     {homepage: "..."},
#     {github: "..."},
#     ...
# ]
social: [
    {homepage:  https://www.info.ucl.ac.be/~sjodogne/},
    {email: sebastien.jodogne@uclouvain.be},
    {googlescholar: "https://scholar.google.com/citations?user=QiHLvR4AAAAJ"},
	{linkedin: "https://www.linkedin.com/in/jodogne/"}
]

---
**Sébastien Jodogne** was born in 1979 in Oupeye (Belgium). He earned his Master's and Ph.D. degrees in computer science from the University of Liège (Belgium), respectively in 2001 and 2006. Between 2007 and 2011, he worked on research projects about high-performance image processing algorithms for machine vision, closed-circuit television, and broadcasting in private companies. He then served as a research engineer at the radiology, radiotherapy and nuclear medicine departments of the University Hospital of Liège between 2011 and 2016, where he designed the free and open-source Orthanc ecosystem for medical imaging. For his work on Orthanc, he received the 2014 Award for the Advancement of Free Software from the Free Software Foundation (FSF). Sébastien Jodogne now works as full-time Assistant Professor in the ICTEAM research institute of UCLouvain, in the field of computer science applied to life sciences. His research interests include health informatics, artificial intelligence, medical imaging, computer vision, and software engineering.
