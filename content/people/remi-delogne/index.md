---
title: "Rémi Delogne"
slug: "remi-delogne"
date: 2022-09-02T11:27:52+02:00
draft: false
type: "people"

position: "Ph.D. Student"

picture: me2.jpg

# Affiliations (short text , url links)
affiliations:
    -
        text: "Teaching Assistant"
    -
        text: "Mathematical Engineering Department (INMA)"
        url: "https://uclouvain.be/fr/node/2107"
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "UCLOUVAIN, BELGIUM"
        url: https://uclouvain.be/

# Social
social: [   
    {email: "remi.delogne@uclouvain.be"},
    {googlescholar: "https://scholar.google.com/citations?user=2M2g8_AAAAAJ&hl=en"},    
    {linkedin: "https://www.linkedin.com/in/remi-d-24644b185/" },
    {instagram: "http://instagram.com/remidlgn?igshid=ZDdkNTZiNTM="},
    {twitter: "https://twitter.com/r_dlge"},
        ]

---

# Life

After earning a degree in mathematics at the university of Durham (UK), I completed a masters degree at the university of Namur (Belgium) in mathematics with a specialisation in Data Science. I then took up a Teaching Assistant position at the UCLouvain (Belgium), where I currently studying for a PhD under the supervision of Pr. Laurent Jacques.


# Current interests
- Compressive Sensing
- Random Projections
- Sketching
# Research

My research currently focuses on signal processing after random sketching. Random sketching involves the projection of a signal (an image or time series for example) using a random procedure. Standard sketching is achieved using a multiplication by a random matrix. I focus instead on non-linear sketching, more precisely quadratic sketching. This is motivated by the recent development of an optical machine called Optical Processing Unit (OPU), capable of applying the sketching at the speed of light (literally) and at very low power. Unfortunately, the non-linear nature of this transformation means that standard signal processing techniques found in classical compressive sensing fail and new methods are needed.

# Outside of work
Outside of work I am a keen rower, I rowed to an elite level back in the UK and I am now trying to keep up the appearance in the Namur rowing club. I am also very fond of cycling, especially long trips around Europe, and hopefully one day the world.

At university I am representative of all PhD and PostDoc student and work along with authorities to alert them about various issues that researchers face.



