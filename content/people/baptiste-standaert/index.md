---
title: "Baptiste Standaert"
slug: "baptiste-standaert"
date: 2022-11-02T10:05:12+01:00
draft: false
type: "people"

# fill the position
# must be one of the following: 'Professor', 'Post-doctoral researcher', 'Ph.D. Student', 'Administrative staff'
position: "Ph.D. Student"

# fill the picture (file inside folder)
picture: "bs.jpeg"

# array of affiliations (optional - each entry must have a 'text' and may have 'url')
affiliations:
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "UCLouvain, Belgium"
        url: https://uclouvain.be/

# add social links 
social: [
    {email: "baptiste.standaert@uclouvain.be"},
    {github: "https://github.com/bstandaert"},
    {linkedin: https://www.linkedin.com/in/baptiste-standaert-a611921b7/},
    {googlescholar: "https://scholar.google.com/citations?user=2okOYRIAAAAJ&hl=fr"},
]
---

## :books: Interests

* Computer vision
* Efficient deployment of convolutional networks on embedded systems
* Compression of convolutional networks by quantization
* Object tracking and re-identification

## :mortar_board: Education

* MSc in electrical engineering, 2021
    *UCLouvain, Belgium*
* BSc in engineering - computer science, 2019
    *UCLouvain, Belgium*
