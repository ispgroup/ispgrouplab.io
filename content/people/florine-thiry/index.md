---
title: "Florine Thiry"
slug: "florine-thiry"
date: 2022-11-04T15:23:20+01:00
draft: false
type: "people"

# fill the position
# must be one of the following: 'Professor', 'Post-doctoral researcher', 'Ph.D. Student', 'Administrative staff'
position: "Ph.D. Student"

# fill the picture (file inside folder)
picture: florine.png

# array of affiliations (optional - each entry must have a 'text' and may have 'url')
affiliations:
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "UCLouvain, Belgium"
        url: https://uclouvain.be/
# add social links
social: [
    {email: florine.thiry@uclouvain.be},
    {linkedin: https://www.linkedin.com/in/florine-thiry-b30876210/},
]
---

## Interests
* Image processing and computer vision
* Keypoints and pose detection
* Contrastive learning

## Education
* MSc in electrical engineering, 2021
    *UCLouvain, Belgium*
* BSc in engineering, 2019
    *UCLouvain, Belgium*
