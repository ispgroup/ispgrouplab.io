---
title: "P.A. Absil"
slug: "pa-absil"
date: 2022-11-18T23:29:43+01:00
draft: false
type: "people"

# fill the position
# must be one of the following: 'Professor', 'Academic' (for associate academic members), 'Post-doctoral researcher', 'Ph.D. Student', 'Administrative staff'
position: "Academic"

# fill the picture (file inside folder)
picture: picture.png

# array of affiliations (optional - each entry must have a 'text' and may have 'url')
affiliations:
    -
        text: "Mathematical Engineering Department (INMA)"
        url: "https://uclouvain.be/fr/node/2107"
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "UCLouvain, Belgium"
        url: https://uclouvain.be/


# add social links 
# this should be an array of map e.g. 
# [
#     {homepage: "..."},
#     {github: "..."},
#     ...
# ]
social: [
    {homepage: https://sites.uclouvain.be/absil/},
    {linkedin: https://www.linkedin.com/in/pierre-antoine-absil-56b39a82},
    {email: pa.absil@uclouvain.be},
    {googlescholar: "https://scholar.google.be/citations?user=VYcCB1cAAAAJ"}
]

---
**Pierre-Antoine Absil** is professor of applied mathematics at the Université catholique de Louvain (UCL) in Louvain-la-Neuve, Belgium. He received an engineering degree (ingénieur civil physicien, 1998) and a PhD in applied sciences (2003) from the University of Liège, Belgium. In the academic year 1998-1999, he was supported by a Pisart research fellowship from the University of Liège. From 1999 to 2003, he was a Research Fellow with the Belgian National Fund for Scientific Research (Aspirant du FNRS). In 2003-2005, he was a Postdoctoral Associate with the School of Computational Science at Florida State University. He was a Research Fellow with Peterhouse, University of Cambridge (UK) in 2005-2006. He was a visitor with Monash University, Australia (2001), the University of Würzburg, Germany (2002), Sandia National Laboratories, USA (2005), the Australian National University (2009), and Université Paul Sabatier, Toulouse, France (2010). Dr Absil's major research area is numerical optimization, with particular interests in numerics on manifolds, linear and nonlinear programming algorithms, and biomedical applications. He was awarded the 2000 IBRA-BIRA prize for his Graduate Thesis (analysis of cardiological signals) and the 2002 SIAM Student Paper Prize for the paper "A Grassmann-Rayleigh Quotient Iteration for Computing Invariant Subspaces". 

**For more information, you can consult his homepage on https://sites.uclouvain.be/absil/**
