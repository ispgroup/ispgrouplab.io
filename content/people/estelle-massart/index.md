---
title: "Estelle Massart"
slug: "estelle-massart"
date: 2022-12-13T09:26:26+01:00
draft: false
type: "people"

# fill the position
# must be one of the following: 'Professor', 'Academic' (for associate academic members), 'Post-doctoral researcher', 'Ph.D. Student', 'Administrative staff'
position: "Academic"

# fill the picture (file inside folder)
picture: estelle_portrait.jpg

# array of affiliations (optional - each entry must have a 'text' and may have 'url')
affiliations: 
    -
        text: "Mathematical Engineering Department (INMA)"
        url: "https://uclouvain.be/fr/node/2107"
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "UCLouvain, Belgium"
        url: https://uclouvain.be/

# add social links 
# this should be an array of map e.g. 
# [
#     {homepage: "..."},
#     {github: "..."},
#     ...
# ]
social: [
    {homepage: https://perso.uclouvain.be/estelle.massart/},
    {email: estelle.massart@uclouvain.be},
    {googlescholar: "https://scholar.google.be/citations?user=F_xNQ7IAAAAJ"}
]

--- 
**Estelle Massart** graduated as an engineer in applied mathematics at [UCLouvain](http://www.uclouvain.be/index.html), Belgium, in 2015, and received a PhD in mathematical engineering from UCLouvain in 2019, under the supervision of [Julien Hendrickx](https://perso.uclouvain.be/julien.hendrickx) and [Pierre-Antoine Absil](https://sites.uclouvain.be/absil/). My PhD thesis, entitled *Data fitting on positive semidefinite matrix manifolds*, was awarded the XXI Alston Householder prize. From 2020 to 2022, I was a postdoctoral researcher with the [Mathematical Institute](https://www.maths.ox.ac.uk/) at the [University of Oxford](https://www.ox.ac.uk/), where I was funded by the [National Physical Laboratory](https://www.npl.co.uk/). From January to August 2022, I was an FNRS scientific collaborator at UCLouvain, in the [mathematical engineering department](https://uclouvain.be/fr/node/2107), a subdivision of the [ICTEAM institute](https://uclouvain.be/fr/node/1991). From September 2022, I am an academic faculty member of UCLouvain, working in the mathematical engineering department.


**For more information, you can consult her homepage on https://perso.uclouvain.be/estelle.massart/**
