---
title: "Christophe De Vleeschouwer"
slug: "christophe-de-vleeschouwer"
date: 2022-09-07T17:56:01+02:00
draft: false
name: "Christophe De Vleeschouwer"
type: "people"

# fill the position
position: "Professor"

# fill the picture (file inside folder)
picture: CDV_pic.jpg

# array of affiliations (optional - each entry must have a 'text' and may have 'url')
affiliations:
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "UCLouvain, Belgium"
        url: https://uclouvain.be/

# add social links 
social: [
    {email: christophe.devleeschouwer@uclouvain.be}
]
---

Christophe De Vleeschouwer was born in Namur, Belgium, in 1972. He received the Electrical Engineering degree and the Ph. D. degree from the Université catholique de Louvain (UCL) Louvain-la-Neuve, Belgium in 1995 and 1999 respectively. He was a research engineer with the IMEC Multimedia Information Compression Systems group (1999-2000). 

He has been a visiting Research Fellow at [UC Berkeley](http://www-video.eecs.berkeley.edu "UC Berkeley") (2001-2002), a post-doctoral researcher at [EPFL](http://lts4www.epfl.ch/ "EPFL") (2004), and a visiting scholar at CMU (2014). He is now a [Belgian NSF](http://www.fnrs.be/ "Belgian NSF") senior research associate at UCL in the ICTEAM institute. 

He has been the coordinator of the APIDIS FP7 project ( www.apidis.org ), and a co-founder of Keemotion (www.keemotion.com). He also served as an Associate Editor for IEEE Transactions on Multimedia.
