---
title: "Olivier Leblanc"
slug: "olivier-leblanc"
date: 2022-09-02T11:27:52+02:00
draft: false
type: "people"

position: "Ph.D. Student"

picture: ol.jpg

# Affiliations (short text , url links)
affiliations:
    -
        text: "FNRS Research Fellow"
    -
        text: "Mathematical Engineering Department (INMA)"
        url: "https://uclouvain.be/fr/node/2107"
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "UCLOUVAIN, BELGIUM"
        url: https://uclouvain.be/

# Social
social: [   
    {email: "olivier.leblanc@uclouvain.be"},
    {github: "https://github.com/olivierleblanc/"},
    {gitlab: "https://reivilo3.gitlab.io/"},
    {googlescholar: "https://scholar.google.com/citations?user=PXm-ig8AAAAJ&hl=fr&oi=sra"},
    {orcid: "https://orcid.org/0000-0002-3708-9374"},
]
---

I am a PhD candidate supervised by <a href="../laurent-jacques">Laurent Jacques</a> at the Image and Signal Processing Group (ISPGroup), in the Mathematical engineering (INMA) department of the ICTEAM institute at UCLouvain, Belgium. 

## Current interests
- Inverse Problems in Optics and Biomedical Sciences
- Compressive Sensing and Computational Imaging
- Image and Signal Processing

### Research

My research is about drawing on signal processing insights to reduce the computational cost of machine learning methods; see more on my research page. Don't hesitate to also check out my list of publications and related code.

My research topic is *GeneCI: Physically-Driven and Generative Neural Networks for Computational Imaging*.

The goal of my research project is to tackle computational imaging (CI) applications that suffer both from the ill-posedness of the imaging process and high computational and memory complexities. I decided to focus on fluorescent lensless endoscopy (LE) with ultra thin multicore optical fibers (MCF), extending the work of Stephanie Guérit, a recently graduated PhD student of my current supervisor, Prof. <a href="../laurent-jacques">Laurent Jacques</a>.
    
* ###  What is Lensless Endoscopy and what challenges does it raise?
The lensless endoscope (LE) is a promising device to acquire in vivo images at a cellular scale. The challenges raised by LE are twofold: (i) first, the miniaturization of this endoscope (its cross section amounts to a few hundreds of microns) prevents it from direct imaging using a lens, both for manufacturing and nonlinear optical effects issues (ii) second, the light actuallycaptured by the device represents a small fraction of the initially emitted light, either by direct reflection or fluorescence (occurring when the biological sample captures part of the energy of the incoming light and re-emits it at another larger wavelength). Hence, one requires to provide sufficient illumination power combined with a sensitive enough light sensor to hope for satisfying image reconstruction. These issues make LE still an open-problem for provable recovery andreal-time usage in concrete biomedical applications, compared to more matured applications such as MRI, confocal microscopy and refraction tomography.


{{<image src="LEMCF.png" position="center" caption="Figure 1. Working principle of a lensless endoscope viewed as an interferometric machine (legend: MCF: multicore optical fiber)" width="600px">}}

* ### What problems do I focus on?
Here are some axes I'm considering for my PhD research:
      
* interferometric Lensless Endoscopy:
    Including the physical constraints of a LE, I refined the sensing model for a 2D object, introducing the physics of electromagnetic wave propagation, more precisely the Rayleigh-Sommerfeld equation in the Far-Field assumption, for now. This new model called Interferometric Lensless Endoscopy (ILE) brings multiple advances both in a theoretical and practical point of view. 

    {{<image src="schema.png" position="center" caption="Figure 2. Interferometric LE and its link with ROPs of the interferometric matrix." width="600px">}}
    
* Tomographic lensless endoscopy: LE is so far limited to the observation of planar objects perpendicular to the optical fiber distal end (see Fig. 1); and this is still thecase with the ILE model described above. I plan to extend this modality to tomographic LE (TLE), namely to the estimation of a biological sample volume (the density offluorophore) by collecting the light re-emitted under a controlled illumination pattern.Depending on what can be done in 3D with the ILE model, I will either model the forward acquisition using a 3D extended ILE model or with a physically-driven neural network (NN),where the NN activation functions represent light-matter interaction, and the NN weights correspond to a discrete 3D representation of the sample. 
    
* Improved TLE modeling with generative networks: 
    TLE crucially depends on (i) an accurate model for the illumination pattern used to probe the biological sample, and (ii) a suitable representation of the sample volume to regularize the related inverse problem. I will tackle the first point by learning the transformation of the wave front operated by the MCF, using a supervised (or inferent) generative network (s-GN) learned in an offline calibration  stage. The second point will be solved by leveraging unsupervised GNs (u-GNs) and their highly compressed latent space representation. These appealing alternatives to analytical representations of images (such as sparse wavelet representation or low-rank models) willbe learned with generative adversarial networks or variational autoencoders.

### Affiliations
I am funded by the Belgian "Fonds National de la Recherche Scientifique" (F.R.S.-FNRS), which granted me 4 years of research funding as "Aspirant FNRS", starting in October 2020.

I'm doing my PhD at UCLouvain (Université catholique de Louvain, the university in Louvain-la-Neuve, Belgium). More precisely, I am affiliated to the ICTEAM research institute, and within it, the Electrical Engineering department (ELEN). Finally, I am a member of the Image and Signal Processing Group (ISPGroup), active in UCLouvain.

### Besides research...
I'm doing sport almost everyday. I'm playing soccer and tennis. I also often go to the gym with friends.

Very curious and passionate by science, I regularly watch videos on YouTube which vulgarize many science subjects, either very technical or also about philosophy, economy, history, ... On a smaller measure, I like reading some non-fiction stuff similar to the videos I watch.

Working daily on my pc, I'm listening to music all day, with the type depending on the period (mostly rock, electro and rap).

I'm currently self learning piano :)


## Education
- MSc in Electrical engineering, 2020
UCLouvain, Belgium
- BSc in Electromechanical engineering, 2018  
UCLouvain, Belgium

## Appointments
- IEEE Student Branch: organizer of technical tutorials and industrial seminars.
