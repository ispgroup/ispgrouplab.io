---
title: "Benoit Brummer"
slug: "benoit-brummer"
date: 2023-01-30T17:00:12+01:00
draft: false
type: "people"

position: Ph.D. Student

picture: Trougnouf_selfie_along_the_GR-1_in_Parc_naturel_régional_de_la_Haute_Vallée_de_Chevreuse_(IMG_20210904_191415)_squared.jpg

affiliations:
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "UCLouvain, Belgium"
        url: https://uclouvain.be/


social: [
{homepage: https://commons.wikimedia.org/wiki/User:Trougnouf},
{mastodon: https://fosstodon.org/@trougnouf},
{github: https://github.com/trougnouf},
{linkedin: https://www.linkedin.com/in/benoit-brummer-05a27a47}

]

---
