---
title: "Gabriel Van Zandycke"
slug: "gabriel-vanzandycke"
date: 2022-11-20T14:01:01+01:00
draft: false
type: "people"

# fill the position
# must be one of the following: 'Professor', 'Academic' (for associate academic members), 'Post-doctoral researcher', 'Ph.D. Student', 'Administrative staff'
position: Ph.D. Student
picture: gabriel-vanzandycke.jpg

affiliations:
    -
        text: "Teaching Assistant at Ecole Polytechnique de Louvain (EPL)"
        url: https://uclouvain.be/fr/facultes/epl/
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "UCLouvain, Belgium"
        url: https://uclouvain.be/
    -
        text: "Sportradar"
        url: https://sportradar.com/

social: [
    {email: "gabriel.vanzandycke@protonmail.com"},
    {github: "https://github.com/gabriel-vanzandycke"},
    {gitlab: "https://gitlab.com/gabriel-vanzandycke"},
    {googlescholar: "https://scholar.google.com/citations?user=-tTGjowAAAAJ"},
    {orcid: "https://orcid.org/0000-0002-8384-4166"}
]

---

I'm a PhD student in Image Processing and Computer Vision under the supervision of Prof. <a href="../christophe-de-vleeschouwer">Christophe De Vleeschouwer</a>. My PhD was funded by the walloon region under the DeepSport project, and Sportradar. I work on basketball scenes understanding.
