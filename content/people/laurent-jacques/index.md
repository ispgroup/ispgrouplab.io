---
title: "Laurent Jacques"
slug: "laurent-jacques"
date: 2022-09-02T11:27:52+02:00
draft: false
type: "people"

position: "Professor"

picture: lj.jpg

location: https://laurentjacques.gitlab.io/

# Affiliations (short text , url links)
affiliations:
    -
        text: "Professor, FNRS Senior Research Associate"
    -
        text: "Mathematical Engineering Department (INMA)"
        url: "https://uclouvain.be/fr/node/2107"
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "UCLouvain, Belgium"
        url: https://uclouvain.be/

# Social
social: [
    {homepage: https://laurentjacques.gitlab.io},
    {linkedin: https://be.linkedin.com/in/laurent-jacques-6641579},
    {email: laurent.jacques@uclouvain.be},
	{mastodon: https://mathstodon.xyz/@lowrankjack},
	{twitter: https://twitter.com/jacquesdurden},
    {github: https://github.com/jacquesdurden/},
    {gitlab: https://laurentjacques.gitlab.io/},
    {googlescholar: "https://scholar.google.com/citations?hl=en&user=zo8lijwAAAAJ&view_op=list_works&pagesize=100"},
    {orcid: https://orcid.org/0000-0002-6261-0328}
]
---
# Short biography

**Laurent Jacques** is a Professor and FNRS Senior Research Associate at the Image and Signal Processing Group ([ISPGroup](https://ispgroup.gitlab.io/)), in the Mathematical engineering ([INMA](https://uclouvain.be/fr/node/2107)) department of the [ICTEAM institute](https://uclouvain.be/fr/node/1991) at [UCLouvain](https://uclouvain.be), Belgium. His main research activities focus on compressive sensing (theory and applications), inverse problems solving in optics, astronomy and biomedical sciences, compressive learning and data sciences, sparse signal representations and other low-complexity signal priors.

**For more information, you can consult his homepage on https://laurentjacques.gitlab.io**
