---
title: "Benoit Macq"
slug: "benoit-macq"
date: 2022-11-18T22:46:42+01:00
draft: false
type: "people"

# fill the position
# must be one of the following: 'Professor', 'Academic' (for associate academic members), 'Post-doctoral researcher', 'Ph.D. Student', 'Administrative staff'
position: "Academic"

# fill the picture (file inside folder)
picture: bmacq.jpg

# array of affiliations (optional - each entry must have a 'text' and may have 'url')

# array of affiliations (optional - each entry must have a 'text' and may have 'url')
affiliations:
    -
        text: "Information and Communication Technologies, Electronics and Applied Mathematics (ICTEAM)"
        url: "https://uclouvain.be/en/research-institutes/icteam"
    -
        text: "UCLouvain, Belgium"
        url: https://uclouvain.be/

# add social links 
# this should be an array of map e.g. 
# [
#     {homepage: "..."},
#     {github: "..."},
#     ...
# ]
# Social
social: [
    {homepage: https://pilab.be},
    {linkedin: https://www.linkedin.com/in/benoitmacq/},
    {email: benoit.macq@uclouvain.be},
    {googlescholar: "https://scholar.google.be/citations?user=H9pGN70AAAAJ&hl=fr"}
]
---

**Benoit Macq** received is Diploma in Electrical Engr. From the UCLouvain, Louvain-la-Neuve, Belgium in 1984. He did his military service at the Royal Military School on development of Lidar interferometry. He did his PhD thesis on digital TV compression in the frame of the RACE program of the European Union contributing to JPEG and MPEG groups. He received his PhD degree from UCLouvain- Belgium in 1989. He has been researcher in Philips Research Laboratory Belgium in 1990 and 1991 developing wavelet-based compression algorithms and contributing to the JPEG-2000 standard. Benoit Macq is Professor at Polytechnic School of UCLouvain since 1993. He has been visiting Professor at Ecole Polytechnique Fédérale de Lausanne, at the Massachusetts Institute of Technology, Boston and he is currently visiting professor at McGill University in Montreal.

**His main research** interests are image compression, image watermarking, image analysis for medical and immersive communications. Benoit Macq was General Chair of IEEE ICIP2011 in Brussels. He was associate editor of IEEE Trans on Multimedia, IEEE Trans on Image Processing, Guest Editor for the Proceedings of the IEEE. He is the founder of the “Journal of Multimodal Interfaces” edited by Springer Verlag. He has been chair of several European projects, including a Network of Excellence on Multimodal Interactions (SIMILAR project) and an Integrated Proposal, EDcine which paved the way of the technology of digital cinema in European Union.

He is the co-founder of 11 spin-off companies.

Benoit Macq has been vice-President of UCLouvain in the 2009-2014 term and he was in charge of the Service to the Society and Internationalisation of his university.  Benoit Macq is Fellow Member of the IEEE and Member of the Royal Academy of Science of Belgium. He is currently Senior Editor Associate of IEEE Trans on Image Processing.

**For more information, you can consult his homepage on https://pilab.be/about-me/?p=benoit_macq**
 
