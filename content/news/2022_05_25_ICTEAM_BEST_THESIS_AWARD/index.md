---
title: "2022_05_25_ICTEAM_BEST_THESIS_AWARD"
slug: "2022_05_25_ICTEAM_BEST_THESIS_AWARD"
date: 2022-05-25
draft: false
type: "news"
---
Dr Vincent Schellekens receives the [ICTEAM Best Thesis Award](https://uclouvain.be/en/research-institutes/icteam/thesis-award.html) for his thesis entitled "*Extending the Compressive Statistical Learning Framework: Quantization, Privacy, and Beyond*". Congratulations Vincent!! 
