---
title: "PaperECCV24Snd"
slug: "PaperECCV24Snd"
date: 2024-08-14T14:42:21+02:00
draft: false
type: "news"
--- 
**Accepted paper**: "Sequential Representation Learning via Static-Dynamic Conditional Disentanglement " by Mathieu Cyrille Simon, Pascal Frossard and Christophe De Vleeschouwer ([arXiv preprint](https://www.arxiv.org/abs/2408.05599)), to be presented at the [European Conference on Computer Vision](https://eccv.ecva.net/) (ECCV, Milan, 1-4 Oct 2024). 

*Unsupervised disentangled representation learning within sequential data, focusing on separating time-independent and time-varying factors in videos. This disentanglement is expected to facilitate numerous downstream generation or classification tasks and enhance model explainability.*
 
