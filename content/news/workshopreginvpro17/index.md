---
title: "Workshopreginvpro17"
slug: "workshopreginvpro17"
date: 2017-08-30
draft: false
type: "news"
---
Organizing the Workshop on "[Regularized Inverse Problem Solving and High-Dimensional Learning Methods](https://sites.google.com/view/wips-learn)". Plenary speakers: G.  Peyré (ENS, Paris, France) and U. Kamilov (WUSTL, USA)
