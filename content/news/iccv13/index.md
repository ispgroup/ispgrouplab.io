---
title: "Cvpr13"
slug: "iccv13"
date: 2013-12-06
draft: false
type: "news"
---
ISPGroup contributes to ICCV 2013:
* Accepted paper: "[Discriminative Label Propagation for Multi-object Tracking with Sporadic Appearance Features](https://dial.uclouvain.be/pr/boreal/object/boreal:134640)", by Amit Kumar K.C. and C. De Vleeschouwer.
* Invited talk: "Autonomous sport production", in Workshop on Vision-based Sports Analytics, by C. Verleysen and C. De Vleeschouwer. 
