---
title: "Cvpr2019"
slug: "cvpr2019"
date: 2019-06-01
draft: false
type: "news"
---
Welcome to Long Beach! See our 4 contributions to ICML 2019 and CVPR2019: 
- S. Carbonnelle and C. De Vleeschouwer, [Layer rotation: a surprisingly simple indicator of generalization in deep networks?](https://openreview.net/forum?id=B1g4DEB234), Workshop on Identifying and Understanding Deep Learning Phenomena, ICML 2019.
- M. Istasse, J. Moreau, and C. De Vleeschouwer, [Associative Embedding for Team Discrimination](http://openaccess.thecvf.com/content_CVPRW_2019/html/CVSports/Istasse_Associative_Embedding_for_Team_Discrimination_CVPRW_2019_paper.html), Computer Vision in Sports, CVPR 2019.
- A. Cioppa et al., [ARTHuS: Adaptive Real-Time Human Segmentation in Sports through Online Distillation](http://openaccess.thecvf.com/content_CVPRW_2019/html/CVSports/Cioppa_ARTHuS_Adaptive_Real-Time_Human_Segmentation_in_Sports_Through_Online_Distillation_CVPRW_2019_paper.html), Computer Vision in Sports, CVPR 2019, joint work with ULiège.
- B. Brummer and C. De Vleeschouwer, [Natural Image Noise Dataset](http://openaccess.thecvf.com/content_CVPRW_2019/papers/NTIRE/Brummer_Natural_Image_Noise_Dataset_CVPRW_2019_paper.pdf), New Trends in Image Restoration and Enhancement workshop, CVPR 2019.
