---
title: "BestPosterISpaRo24"
slug: "BestPosterISpaRo24"
date: 2024-06-27
draft: false
type: "news"
---
**Best Poster Award** at the International Conference on Space Robotics (ISpaRo24): Antoine Legrand, Renaud Detry and Christophe De Vleeschouwer, "Leveraging neural radiance fields for pose estimation of an unknown space object during proximity operations" ([arXiv](https://arxiv.org/pdf/2405.12728)), [ISpaRo 2024](https://www.isparo.space/).
