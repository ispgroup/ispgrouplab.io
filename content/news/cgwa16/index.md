---
title: "Cgwa16"
slug: "cgwa16"
date: 2016-11-16
draft: false
type: "news"
---
Organizing in UCL the regular workshop "[FNRS Wavelet Contact Group on Wavelet and Applications](https://sites.google.com/site/fcgwaveapps/)" this time dedicated to random binary embeddings, compressive sensing and signal sampling on graphs, thanks to the invitation of Sjoerd Dirksen (RWTHAachen, Germany) and [Gilles Puy](https://sites.google.com/site/puygilles/) (Technicolor, Rennes, France), the two plenary speakers. 
