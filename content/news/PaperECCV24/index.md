---
title: "PaperECCV24"
slug: "PaperECCV24"
date: 2024-08-14T14:37:50+02:00
draft: false
type: "news"
--- 
**Accepted paper**: "*Keypoint Promptable Re-Identification*" by Vladimir Somers, A. Alahi and C. De Vleeschouwer ([arXiv preprint](https://arxiv.org/abs/2407.18112)), to be presented at the [European Conference on Computer Vision](https://eccv.ecva.net/) (ECCV, Milan, 1-4 Oct 2024). 

*KPR is SOTA re-identification method, with optional keypoint prompts, that is robust to multi-person occlusions and plug-and-play with any pose estimator (or through manual keypoint selection). More information [here](https://github.com/VlSomers/keypoint_promptable_reidentification).*
