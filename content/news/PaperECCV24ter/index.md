---
title: "PaperECCV24ter"
slug: "PaperECCV24ter"
date: 2024-08-14T14:49:43+02:00
draft: false
type: "news"
---
**Accepted paper**: "*MPL: Lifting 3D Human Pose from Multi-view 2D Poses*" by Seyed Abolfazl Ghasemzadeh, A. Alahi, and C. De Vleeschouwer, to be presented at "*[T-CAP: Towards a Complete Analysis of People: Fine-grained Understanding for Real-World Applications](https://sites.google.com/view/t-cap-2024/home)*" workshop at the [European Conference on Computer Vision](https://eccv.ecva.net/) (ECCV, Milan, 1-4 Oct 2024). 

*MPL is a 3D human pose lifter that takes multi-view 2D poses as its inputs. The main challenge in multi-view 3D human pose estimation is lack of real-life images paired with 3D. This challenge is bypassed in MPL by relying on a two-stage framework consisting of an off-the-shelf 2D pose estimator and a multi-view 3D pose lifter. More information [here](https://github.com/aghasemzadeh/OpenMPL)*
