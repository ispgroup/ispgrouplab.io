---
title: "Best Presentation Award at AI4Space workshop in ECCV2022"
slug: "best-pres-award-eccv22"
date: 2022-10-23
draft: false
type: "news"
---
Best Presentation Award at [AI4Space](https://aiforspace.github.io/2022/) workshop in [ECCV 2022](https://eccv2022.ecva.net/), by A. Legrand, R. Detry, C. De Vleeschouwer, "End-to-end Neural Estimation of Spacecraft Pose with Intermediate Detection of Keypoints", AI4Space workshop, ECCV 2022.
