---
title: "2019_06_18_Best_paper_award"
slug: "2019_06_18_Best_paper_award"
date: 2019-06-18T17:42:31+02:00
draft: false
type: "news"
---

**Best paper award** at *Computer Vision in Sports* workshop in CVPR2019 for the joint work with ULiège: A. Cioppa, A. Deliege, M. Istasse, C. De Vleeschouwer, Marc Van Droogenbroeck, [ARTHuS: Adaptive Real-Time Human Segmentation in Sports through Online  Distillation](http://openaccess.thecvf.com/content_CVPRW_2019/html/CVSports/Cioppa_ARTHuS_Adaptive_Real-Time_Human_Segmentation_in_Sports_Through_Online_Distillation_CVPRW_2019_paper.html), Computer Vision in Sports, CVPR 2019.
