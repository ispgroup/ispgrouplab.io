---
title: "Boelpaepe17"
slug: "boelpaepe17"
date: 2017-12-18
draft: false
type: "news"
---
Profs C. De Vleeschouwer and L. Jacques have been awarded the 45th "de Boelpaepe" prize (2015-2016) by the Académie royale des sciences, des lettres et des beaux-arts de Belgique (Royal Academy of Science, Letters and Fine Arts of Belgium) for their scientific research on image processing with wavelet analysis and sparsity methods. 
