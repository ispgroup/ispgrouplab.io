---
title: "Quadsense Postdoc Opening"
slug: "quadsense-postdoc-opening"
date: 2023-12-23T14:00:37+01:00
draft: false
type: "news"
---
New postdoc position opened in the research group of Prof. [Laurent Jacques](https://laurentjacques.gitlab.io) to work on the [QuadSense](https://laurentjacques.gitlab.io/project/quadsense/) project, a new project funded by the [Belgian Fund for Scientific Research - FNRS](https://www.frs-fnrs.be/en). 

More information (application procedure, project topic, ...)  &rarr; [check here](https://laurentjacques.gitlab.io/project/quadsense/) &larr;
 
