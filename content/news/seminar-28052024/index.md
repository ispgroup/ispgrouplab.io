---
title: "Seminar 28052024"
slug: "seminar-28052024"
date: 2024-04-26T09:04:43+02:00
draft: false
type: "news"
---
On Tuesday, **May 28th, 2024**, 14h, we'll be glad to welcome Ulugbek Kamilov from Washington University, St. Louis, USA, for a joint ISPGroup / LINMA2120 Seminar entitled "*Computational Imaging: Restoration Deep Networks as Implicit Priors.*" More information [here](https://ispgroup.gitlab.io/seminar/) and [here](https://uclouvain.be/en/research-institutes/icteam/inma/seminars.html).
