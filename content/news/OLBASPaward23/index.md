---
title: "OLBASPaward23"
slug: "OLBASPaward23"
date: 2023-02-10
draft: false
type: "news"
---
[Olivier Leblanc](https://ispgroup.gitlab.io/people/olivier-leblanc) got the **[best contribution award](https://baspfrontiers.org/best-contribution-awards/)** at the [Biomedical and Astronomical Signal Processing conference (BASP'23)](https://baspfrontiers.org/) for his work entitled "Interferometric Lensless Imaging -- Rank-one Projections of Image Frequencies with Speckle Illuminations."
