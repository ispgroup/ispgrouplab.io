---
title: "2020_11_12_ICTEAM_BEST_THESIS_AWARD"
slug: "2020_11_12_ICTEAM_BEST_THESIS_AWARD"
date: 2020-11-12T17:37:56+02:00
draft: false
type: "news"
---

Dr Amirafshar Moshtaghpour receives the [ICTEAM Best Thesis Award](https://uclouvain.be/en/research-institutes/icteam/thesis-award.html) for his thesis entitled "*Computational Interferometry for Hyperspectral Imaging*". Congratulations Amir!! 
