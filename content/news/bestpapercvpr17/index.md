---
title: "Bestpapercvpr17"
slug: "bestpapercvpr17"
date: 2017-09-17
draft: false
type: "news"
---
Congrats to Simon Carbonnelle, member of the ISPGroup, and his collaborators in the "OneBonsai" team which won ArcelorMittal Belgium’s first hackathon in the two categories "Artificial Intelligence" and "Best Overall". 
