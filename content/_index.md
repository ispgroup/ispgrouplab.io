---
title: 'homepage'
---

{{<image src="/media/img/ispg_logo_new_large.jpg" style="width:80%; max-width:450px" position="center">}}


# Welcome to ISPGroup! 

The **Image and Signal Processing Group** of UCLouvain, ICTEAM, Belgium, whose members are listed [here](/people), is active in many theories, models, techniques, [codes, datasets, and codecs](/code) related to signal and image processing in a broad sense. 

For instance, the ISPGroup researchers work on signal acquisition, compression and streaming; machine and deep learning for computer vision; (multiple) object tracking; content-based data retrieval; biomedical signal and medical image processing; compressed sensing and inverse problem solving, compputational imaging; hyperspectral imaging; tomographic methods; sparse signal representation; data restoration, ... and many other topics described in some of [these posts](/research), [these publications](/publications), and in this visual---and clickable---**word cloud**:
