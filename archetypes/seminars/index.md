---
title: "{{ replace .Name "-" " " | title }}"
slug: "{{.Name}}"
draft: false

# name of the speaker
speaker : 

# date of the seminar
seminarDate:           

# external link (eg web page of the speark)
externalLink: 

# Attached files (this must be a key/value list eg. slides: name_of_the_file.pdf )
files : []

# Location of the seminar
location: 

# duration of the seminar in minutes
duration: 

# Abstract of the seminar
abstract:

---

