---
title: "{{ replace .Name "-" " " | title }}"
slug: "{{.Name}}"
date: {{ .Date }}
draft: false
type: "code"


# Table of content
toc: false       # do you want to show a ToC ? [true, false]
tocOpen: false   # should the ToC be opened on page load? [true, false]
tocSide: left   # which side do you want the ToC to be shown (desktop only - always top on mobile) [left, right]

# Add a thumbnail (will be shown only in list of codes)
thumbnail: 

# Add a cover picture [optional]
cover: 
coverCaption: 

# Add a short description that will be shown on the list page [optional]
description: 

# Add categories of the research post (e.g. image processing)
categories: []

# Add researchers involved
researchers: []
---
