---
title: "{{ replace .Name "-" " " | title }}"
slug: "{{.Name}}"
date: {{ .Date }}
draft: false
type: "people"

# fill the position
# must be one of the following: 'Professor', 'Academic' (for associate academic members), 'Post-doctoral researcher', 'Ph.D. Student', 'Administrative staff'
position: 

# fill the picture (file inside folder)
picture: 

# array of affiliations (optional - each entry must have a 'text' and may have 'url')
affiliations:

# add social links 
# this should be an array of map e.g. 
# [
#     {homepage: "..."},
#     {github: "..."},
#     ...
# ]
social: [

]

---
