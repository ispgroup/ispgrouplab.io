# ispgroup.gitlab.Io

## Working on the website

First clone the repo (you must be authenticated on gitlab, i.e. use an ssh key)

    git clone git@gitlab.com:ispgroup/ispgroup.gitlab.io.git

### Update your local version and check other branches

To update to repository:

    git fetch --prune --all
    git checkout [a-branch-name]
    git pull

This will pull the latest available version of the specific branch. You can then either create a new branch or keep working on an existing branch.
If you want to push a new local branch on the repository

    git checkout -b my-new-branch       (this will create a new branch)
    git push -u origin my-new-branch    (define the upstream to origin/my-new-branch)
    
    (after updates have been implemented)
    git push    (once the upstream is defined you don't need to specify it)

It is forbidden to push on the *main* branch. The *main* branch may only be updated with merge request. If you create a merge request, assign it to one of the repository maintainer.

### Publish your content
Once the content of your branch is ready to be published on the website, you need to initiate a merge request to one of the maintainers (currently, arnaud(dot)browet(at)uclouvain(dot)be or laurent(dot)jacques(at)uclouvain(dot)be).

To create a merge request, open gitlab in your browser, navigate to the ispgroup.gitlab.io repository and tab into "merge requests"

Alternatively, when you push your branch into gitlab, you can add option (with -o) to automatically create a merge request e.g.

    git push -o merge_request.create -o merge_request.target=main -o merge_request.assign=9651730 -o merge_request.assign=12324625

Note that the merge_request.assign option are 9651730 for Laurent Jacquest and 12324625 for Arnaud Browet (you can add both in all your merge request)

You can also add the options

    -o merge_request.title="My Title" [to specifiy a title]
    -o merge_request.description="My Description" [to specify a description]


### Local merges
When editing content on your local branch, you might fall behind the *main* branch content. In most cases, it will not affect your work because you will be working on different content pages. However, if it matters, you can merge the main branch into your local branch.

    (first commit or stash your current changes)
    git checkout main
    git pull
    git checkout <your_local_branch>
    git merge main
    
Note that, when merging the *main* branch into your local branch, you might need to resolve conflicts.


# Update the content of the website

## Start a development server
You will need to have hugo installed but you don't need Go. To install hugo on your device, please check the [hugo installation guide](https://gohugo.io/getting-started/installing)

To start a development server:
    
    hugo server -D 

You may check the hugo server command for additional flags but -D allows to serve "Draft" pages (which will not be included in the production compilation: don't forget to update the Draft parameter of your pages when it's ready)

## Markdown content

The markdown processor used by Hugo is [goldmark](https://github.com/yuin/goldmark/) which is fully compliant with the [CommonMark](https://commonmark.org) specifications. If needed, [Markdown Guide](https://www.markdownguide.org/basic-syntax/) has a very good coverage of basic markdown syntax.

Mathematics & equations are rendered using the [Katex](https://katex.org/) library. You should be able to use regular math equations in latex command anywhere using either \$..\$ or \$\$..\$\$ 
If something is not working has intended, either contact arnaud browet or open an issue on gitlab.

## create a new member (a.k.a people)

    hugo new people/laurent-jacques

will create a new folder inside content/people where you can edit the content of the page inside index.md (check the existing pages for examples). Required files (e.g. images) for this page must be put directly inside this folder and referenced without any path in your md content.

Front-matter parameters:
| Parameter | Value |
|-----------|-------|
| title | \[automatically created\] Name of the member |
| slug | \[automatically created\] url for the page (must not contain whitespaces) |
| date | \[automatically created\] published date|
| draft | (default:true) set to false to publish the page on live|
| type | "people" (do not alter this value)|
| position | (required) position of the member, must be one of the following: 'Professor', 'Post-doctoral researcher', 'Ph.D. Student', 'Administrative staff'|
| picture | (optional) cover picture for the member|
| affiliations | array of affiliations. each affiliation must have a "text" and may have a "url". E.g.<br>affiliations:<br>&nbsp;&nbsp;&nbsp;&nbsp;- text: "Uclouvain"<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;url: https://uclouvain.be |
| social | array of map of links to social platforms (the list of available icons is available [here](https://github.com/rhazdon/hugo-theme-hello-friend-ng/blob/master/docs/svgs.md)) E.g.<br>social:[<br>&nbsp;&nbsp;&nbsp;&nbsp;{facebook:"https://facebook.com"}<br>&nbsp;&nbsp;&nbsp;&nbsp;{linkedin: "https://www.linkedin.com"}<br/>]|

## create a new research post

    hugo new research/my-cool-new-research-paper

Note that the slug (last part of the url in the website) will be set to *my-cool-new-research-paper* and the title of the page will be set to *My Cool New Research Paper*. Don't hesitate to update the title of the page, but keep the slug untouched. If you need another slug, then update the creation command accordingly

Front-matter parameters:
| Parameter | Value |
|-----------|-------|
| title | \[automatically created\] Name of the post |
| slug | \[automatically created\] url for the post (must not contain whitespaces) |
| date | \[automatically created\] published date|
| draft | (default:true) set to false to publish the page on live|
| type | "research" (do not alter this value)|
| toc | (default: true) show a table of content|
| tocOpen | (default: true) the ToC should be open on page load|
| tocSide | (default: left) position of the ToC on large screen|
| thumbnail | thumbnail picture displayed in the list of research |
| cover | cover picture for the research post (source file must be copied inside the directory) |
| coverCaption | caption for the cover picture| 
| description | short description of the research post (displayed on the archive list)|
| categories | array of categories for the research post (allows search and display in category page. E.g. categories:\["Signal Processing", "Image processing"\]|
| researchers | array of involved researchers, either as the slug of a people page or as a fullname. E.g. researchers: ["victor-joos", "Arnaud Browet", "Someone Else" ] |
| comments | [yaml object] add "comments" from mastodon  - you must respect the identation of the following elements|
| comments > id | id of the mastodon post |
| comments > host | (optional) host of mastodon, default to sigmoid.social |
| comments > username | (optional) username of the mastodon post , default to ispgroup |


## OpenScience

    hugo new code/my-code-or-dataset

Create a new page in the OpenScience section. Use it to share code or dataset

Front-matter parameters:
| Parameter | Value |
|-----------|-------|
| title | \[automatically created\] Name of the post |
| slug | \[automatically created\] url for the post (must not contain whitespaces) |
| date | \[automatically created\] published date|
| draft | (default:true) set to false to publish the page on live|
| type | "code" (do not alter this value)|
| toc | (default: false) show a table of content|
| tocOpen | (default: false) the ToC should be open on page load|
| tocSide | (default: left) position of the ToC on large screen|
| thumbnail | thumbnail picture displayed in the list of codes |
| cover | cover picture for the research post (source file must be copied inside the directory) |
| coverCaption | caption for the cover picture| 
| description | short description of the code or dataset (displayed on the archive list)|
| categories | array of categories for the code or dataset (allows search and display in category page. E.g. categories:\["Dataset", "Images"\]|
| researchers | array of involved researchers, either as the slug of a people page or as a fullname. E.g. researchers: ["victor-joos", "Arnaud Browet", "Someone Else" ] |


## Seminars 

    hugo new seminars/someone-is-visiting

The seminar pages are pretty restrictive and need the following parameters


|  Parameter      |  value |
| ----------- | ----------- |
|  speaker      | name of the speaker       |
| seminarDate   | Date of the seminar (format YYYY-MM-DDTHH:MM  eg: 2022-10-15T16h00 for 15 october 2022 at 16H00)        |
| externalLink      | (optional) external web page of the speaker       |
| files      | Array of available files linked to the seminar (eg. slides)     |
| location      | location of the seminar       |
| duration      | Duration of the seminar       |
| Abstract      |  Short abstract of the seminar      |

Note that, for the seminars, the content of the markdown file (what comes after the front matter between --- ) is not used

## News

    hugo new news/something-to-announce

You must put the content of the news after the front matter. All the parameters (appart from Draft) must be kept untouched

## Publications

Publications are automatically updated every time the site is compiled in the production environment.
During development, the publications shown are just a snapshot (at the time of writing this documentation)

If you want to update the publications, you can run the shell script

    sh get_dial_publications.sh

You can check that this script is not responsible to query the Dial API. Instead, the Dial API is queried with *get_dial_publications_local.php*. This file must be stored on a web server, internal to the Uclouvain network (the Dial API is not available outside) that is accessible from outside the Uclouvain network. Currently, the php script is running on *sites.uclouvain.be/ispgroup/dial/* which is the url targeted by the shell script.

## Shortcodes

In Hugo, [shortcodes](https://gohugo.io/content-management/shortcodes/) allow to embed more "advanced" layout directly in markdown. All the built-in Hugo shortcodes are available and specific shortcodes have been developed for the website. To use a shortcode in your markdown content, use it as html tag enclosed with {{...}} e.g. to use the <image> shortcode

    ...
    {{<image src="<source_file>" position="<position-of-the-image>" caption="<caption-for-the-image>">}}
    ...

Here is the list of custom shortcodes

|Shortcode| Parameters|
|---|---|
|< image > | src \[required\] source file for the image <br>caption (optional) A caption to be displayed under the image<br> width (optional - default: 100%) width of the image (can be any regular css units e.g. 50%, 300px, 10vw)<br>alt (optional) Alternative text if the image can not be loaded<br>position (optional - default: center) position of the image in the page; can be either of "left", "center", "right"<br>style (optional) Embed css style directly on the <img/> element. only use it if you know what you're doing.
|< iframe >| src (required) source for the iframe content<br>position (optional - default: center) position of the iframe in the page; can be either of "left", "center", "right"<br>*args : you can use any regular iframe attribute (sandbox, referrerpolicy ,allowfullscreen ... )|
|nextseminars| Show the list of upcoming seminars<br> limit (optional - default:3) Number of upcoming seminars to show |
|previousseminars| Similar to next seminars but for previous ones :)|
|ref| slug of the person you want to reference. E.g. {{<ref "laurent-jacques">}} |


## Bibliography

In research pages, you can use bibliographic references using either your own bibliography.json file (to be created within the research page folder) or dial publications (automatically updated daily).

### Using bibliography.json

To use your own bibliography file, you must first convert your .bib file to .json with the csljson format. This conversion can be easily achieved with pandoc (use the -t tag for the format and the -o tag for the output file)

    pandoc my_bibliography.bib -t csljson -o bibliography.json

For example the following bib entry

    @ARTICLE{smit54,
        AUTHOR = {J. G. Smith and H. K. Weston},
        TITLE = {Nothing Particular in this Year's History},
        YEAR = {1954},
        JOURNAL = {J. Geophys. Res.},
        VOLUME = {2},
        PAGES = {14-15}
    }

will be converted to 

    {
        "author": [
        {
            "family": "Smith",
            "given": "J. G."
        },
        {
            "family": "Weston",
            "given": "H. K."
        }
        ],
        "container-title": "J. Geophys. Res.",
        "id": "smit54",
        "issued": {
        "date-parts": [
            [
            1954
            ]
        ]
        },
        "page": "14-15",
        "title": "Nothing particular in this year’s history",
        "type": "article-journal",
        "volume": "2",
        "issue": "4",
    },


Note that the "id" is the same as your regular latex key (smith54 in the example above)

Once your biliography.json file is created, add it to the research page folder. You can then cite your references using the \<cite\>  shortcode

    {{<cite type="t" id="smit54">}}

You must always specify the id of the publication and you may choose one of the following type

| type | result |
|---|---|
| N | numbered citation. eg: ... [2] ... |
| P | parenthesis citation. eg: .... (Smith and Weston, 1954) ... |
| T | textual citation. eg: .... Smith and Weston (1954) ... |

An error will be printed in research page if 
- the bibliography.json file is not present or not named correctly
- the id is not defined or not found in the biliography.json file
- the type parameter is not one of the above

### Using dial publication

To cite paper in dial, you only need to use the \<citedial\> shorcode

    {{<citedial type="P" id="boreal:216656">}}

Note that the id is the regular boreal id (it should start with 'boreal:' followed by the dial id that you can found as the last parameter of the dial handle/url)