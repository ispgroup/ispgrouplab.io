var publicationsList = new List("publicationContainer", {
  valueNames: ["publication-title", "authors", "book", "journal"],
});

var selectedYear = null;

// year filter
document.querySelector("#year").addEventListener("change", function () {
  if (this.value == "all") selectedYear = null;
  else selectedYear = this.value;

  applyFilter();
});

const applyFilter = () => {
  publicationsList.filter(function (item) {
    if (!selectedYear) return true;

    $i = item.elm;
    $y = $i.dataset.year;

    if ($y==selectedYear) return true;
    return false
  });
};

applyFilter();

/*
// PREVIOUS IMPLEMENTATION WITH ISOTOPE JS
// quick search regex
//var qs;
var qsRegex;
var selectedYear;

var elem = document.querySelector('.publications-list');
var iso = new Isotope(elem, {
    // options
    itemSelector: '.publication-item',
    layoutMode: 'vertical',
    getSortData: {
    sortingValue: '[data-sortingValue]'
    // year: '[data-year]',
    // day: '[data-day]'
    },
    sortBy:['sortingValue'],
    // sortBy: ['year', 'day'],
    sortAscending: {
    // year: false,
    // day: true,
    sortingValue: false,
    },
    filter: function (e) {
    if (selectedYear){
        if (e.dataset.year != selectedYear) return false
    }

    if (!qsRegex) return true

    var title = e.querySelector('.publication-title')
    
    if (title) {
        //return title.innerText.toLowerCase().includes(qs)
        return title.innerText.match(qsRegex);
    }
    return false
    }
});

// year filter
document.querySelector('#year').addEventListener('change', function(){
    if (this.value == 'all') selectedYear=null;
    else selectedYear = this.value;    
    iso.arrange()
})
// use value of search field to filter
var quickSearch = document.querySelector('.quicksearch')

quickSearch.addEventListener('keyup', debounce(function () {
    if (!quickSearch.value) {
    qsRegex = null;
    }
    else {
    //qs = quickSearch.value.toLowerCase()
    qsRegex = new RegExp(quickSearch.value, 'gi');
    }
    iso.arrange()
}, 150));

// debounce so filtering doesn't happen every millisecond
function debounce(fn, threshold) {
    var timeout;
    threshold = threshold || 100;
    return function debounced() {
    clearTimeout(timeout);
    var args = arguments;
    var _this = this;
    function delayed() {
        fn.apply(_this, args);
    }
    timeout = setTimeout(delayed, threshold);
    };
}
*/
