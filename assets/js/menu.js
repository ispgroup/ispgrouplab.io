// Mobile menu
const menuTrigger = document.querySelector(".menu-trigger");
const menu = document.querySelector(".menu");
const mobileQuery = getComputedStyle(document.body).getPropertyValue(
  "--tabletWidth"
);
const isMobileTheme = () => window.matchMedia(mobileQuery).matches;
const isMobileMenu = () => {
  menuTrigger && menuTrigger.classList.toggle("hidden", !isMobileTheme());
  menu && menu.classList.toggle("hidden", isMobileTheme());
};

isMobileMenu();

menuTrigger &&
  menuTrigger.addEventListener(
    "click",
    () => menu && menu.classList.toggle("hidden")
  );

window.addEventListener("resize", isMobileMenu);

// set smooth scroll margin
const HeaderElement = document.querySelector('header.header')
const setSmoothScrollMargin = ()=>{
  const headerHeight = HeaderElement.offsetHeight
  
  document.documentElement.style.setProperty('--scrollMarginTop', `${headerHeight+10}px`);
  
}
setSmoothScrollMargin()
window.addEventListener("resize", setSmoothScrollMargin);
