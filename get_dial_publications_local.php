<?php 

$dial_root_url = 'https://dial.uclouvain.be/solr/repository/select?';
$params = [
    'version' => "2.2",
    'start' => '0',
    'rows'=>'9999',
    'qt'=>'standard',
    'wt'=> 'json',
    'q'=>'ss_state:A AND sm_contentmodel:"boreal-system:ResearchPublicationCM" AND sm_creator:("De Vleeschouwer, Christophe" OR "Jacques, Laurent" OR "Macq, Benoît") AND sm_type:("Article de périodique (Journal article)", "Communication à un colloque (Conference Paper)", "Contribution à ouvrage collectif (Book Chapter)", "Brevet (Patent)")',
];

$query_params=Array();
foreach ($params as $k=>$v){
    array_push($query_params, $k.'='.urlencode($v));
}

$dial_query_url = $dial_root_url . join('&', $query_params);

$json = file_get_contents($dial_query_url);

if ($json):
    echo $json;
else:
    header("HTTP/1.1 500 Internal Server Error");
    die();
endif;
?>